c     Subroutine to test that perpendicular vector constructed for the
c     collinear limit when spin correlations are on is consistent with the
c     pt of the radiation.

      subroutine testcolllim(emitter)
      implicit none
      integer emitter
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      real * 8 q0,xocsi,x,kperp(0:3),kperp2,jac1,jac2,jac3,jac4
      real * 8 xborn(ndiminteg),xrad(3),kperpr(3)
      real * 8 random
      integer j

      do j=1,ndiminteg
         xborn(j)=random()
      enddo

      call gen_born_phsp(xborn)
      kn_emitter = emitter

      xrad(1)=1d-4*random()
      xrad(2)=random()
      xrad(3)=random()
      if(emitter > 2) then
         call gen_real_phsp_fsr(xrad,jac1,jac2,jac3,jac4)
         call buildfsrvars(emitter,q0,xocsi,x,kperp,kperp2)
      else
         call gen_real_phsp_isr(xrad,jac1,jac2,jac3,jac4)
         kperp(1)=sin(kn_azi)
         kperp(2)=cos(kn_azi)
         kperp(3)=0
         kperp(0)=0
      endif
c     Now build the kperp from the real kinematics
      kperpr = kn_cmpreal(1:3,nlegreal)-kn_cmpborn(1:3,emitter)
     1     * dotp3(kn_cmpreal(1:3,nlegreal),kn_cmpborn(1:3,emitter))/
     2     dotp3(kn_cmpborn(1:3,emitter),kn_cmpborn(1:3,emitter))
      write(*,*) ' testcolllim, emitter ',5
      
      write(*,*) testprop(kperpr(1:3),kperp(1:3))
      
      write(*,*) ' end testcolllim, emitter ',emitter
      write(*,*)
      contains
      real * 8 function dotp3(p1,p2)
      real * 8 p1(3),p2(3)
      dotp3 = p1(1)*p2(1)+ p1(2)*p2(2)+ p1(3)*p2(3)
      end function dotp3
      logical function testprop(v1,v2)
      real * 8 v1(3),v2(3),v1max,v2max
c     test if vectors v1 and v2 are proportional
      integer j
      v1max = v1(1)
      do j=2,3
         if(abs(v1(j))>abs(v1max)) v1max=v1(j)
      enddo
      v2max = v2(1)
      do j=2,3
         if(abs(v2(j))>abs(v2max)) v2max=v2(j)
      enddo
      testprop = .true.
      if(abs(v1max)+abs(v2max) == 0) then
         continue
      elseif(v1max*v2max == 0) then
c One is zero and one is not!         
         testprop = .false.
      else
         do j=1,3
            if(abs(v1(j)/v1max-v2(j)/v2max)>1d-4) then
               testprop = .false.
            endif
         enddo
      endif
      write(*,*) v1/v1max
      write(*,*) v2/v2max
      end function testprop
      
      end
