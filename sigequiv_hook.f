c     sigequiv_hook.f contains the subroutines to initialize the
c     equivto and equivcoef arrays. The default sigequiv_hook.f (this file)
c     does not do any initialization and returns
c     a negative return code. It can be replaced with subroutines that
c     do a proper initialization in the process directory.
c     If the flag
c     writeequivfile 1
c     is present in the powheg.input (and the subroutines in  sigequiv_hook.f
c     return a negative return code) the program prints files with names
c     sigequiv_hook-<flag>-XXXX.f, where flag is one of rad, btl, born, virt,
c     and the suffix -XXXX, where XXXX is a four digit integer, is present
c     only in the manyseeds runs, and it represents the seed number.


      subroutine fillequivarraybtl(nentries,equivto,equivcoef,iret)
      implicit none
      integer nentries,iret
      integer equivto(nentries)
      real * 8 equivcoef(nentries)
      iret = -1
      end

      subroutine fillequivarrayrad(nentries,equivto,equivcoef,iret)
      implicit none
      integer nentries,iret
      integer equivto(nentries)
      real * 8 equivcoef(nentries)
      iret = -1
      end

      subroutine fillequivarrayborn(nentries,equivto,equivcoef,iret)
      implicit none
      integer nentries,iret
      integer equivto(nentries)
      real * 8 equivcoef(nentries)
      iret = -1
      end

      subroutine fillequivarrayvirt(nentries,equivto,equivcoef,iret)
      implicit none
      integer nentries,iret
      integer equivto(nentries)
      real * 8 equivcoef(nentries)
      iret = -1
      end
      
      subroutine fillequivarrayreg(nentries,equivto,equivcoef,iret)
      implicit none
      integer nentries,iret
      integer equivto(nentries)
      real * 8 equivcoef(nentries)
      iret = -1
      end
