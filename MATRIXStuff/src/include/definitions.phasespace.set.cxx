//#define psi_phasespace_randoms (*psi.phasespace_randoms)

#define psi_g_onshell_decay (psi.g_onshell_decay)
//#define psi_g_onshell_decay (psi.g_onshell_decay())
//#define psi_g_onshell_decay (*psi.g_onshell_decay)

#define psi_v_smin (psi.v_smin)
#define psi_v_smax (psi.v_smax)

#define psi_g_p (psi.g_p)
#define psi_g_f (psi.g_f)
#define psi_g_t (psi.g_t)
#define psi_g_d (psi.g_d)

#define psi_needed_v_smin (psi.needed_v_smin)
#define psi_needed_v_smax (psi.needed_v_smax)

#define psi_needed_g_p (psi.needed_g_p)
#define psi_needed_g_f (psi.needed_g_f)
#define psi_needed_g_t (psi.needed_g_t)
#define psi_needed_g_d (psi.needed_g_d)

#define psi_c_p (psi.c_p)
#define psi_c_f (psi.c_f)
#define psi_c_t (psi.c_t)
#define psi_c_d (psi.c_d)



#define psi_xbsmin_opt (psi.smin_opt)
#define psi_xbsqrtsmin_opt (psi.sqrtsmin_opt)

#define psi_smin_opt (psi.smin_opt)
#define psi_sqrtsmin_opt (psi.sqrtsmin_opt)



#define psi_n_particle (psi.csi->n_particle)


#define psi_xb_max (psi.xb_max)
#define psi_xb_max_dipoles (psi.xb_max_dipoles)
#define psi_no_random (psi.no_random)
#define psi_no_random_dipole (psi.no_random_dipole)

#define psi_random_MC (psi.random_MC)
#define psi_random_MC_tau (psi.random_MC_tau)
#define psi_random_tau (psi.random_tau)
#define psi_random_x12 (psi.random_x12)
#define psi_random_z (psi.random_z)


#define psi_container_IS_name (psi.container_IS_name)
#define psi_container_IS_startvalue (psi.container_IS_startvalue)
#define psi_container_IS_switch (psi.container_IS_switch)

#define psi_xbp_all (psi.xbp_all)
#define psi_xbs_all (psi.xbs_all)
#define psi_xbsqrts_all (psi.xbsqrts_all)

#define psi_xbp (psi.xbp_all)
#define psi_xbs (psi.xbs_all)
#define psi_xbsqrts (psi.xbsqrts_all)

#define psi_start_xbp_all (psi.start_xbp_all)
#define psi_start_xbs_all (psi.start_xbs_all)
#define psi_start_xbsqrts_all (psi.start_xbsqrts_all)

#define psi_dipole_sinx_min (psi.dipole_sinx_min)




#define psi_M (psi.M)
#define psi_M2 (psi.M2)
#define psi_cM2 (psi.cM2)
#define psi_Gamma (psi.Gamma)
#define psi_map_Gamma (psi.map_Gamma)
#define psi_reg_Gamma (psi.reg_Gamma)
/*
#define psi_M (psi.M())
#define psi_M2 (psi.M2())
#define psi_cM2 (psi.cM2())
#define psi_Gamma (psi.Gamma())
#define psi_map_Gamma (psi.map_Gamma())
#define psi_reg_Gamma (psi.reg_Gamma())
*/


#define psi_n_alpha_steps (psi.n_alpha_steps)
#define psi_n_alpha_events (psi.n_alpha_events)
#define psi_n_alpha_epc (psi.n_alpha_epc)
#define psi_n_tau_steps (psi.n_tau_steps)
#define psi_n_tau_events (psi.n_tau_events)
#define psi_n_tau_bins (psi.n_tau_bins)
#define psi_n_x1x2_steps (psi.n_x1x2_steps)
#define psi_n_x1x2_events (psi.n_x1x2_events)
#define psi_n_x1x2_bins (psi.n_x1x2_bins)
#define psi_n_z1z2_steps (psi.n_z1z2_steps)
#define psi_n_z1z2_events (psi.n_z1z2_events)
#define psi_n_z1z2_bins (psi.n_z1z2_bins)
#define psi_n_IS_events (psi.n_IS_events)
#define psi_n_IS_events_factor (psi.n_IS_events_factor)
#define psi_n_IS_steps (psi.n_IS_steps)
#define psi_n_IS_gridsize (psi.n_IS_gridsize)
#define psi_n_IS_gridsize_p (psi.n_IS_gridsize_p)
#define psi_n_IS_gridsize_f (psi.n_IS_gridsize_f)
#define psi_n_IS_gridsize_t_t (psi.n_IS_gridsize_t_t)
#define psi_n_IS_gridsize_t_phi (psi.n_IS_gridsize_t_phi)
#define psi_n_IS_gridsize_d_cth (psi.n_IS_gridsize_d_cth)
#define psi_n_IS_gridsize_d_phi (psi.n_IS_gridsize_d_phi)
#define psi_n_IS_gridsize_xy (psi.n_IS_gridsize_xy)
#define psi_n_IS_gridsize_zuv (psi.n_IS_gridsize_zuv)

#define psi_n_events_max (psi.n_events_max)
#define psi_n_events_min (psi.n_events_min)
#define psi_n_step (psi.n_step)

#define psi_E (psi.E)
#define psi_tau_0 (psi.tau_0)
#define psi_s_had (psi.s_had)
#define psi_tau_0_s_had (psi.tau_0_s_had)

#define psi_coll_choice (psi.coll_choice)

#define psi_exp_ij_k_y (psi.exp_ij_k_y)
#define psi_exp_ij_k_z (psi.exp_ij_k_z)
#define psi_exp_ij_a_x (psi.exp_ij_a_x)
#define psi_exp_ij_a_z (psi.exp_ij_k_z)
#define psi_exp_ai_k_x (psi.exp_ai_k_x)
#define psi_exp_ai_k_u (psi.exp_ai_k_u)
#define psi_exp_ai_b_x (psi.exp_ai_b_x)
#define psi_exp_ai_b_v (psi.exp_ai_b_v)
#define psi_map_technical_x (psi.map_technical_x)
#define psi_map_technical_s (psi.map_technical_s)
#define psi_map_technical_t (psi.map_technical_t)
#define psi_mass0 (psi.mass0)
#define psi_nuxs (psi.nuxs)
#define psi_nuxt (psi.nuxt)
#define psi_exp_pdf (psi.exp_pdf)
#define psi_exp_pT (psi.exp_pT)
#define psi_exp_y (psi.exp_y)
#define psi_mapping_cut_pT (psi.mapping_cut_pT)

#define psi_cut2_pT (psi.cut2_pT)
#define psi_cut_pT_lep (psi.cut_pT_lep)


#define psi_type_parton (psi.type_parton)
#define psi_basic_type_parton (psi.basic_type_parton)


//#define psi_weight_IS (psi.weight_IS)
#define psi_weight_IS (psi.switch_IS_mode_phasespace)

#define psi_weight_opt (psi.weight_opt)
#define psi_MCweight_opt (psi.weight_opt)


#define psi_weight_in_contribution (psi.weight_in_contribution)
#define psi_weight_in_directory (psi.weight_in_directory)
#define psi_weight_min (psi.weight_min)
#define psi_weight_limit_min (psi.weight_limit_min)
#define psi_weight_limit_max (psi.weight_limit_max)
#define psi_MCweight_min (psi.weight_min)
#define psi_MCweight_limit_min (psi.weight_limit_min)
#define psi_MCweight_limit_max (psi.weight_limit_max)
/*
#define psi_ (psi.())
#define psi_ (psi.())
#define psi_ (psi.())
#define psi_ (psi.())
*/

#define psi_filename_MCweight (psi.filename_MCweight)
#define psi_filename_tauweight (psi.filename_tauweight)
#define psi_filename_x1x2weight (psi.filename_x1x2weight)
#define psi_filename_z1z2weight (psi.filename_z1z2weight)
#define psi_filename_MCweight_in_contribution (psi.filename_MCweight_in_contribution)
#define psi_filename_tauweight_in_contribution (psi.filename_tauweight_in_contribution)
#define psi_filename_x1x2weight_in_contribution (psi.filename_x1x2weight_in_contribution)
#define psi_filename_z1z2weight_in_contribution (psi.filename_z1z2weight_in_contribution)

#define psi_steering (psi.steering)

#define psi_switch_MC (psi.switch_MC)
#define psi_switch_MC_tau (psi.switch_MC_tau)
#define psi_switch_MC_x_dipole (psi.switch_MC_x_dipole)

#define psi_switch_IS_MC (psi.switch_IS_MC)
#define psi_switch_IS_tau (psi.switch_IS_tau)
#define psi_switch_IS_x1x2 (psi.switch_IS_x1x2)
#define psi_switch_IS_z1z2 (psi.switch_IS_z1z2)

#define psi_switch_qTcut (psi.switch_qTcut) // also defined in observable_set !!!

#define psi_MC_optswitch (psi.MC_optswitch)

#define psi_r (psi.r)
#define psi_sran (psi.sran)


#define psi_MC_n_channel_phasespace (psi.MC_n_channel_phasespace)
#define psi_MC_sum_channel_phasespace (psi.MC_sum_channel_phasespace)
#define psi_MC_n_channel (psi.MC_n_channel)
#define psi_MC_channel_phasespace (psi.MC_channel_phasespace)
#define psi_MC_channel (psi.MC_channel)


#define psi_hcf (psi.hcf)
#define psi_ps_factor (psi.ps_factor)

#define psi_g_tot (psi.g_tot)
#define psi_g_MC (psi.g_MC)
#define psi_g_pdf (psi.g_pdf)
#define psi_g_tau (psi.g_tau)
#define psi_g_x1x2 (psi.g_x1x2)
#define psi_g_global_NWA (psi.g_global_NWA)

#define psi_MC_g_IS_global (psi.MC_g_IS_global)

#define psi_temp_sum_w_channel (psi.temp_sum_w_channel)
#define psi_sum_w_channel (psi.sum_w_channel)
#define psi_MC_sum_w_channel (psi.MC_sum_w_channel)
#define psi_w_channel_av (psi.w_channel_av)
#define psi_MC_alpha (psi.MC_phasespace.alpha)
#define psi_MC_beta (psi.MC_phasespace.beta)
//#define psi_MC_alpha (psi.MC_alpha)
//#define psi_MC_beta (psi.MC_beta)
#define psi_MC_g_channel (psi.MC_phasespace.g_channel)
//#define psi_MC_g_channel (psi.MC_g_channel)
#define psi_MC_g_IS_channel (psi.MC_g_IS_channel)

#define psi_MC_n_rej_channel (psi.MC_n_rej_channel)
#define psi_cuts_channel (psi.cuts_channel)
#define psi_MC_n_acc_channel (psi.MC_n_acc_channel)
#define psi_counts_channel (psi.counts_channel)

#define psi_phasespace_randoms (psi.phasespace_randoms)
#define psi_random_manager (psi.random_manager)




#define psi_MC_opt_end (psi.MC_opt_end)
#define psi_IS_opt_end (psi.IS_opt_end)


#define psi_tau_opt_end (psi.tau_opt_end)
#define psi_tau_channel (psi.tau_channel)
#define psi_tau_alpha (psi.tau_alpha)
#define psi_tau_beta (psi.tau_beta)
#define psi_tau_counts_channel (psi.tau_counts_channel)
#define psi_tau_cuts_channel (psi.tau_cuts_channel)
#define psi_sum_tau_channel_weight (psi.sum_tau_channel_weight)
#define psi_sum_tau_channel_weight2 (psi.sum_tau_channel_weight2)
#define psi_n_tau_MC_channel (psi.n_tau_MC_channel)
#define psi_tau_MC_map (psi.tau_MC_map)
#define psi_tau_MC_tau_channel (psi.tau_MC_tau_channel)
#define psi_tau_MC_tau_alpha (psi.tau_MC_tau_alpha)
#define psi_tau_MC_tau_beta (psi.tau_MC_tau_beta)
#define psi_tau_MC_tau_gamma (psi.tau_MC_tau_gamma)




#define psi_MC_tau (psi.MC_tau)
#define psi_MC_x_dipole_mapping (psi.MC_x_dipole_mapping)
#define psi_MC_x_dipole (psi.MC_x_dipole)


// needed ???
#define psi_tau_MC_tau_counts_channel (psi.tau_MC_tau_counts_channel)
#define psi_tau_MC_tau_cuts_channel (psi.tau_MC_tau_cuts_channel)
#define psi_sum_tau_MC_tau_channel_weight (psi.sum_tau_MC_tau_channel_weight)
#define psi_sum_tau_MC_tau_channel_weight2 (psi.sum_tau_MC_tau_channel_weight2)
////

#define psi_x1x2_opt_end (psi.x1x2_opt_end)
#define psi_x1x2_channel (psi.x1x2_channel)
#define psi_x1x2_alpha (psi.x1x2_alpha)
#define psi_x1x2_beta (psi.x1x2_beta)
#define psi_x1x2_counts_channel (psi.x1x2_counts_channel)
#define psi_x1x2_cuts_channel (psi.x1x2_cuts_channel)
#define psi_sum_x1x2_channel_weight (psi.sum_x1x2_channel_weight)
#define psi_sum_x1x2_channel_weight2 (psi.sum_x1x2_channel_weight2)


#define psi_z1z2_opt_end (psi.z1z2_opt_end)
#define psi_z1z2_channel (psi.z1z2_channel)
#define psi_z1z2_alpha (psi.z1z2_alpha)
#define psi_z1z2_beta (psi.z1z2_beta)
#define psi_z1z2_counts_channel (psi.z1z2_counts_channel)
#define psi_z1z2_cuts_channel (psi.z1z2_cuts_channel)
#define psi_sum_z1z2_channel_weight (psi.sum_z1z2_channel_weight)
#define psi_sum_z1z2_channel_weight2 (psi.sum_z1z2_channel_weight2)


#define psi_i_alpha_it (psi.i_alpha_it)
#define psi_alpha_it (psi.alpha_it)
#define psi_diff_w (psi.diff_w)
#define psi_Dmin (psi.Dmin)
#define psi_MCweight_lc_min (psi.MCweight_lc_min)
#define psi_a_reserved_min (psi.a_reserved_min)


#define psi_n_events_MC_opt (psi.n_events_MC_opt)
#define psi_opt_n_events_min (psi.opt_n_events_min)


#define psi_switch_n_events_opt (psi.switch_n_events_opt)

#define psi_boost (psi.boost)
#define psi_x_pdf (psi.x_pdf)

#define psi_z_coll (psi.z_coll)
#define psi_all_xz_coll_pdf (psi.all_xz_coll_pdf)
#define psi_g_z_coll (psi.g_z_coll)

#define psi_tau_0_num (psi.tau_0_num)

#define psi_i_gen (psi.i_gen)
#define psi_i_rej (psi.i_rej)
#define psi_i_acc (psi.i_acc)
#define psi_i_nan (psi.i_nan)
#define psi_i_tec (psi.i_tec)

// shift completely to contribution_set !!!

#define psi_o_map (psi.o_map)
#define psi_no_map (psi.no_map)
#define psi_o_prc (psi.o_prc)
#define psi_no_prc (psi.no_prc)

#define psi_process_class (psi.process_class)
#define psi_subprocess (psi.subprocess)
#define psi_type_perturbative_order (psi.type_perturbative_order)
#define psi_type_contribution (psi.type_contribution)
#define psi_type_correction (psi.type_correction)
#define psi_contribution_order_alpha_s (psi.contribution_order_alpha_s)
#define psi_contribution_order_alpha_e (psi.contribution_order_alpha_e)
#define psi_contribution_order_interference (psi.contribution_order_interference)
#define psi_phasespace_order_alpha_s (psi.phasespace_order_alpha_s)
#define psi_phasespace_order_alpha_e (psi.phasespace_order_alpha_e)
#define psi_phasespace_order_interference (psi.phasespace_order_interference)

#define psi_symmetry_factor (psi.symmetry_factor)

// until here...

#define psi_random_seed (psi.random_seed)

#define psi_RA_singular_region (psi.RA_singular_region)
#define psi_RA_singular_region_name (psi.RA_singular_region_name)
#define psi_RA_singular_region_list (psi.RA_singular_region_list)
#define psi_RA_techcut (psi.RA_techcut)
#define psi_RA_x_a (psi.RA_x_a)


#define psi_cut_technical (psi.cut_technical)

#define psi_QT_qt2 (psi.QT_qt2)
#define psi_QT_qt (psi.QT_qt)
#define psi_QT_y (psi.QT_y)
#define psi_QT_jacqt2 (psi.QT_jacqt2)
#define psi_QT_random_qt2 (psi.QT_random_qt2)

#define psi_QT_random_z1 (psi.QT_random_z1)
#define psi_QT_random_z2 (psi.QT_random_z2)
#define psi_QT_random_qt (psi.QT_random_qt)

#define psi_zx_pdf (psi.zx_pdf)
#define psi_xz_pdf (psi.xz_pdf)
#define psi_zz_pdf (psi.zz_pdf)
#define psi_z_pdf (psi.z_pdf)
#define psi_zall_pdf (psi.zall_pdf)
#define psi_QT_g_z1 (psi.QT_g_z1)
#define psi_QT_g_z2 (psi.QT_g_z2)
#define psi_QT_eps (psi.QT_eps)
#define psi_QT_random_IS (psi.QT_random_IS)
#define psi_QT_g_IS_ (psi.QT_g_IS_)

#define psi_do_resummation (psi.do_resummation)
#define psi_Qres (psi.Qres)
#define psi_dynamical_Qres (psi.dynamical_Qres)
#define psi_Qres_prefactor (psi.Qres_prefactor)

/*
#define psi_n_gen (psi.n_gen)
#define psi_n_rej (psi.n_rej)
#define psi_n_acc (psi.n_acc)
*/

#define psi_user_switch_name (psi.user.switch_name)
#define psi_user_switch_value (psi.user.switch_value)
#define psi_user_switch_map (psi.user.switch_map)
#define psi_user_cut_name (psi.user.cut_name)
#define psi_user_cut_value (psi.user.cut_value)
#define psi_user_cut_map (psi.user.cut_map)
#define psi_user_int_name (psi.user.int_name)
#define psi_user_int_value (psi.user.int_value)
#define psi_user_int_map (psi.user.int_map)
#define psi_user_double_name (psi.user.double_name)
#define psi_user_double_value (psi.user.double_value)
#define psi_user_double_map (psi.user.double_map)



/*
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
#define psi_ (psi.)
*/

