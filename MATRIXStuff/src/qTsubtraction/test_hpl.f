      program main
      double precision u,v
      real*8 HZ1,HZ2,HZ3,HZ4,GYZ1,GYZ2,GYZ3,GYZ4
      real*8 HZ1a,HZ2a,HZ3a,HZ4a,HYZ1a,HYZ2a,HYZ3a,HYZ4a
      dimension HZ1(0:1),HZ2(0:1,0:1),HZ3(0:1,0:1,0:1), 
     $          HZ4(0:1,0:1,0:1,0:1) 
      dimension GYZ1(0:3),GYZ2(0:3,0:3),GYZ3(0:3,0:3,0:3), 
     $          GYZ4(0:3,0:3,0:3,0:3) 
      common/HPL2OUT/
     $ HZ1a(0:1),HZ2a(0:1,0:1),HZ3a(0:1,0:1,0:1), 
     $           HZ4a(0:1,0:1,0:1,0:1), 
     $ HYZ1a(0:3),HYZ2a(0:3,0:3),HYZ3a(0:3,0:3,0:3), 
     $            HYZ4a(0:3,0:3,0:3,0:3)       
      u = 0.622846310578366d0
      v = 0.000195800224171579d0
      call tdhpl(u,v,4,GYZ1,GYZ2,GYZ3,GYZ4,HZ1,HZ2,HZ3,HZ4)


      print*, "v, u = ",v,u
      print*, "G(0) = ", GYZ1(0)
      print*, "G(1) = ", GYZ1(1)
      print*, "G(2) = ", GYZ1(2)
      print*, "G(3) = ", GYZ1(3)
      print*, "G(0,0) = ", GYZ2(0,0)
      print*, "G(1,0) = ", GYZ2(1,0)
      print*, "G(2,0) = ", GYZ2(2,0)
      print*, "G(3,0) = ", GYZ2(3,0)
      print*, "G(0,0) = ", GYZ2(0,1)
      print*, "G(0,0) = ", GYZ2(1,1)
      print*, "G(0,0) = ", GYZ2(2,1)
      print*, "G(0,0) = ", GYZ2(3,1)
      print*, "H(0,v) = ", HZ1(0)
      print*, "H(1,v) = ", HZ1(1)
      print*, "H(0,0,v) = ", HZ2(0,0)
      print*, "H(0,1,v) = ", HZ2(0,1)
      print*, "H(1,0,v) = ", HZ2(1,0)
      print*, "H(1,1,v) = ", HZ2(1,1)
      end
