CC   Resummation coefficients needed both in the counterterm and in 
CC   the Hst term

      double precision A1q,B1q,A2q,B2q,Kappa,beta0,C1qqdelta,Delta2qq,
     &                 deltaqqqq,D0qqqq,D1qqqq,
     &                 A1g,B1g,A2g,B2g,C1ggdelta,Delta2gg,
     &                 deltagggg,D0gggg,D1gggg,
     &                 gammaQ1
      common/rescoeff/A1q,B1q,A2q,B2q,Kappa,beta0,C1qqdelta,Delta2qq,
     &                 deltaqqqq,D0qqqq,D1qqqq,
     &                 A1g,B1g,A2g,B2g,C1ggdelta,Delta2gg,
     &                 deltagggg,D0gggg,D1gggg,
     &                 gammaQ1
