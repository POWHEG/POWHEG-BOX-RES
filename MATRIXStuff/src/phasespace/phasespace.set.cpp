#include "header.hpp"

////////////////////
//  constructors  //
////////////////////
phasespace_set::phasespace_set(){}
phasespace_set::phasespace_set(inputparameter_set & isi, contribution_set & _csi){
  Logger logger("phasespace_set::phasespace_set (isi)");
  logger << LOG_DEBUG << "called" << endl;

  csi = &_csi;
 
  coll_choice = isi.coll_choice;

  for (int i_p = 1; i_p < csi->type_parton[0].size(); i_p++){
    logger << LOG_DEBUG << "type_parton[" << i_p << "] = " << csi->type_parton[0][i_p] << endl;
  }

  // !!! must be changed: type_parton is different in oss and pss !!! should be solved now...
  hcf = pow(0.197326968E-13, 2.) * 1.e+39 / (2. * pow(2. * pi, 3 * csi->n_particle - 4));

  xb_max = intpow(2, csi->n_particle + 2);
  xb_max_dipoles = intpow(2, csi->n_particle + 2 - 1);
  xb_out_dipoles = xb_max_dipoles - 4;


  MC_opt_end = 0; // to avoid 'uninitialized' warning !!!
  end_optimization = 0; // to avoid 'uninitialized' warning !!!
  

  
  switch_off_random_generator = isi.switch_off_random_generator;
  switch_IO_generator = isi.switch_IO_generator;
  n_random_per_psp = 0;

  // number of degrees of freedom of csi->n_particle phasespace:
  no_random = 3 * csi->n_particle - 4;
  // 'random_MC' should replace 'r' at some point !!!

  //  r remains the random-number vector used in phase-space generation files (psg). -> not any more !!!
  //
  /*///
  r.resize(no_random + 1);
  random_MC.resize(no_random + 1);
  */


  n_shift_run = isi.zwahl;


  /*
  //  n_random_MC: 1 for the channel selection, (3n - 4) for the n-particle phasespace:
  n_random_MC = 1 + no_random;
  ///  if (switch_IS_MC != -1){n_random_MC += no_random;}
  logger << LOG_INFO << "switch_MC      = " << setw(3) << switch_MC << "   " << setw(20) << "n_random_MC" << " = " << n_random_MC << endl;
  // Should be 0 if switch_MC == -1 !!!
  n_random_per_psp += n_random_MC;

  //  n_random_MC_tau: 1 for the MC_tau channel selection:
  //  n_random_MC_tau: 2 set instead in order to reproduce old results !!!
  n_random_MC_tau = 2;
  logger << LOG_INFO << "switch_MC_tau  = " << setw(3) << switch_MC_tau << "   " << setw(20) << "n_random_MC_tau" << " = " << n_random_MC_tau << endl;
  // Should be 0 if switch_MC_tau == -1 !!!
  random_MC_tau.resize(n_random_MC_tau);
  n_random_per_psp += n_random_MC_tau;
  
  //  n_random_tau: 1 for the tau IS selection, 1 random number for the tau value:
  n_random_tau = 2;
  logger << LOG_INFO << "switch_IS_tau  = " << setw(3) << switch_IS_tau << "   " << setw(20) << "n_random_tau   " << " = " << n_random_tau << endl;
  // Should be 1 if switch_IS_tau == -1 !!!
  random_tau.resize(n_random_tau);
  n_random_per_psp += n_random_tau;

  //  n_random_x1x2: 1 for the x1x2 IS selection, 1 random number for the x1x2 value:
  n_random_x12 = 2;
  logger << LOG_INFO << "switch_IS_x1x2 = " << setw(3) << switch_IS_x1x2 << "   " << setw(20) << "n_random_x12  " << " = " << n_random_x12 << endl;
  // Should be 1 if switch_IS_tau == -1 !!!
  random_x12.resize(n_random_x12);
  // number of random numbers for x12 mapping:
  n_random_per_psp += n_random_x12;

  random_z.resize(3, vector<double> (2));
  if (csi->class_contribution_collinear){
    //  n_random_z: 1 for the z1/2 IS selection, 1 random number for the z1/2 value:
    n_random_z = 2;
    logger << LOG_INFO << "switch_IS_z1z2 = " << setw(3) << switch_IS_z1z2 << "   " << setw(20) << "n_random_z     " << " = " << n_random_z << endl;
    for (int i_z = 1; i_z < 3; i_z++){
      random_z[i_z].resize(n_random_z);
      // number of random numbers for z1/2 mapping:
      n_random_per_psp += n_random_z;
    }
  }

  logger << LOG_INFO << "n_random_per_psp = " << n_random_per_psp << endl;
*/
  
  RA_x_a = 0; // to avoid warning !!!

  ///  initialization_random_number_generator(isi);

  /*
  rng.initialization(n_random_per_psp, isi.zwahl, *this);
  */
  /*
  logger << LOG_DEBUG << "isi.MCweight_in_directory = " << isi.MCweight_in_directory << endl;
  logger << LOG_DEBUG << "csi->subprocess = " << csi->subprocess << endl;
  string dir_MCweights_in_contribution = "../../../../" + isi.MCweight_in_directory + "/weights";
  random_manager = randommanager(dir_MCweights_in_contribution, *this);
  */
  
  ///  random_manager = randommanager(dir_MCweights_in_contribution, *this);
  //  random_manager.psi = this;
  //  random_manager.readin_weights();
  //  for (int i = 0; i < isi.zwahl; i++){x = ran(sran);}


  o_map.resize(1);
  o_map[0].resize(3 + csi->n_particle, 0);
  //  o_map.resize(3 + csi->n_particle, 0);

  o_prc.resize(1);
  o_prc[0].resize(3 + csi->n_particle, 0);

  switch_MC = isi.switch_MC;
  switch_MC_tau = isi.switch_MC_tau;
  switch_MC_x_dipole = isi.switch_MC_x_dipole;

  switch_IS_MC = isi.switch_IS_MC;
  switch_IS_tau = isi.switch_IS_tau;
  switch_IS_x1x2 = isi.switch_IS_x1x2;
  switch_IS_z1z2 = isi.switch_IS_z1z2;
  switch_IS_qTres = isi.switch_IS_qTres;
  //  further modifications needed in this file to re-activate qT mapping in resummation
  
  switch_n_events_opt = isi.switch_n_events_opt;

  switch_use_alpha_after_IS = isi.switch_use_alpha_after_IS;
  switch_step_mode_grid = isi.switch_step_mode_grid;

  switch_off_RS_mapping = isi.switch_off_RS_mapping;

  switch_off_RS_mapping_ij_k = isi.switch_off_RS_mapping_ij_k;
  switch_off_RS_mapping_ij_a = isi.switch_off_RS_mapping_ij_a;
  switch_off_RS_mapping_ai_k = isi.switch_off_RS_mapping_ai_k;
  switch_off_RS_mapping_ai_b = isi.switch_off_RS_mapping_ai_b;
  
  switch_resummation = isi.switch_resummation;
  
  tau_0_num.resize(1, 1.);

  i_gen = 0;
  i_rej = 0;
  i_acc = 0;
  i_nan = 0;
  i_tec = 0;

  last_step_mode = 0;

  user = isi.user;
    
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


/*
void phasespace_set::initialization_random_number_generator(inputparameter_set & isi){
  Logger logger("phasespace_set::initialization_random_number_generator");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  //////////////////////////////////////////////////
  //  random-number initialization determination  //
  //////////////////////////////////////////////////

  double x = 0.;
  sran.resize(3);
  if (csi->subprocess != ""){
    // new implementation based on trivial prime number generator:
    int random_per_psp = 3 * csi->n_particle - 4 + 2 + 2;
    int random_sheet = isi.zwahl / random_per_psp;
    int max_n_prime_number = (random_sheet + 1) * 3;
    logger << LOG_DEBUG << "max_n_prime_number = " << max_n_prime_number << endl;
    vector<int> prime_number(1, 2);
    logger << LOG_DEBUG << "random_sheet no. 0" << endl;
    logger << LOG_DEBUG << "prime_number[" << setw(4) << prime_number.size() - 1 << "] = " << setw(10) << prime_number[prime_number.size() - 1] << "   sqrt = " << sqrt(prime_number[prime_number.size() - 1]) << endl;
    int number = 1;
    while (prime_number.size() < max_n_prime_number){
      number += 2;
      double max_number = sqrt(number);
      int flag = 0;
      for (int i_n = 0; i_n < prime_number.size(); i_n++){
	if (prime_number[i_n] > max_number){break;}
	if (number % prime_number[i_n] == 0){flag = 1; break;} 
      }
      if (!flag){
	if (prime_number.size() % 3 == 0){logger << LOG_DEBUG << "random_sheet no. " << prime_number.size() / 3 << endl;}
	prime_number.push_back(number);
	logger << LOG_DEBUG << "prime_number[" << setw(4) << prime_number.size() - 1 << "] = " << setw(10) << prime_number[prime_number.size() - 1] << "   sqrt = " << sqrt(prime_number[prime_number.size() - 1]) << endl;
      }
    }

    logger << LOG_DEBUG << "selected random_sheet: " << random_sheet << endl;
    sran[0] = sqrt(prime_number[prime_number.size() - 3]);
    sran[1] = sqrt(prime_number[prime_number.size() - 2]);
    sran[2] = sqrt(prime_number[prime_number.size() - 1]);

    for (int i_z = 0; i_z < 3; i_z++){sran[i_z] = sran[i_z] - int(sran[i_z]);}

    int zwahl_shift = isi.zwahl % random_per_psp;

    logger << LOG_DEBUG << "random_per_psp = " << random_per_psp << endl;
    logger << LOG_DEBUG << "isi.zwahl      = " << isi.zwahl << endl;
    logger << LOG_DEBUG << "random_sheet   = " << random_sheet << endl;
    
    for (int i = 0; i < zwahl_shift; i++){x = ran(sran);}
    logger << LOG_DEBUG << "x              = " << x << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/


void phasespace_set::calculate_g_tot(){
  Logger logger("phasespace_set::calculate_g_tot");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  ///  logger << LOG_INFO << "g_MC = " << g_MC << endl;
  /*
  for (int j = 0; j < MC_phasespace.alpha.size(); j++){
    if (munich_isnan(MC_phasespace.alpha[j])){
      ///	logger << LOG_INFO << "MC_phasespace.g_channel[" << j << "] = " << MC_phasespace.g_channel[j] << "   MC_phasespace.alpha[" << j << "] = " << MC_phasespace.alpha[j] << endl;
    }
  }
  */
  //  logger << LOG_INFO << "g_pdf = " << g_pdf << endl;
  //  logger << LOG_INFO << "g_tot = " << g_tot << endl;
  //  logger << LOG_INFO << "g_MC = " << g_MC << endl;
  
  g_MC = 0.;
  for (int j = 0; j < MC_phasespace.alpha.size(); j++){g_MC += MC_phasespace.g_channel[j] * MC_phasespace.alpha[j];}
  //  for (int j = 0; j < MC_phasespace.alpha.size(); j++){logger << LOG_DEBUG_VERBOSE << "MC_phasespace.alpha[" << j << "] = " << MC_phasespace.alpha[j] << endl;}
  //  for (int j = 0; j < MC_phasespace.g_channel.size(); j++){logger << LOG_DEBUG_VERBOSE << "MC_phasespace.g_channel[" << j << "] = " << MC_phasespace.g_channel[j] << endl;}
  
  if (g_global_NWA != 1.){g_MC *= g_global_NWA;} 
  if (coll_choice > 0){g_tot = g_MC * g_pdf;}
  else {g_tot = g_MC;}
  if (switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 3){g_tot = g_tot * MC_g_IS_global;}

  logger << LOG_DEBUG_VERBOSE << setw(20) << "MC_tau.channel" << " = " << MC_tau.channel << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "g_MC" << " = " << setprecision(20) << setw(28) << g_MC << "   " << double2hexastr(g_MC) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "g_pdf" << " = " << setprecision(20) << setw(28) << g_pdf << "   " << double2hexastr(g_pdf) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "g_tot" << " = " << setprecision(20) << setw(28) << g_tot << "   " << double2hexastr(g_tot) << endl;
  logger << LOG_DEBUG_VERBOSE << setw(20) << "xbs_all[0][0]" << " = " << setprecision(20) << setw(28) << xbs_all[0][0] << "   " << double2hexastr(xbs_all[0][0]) << endl;
  
  ps_factor = hcf / (g_tot * xbs_all[0][0]);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::calculate_IS(){
  Logger logger("phasespace_set::calculate_IS");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  
  logger << LOG_DEBUG_VERBOSE << "i_gen = " << setw(12) << i_gen << "   i_acc = " << setw(12) << i_acc << "   i_rej = " << setw(12) << i_rej << "   i_nan = " << setw(12) << i_nan << "   i_tec = " << setw(12) << i_tec << endl;

  logger << LOG_DEBUG << "NEW RNG -- get_random_number_set" << endl << rng.output_random_generator_set() << endl;
  rng.get_random_number_set();
  logger << LOG_DEBUG << "NEW RNG -- random_number_set   -- n_random_per_psp = " << n_random_per_psp << endl << rng.output_random_number_set() << endl;


  xbp_all = start_xbp_all;
  xbs_all = start_xbs_all;
  xbsqrts_all = start_xbsqrts_all;
  i_gen++;

  if (coll_choice != 0){
    logger << LOG_DEBUG_VERBOSE << "start_xbs_all[0][xb_max - 4 = " << xb_max - 4 << "] = " << start_xbs_all[0][xb_max - 4] << endl;

    if (start_xbs_all[0][xb_max - 4] != 0.){
      if (switch_IS_x1x2 != 0){
	calculate_initial_tau_fixed_x1x2_IS();
      }
      else if (switch_IS_x1x2 == 0){
	calculate_initial_tau_fixed_x1x2();
      }
      else {
	logger << LOG_FATAL << "No valid  tau_fixed  ---  " << "switch_IS_x1x2 = " << switch_IS_x1x2 << "  combination." << endl;
	exit(1);
      }
     }
    else {
      if (switch_IS_tau != 0 && switch_IS_x1x2 != 0){
	calculate_initial_tau_IS_x1x2_IS();
      }
      else if (switch_IS_tau != 0 && switch_IS_x1x2 == 0){
	calculate_initial_tau_IS_x1x2();
      }
      else if (switch_IS_tau == 0 && switch_IS_x1x2 != 0){
	calculate_initial_tau_x1x2_IS();
      }
      else if (switch_IS_tau == 0 && switch_IS_x1x2 == 0){
	calculate_initial_tau_x1x2();
      }
      else {
	logger << LOG_FATAL << "No valid  switch_IS_tau = " << switch_IS_tau << "  ---  " << "switch_IS_x1x2 = " << switch_IS_x1x2 << "  combination." << endl;
	exit(1);
      }
    }

    boost = (x_pdf[1] - x_pdf[2]) / (x_pdf[1] + x_pdf[2]);
    g_pdf = g_tau * g_x1x2;
    logger << LOG_DEBUG_VERBOSE << "g_pdf = " << g_pdf << endl;
  }

  /*
  logger << LOG_DEBUG << "OLD RNG -- channel and channel_phasespace -- no_random = " << no_random << "   r.size() = " << r.size() << endl;
  ///  for (int i_r = 0; i_r < 3; i_r++){logger << LOG_DEBUG << "sran[" << setw(2) << i_r << "] = " << sran[i_r] << endl;}
  randomvector(sran, no_random, r);
  for (int i_r = 0; i_r < r.size(); i_r++){logger << LOG_DEBUG << "r[" << setw(2) << i_r << "] = " << r[i_r] << endl;}

  for (int i_r = 0; i_r < r.size(); i_r++){r[i_r] = 0.;}
  logger << LOG_DEBUG << "NEW RNG: rng.access_random_number(" << r.size() << ")" << endl;
  /*
  r = rng.access_random_number(no_random + 1);
  */
  /**//*
  r = rng.access_random_number(1);
  r.resize(no_random + 1);
  /**//*
  
  //  r = rng.access_random_number(r.size());
  for (int i_r = 0; i_r < r.size(); i_r++){logger << LOG_DEBUG << "r[" << setw(2) << i_r << "] = " << r[i_r] << endl;}


  
  // remove MC_channel !!!
  if (r.size() > 0){for (int j = 0; j < MC_phasespace.beta.size(); j++){if (r[0] <= MC_phasespace.beta[j]){MC_phasespace.channel = j; break;}}}
  //  if (r.size() > 0){for (int j = 0; j < MC_phasespace.beta.size(); j++){if (r[0] <= MC_phasespace.beta[j]){MC_channel = j; MC_phasespace.channel = j; break;}}}
  */
  
  double temp_random_MC_channel = rng.access_random_number();
  for (int j = 0; j < MC_phasespace.beta.size(); j++){if (temp_random_MC_channel <= MC_phasespace.beta[j]){MC_phasespace.channel = j; break;}}

  
  //  MC_channel_phasespace = MC_channel;
  MC_g_IS_global = 1.;

  // more informative name for 'weight_IS', e.g. switch_IS_mode_phasespace ???
  logger << LOG_DEBUG_VERBOSE << "MC_channel_phasespace = " << MC_channel_phasespace << endl;
  if (switch_IS_mode_phasespace == 0){
    random_manager.counter_mode_12 = 0;
    for (int k = 0; k < no_random; k++){
      r[k + 1] = rng.access_random_number();
    }
  }
  else if (switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 2){
    random_manager.counter_mode_12 = 0;
    ///    double g_IS_temp;
    for (int k = 0; k < no_random; k++){
      //      r[k + 1] = rng.access_random_number();
      // For debugging only !!! - does not work so far...
      r[k + 1] = random_manager.random_psp[MC_phasespace.channel * no_random + k].get_random();
      ///      r[k + 1] = phasespace_randoms[MC_phasespace.channel * no_random + k]->get_random();
      //      logger << LOG_INFO << "MC_phasespace.channel = " << MC_phasespace.channel << "   r[" << k + 1 << "] = " << r[k + 1] << endl;
      /*
     ///      phasespace_randoms[MC_phasespace.channel * no_random + k]->get_random(r[k + 1], g_IS_temp);
     ///      MC_g_IS_global *= g_IS_temp;
      */
    }
  }
  //  For switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 2,
  //  r[1 ... no_random] are not used (random numbers are determied directly).
  
  ///   for (int i_r = 0; i_r < r.size(); i_r++){logger << LOG_DEBUG << "r[" << setw(2) << i_r << "] = " << r[i_r] << endl;}
 
  if (o_map[0][1] == 2 && o_map[0][2] == 1){swap(xbp_all[0][1], xbp_all[0][2]);}

  /*
  for (int i_x = 0; i_x < xbp_all[0].size(); i_x++){
    //    logger << LOG_DEBUG << "start_xbp[" << setw(3) << i_x << "] = " << start_xbp_all[0][i_x] << " " << setw(23) << setprecision(15) << start_xbsqrts_all[0][i_x] << " " << setw(23) << setprecision(15) << start_xbs_all[0][i_x] << endl;
    //    logger << LOG_DEBUG << "xbp[" << setw(3) << i_x << "] = " << xbp_all[0][i_x] << " " << setw(23) << setprecision(15) << xbsqrts_all[0][i_x] << " " << setw(23) << setprecision(15) << xbs_all[0][i_x] << endl;
    if (xbs_all[0][i_x] != 0. || xbp_all[0][i_x] != nullvector){
      logger << LOG_DEBUG_VERBOSE << "xbp[" << setw(3) << i_x << "] = " << xbp_all[0][i_x] << " " << setw(23) << setprecision(15) << xbsqrts_all[0][i_x] << " " << setw(23) << setprecision(15) << xbs_all[0][i_x] << endl;
    }
  }
  */
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// Argument redundant !!!
void phasespace_set::calculate_IS_RA(vector<dipole_set> & _dipole){
  Logger logger("phasespace_set::calculate_IS_RA");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  
  for (int i_a = 0; i_a < _dipole.size(); i_a++){
    if (MC_phasespace.channel < _dipole[i_a].sum_channel()){
      RA_x_a = i_a;
      if (i_a == 0){MC_channel_phasespace = MC_phasespace.channel;}
      else {MC_channel_phasespace = MC_phasespace.channel - MC_sum_channel_phasespace[i_a - 1];}
      break;
    }
  }
  int temp_zero = 0;
  if (RA_x_a > 0){temp_zero = _dipole[RA_x_a - 1].sum_channel();}
  logger << LOG_DEBUG_VERBOSE << "MC_phasespace.channel = " << MC_phasespace.channel << "   RA_x_a = " << RA_x_a << "   channel[" << RA_x_a << "] = " << MC_phasespace.channel - temp_zero << endl;
  logger << LOG_DEBUG_VERBOSE << "MC_channel_phasespace = " << MC_channel_phasespace << "   RA_x_a = " << RA_x_a << "   channel[" << RA_x_a << "] = " << MC_phasespace.channel - temp_zero << endl;

  //  MC_channel_phasespace  doesn't seem to be used !!!
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

/*
//  new rng:
//  should not be used any longer ...
void phasespace_set::calculate_IS_CX(){
  Logger logger("phasespace_set::calculate_IS_CX");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;

  //  for (int i_x = 0; i_x < 3; i_x++){xz_pdf[i_x][0] = x_pdf[i_x];}
  for (int i_z = 1; i_z < 3; i_z++){
    QT_random_z[i_z]->get_random(QT_random_IS, QT_g_IS_);
    z_coll[i_z] = pow(x_pdf[i_z], QT_eps + (1. - QT_eps) * QT_random_IS);
    //    g_z_coll[i_z] = -1. / (log(x_pdf[i_z]) * (1. - QT_eps)) * QT_g_IS_;
    g_z_coll[i_z] = -1. / (log(x_pdf[i_z]) * (1. - QT_eps));
    g_pdf *= QT_g_IS_;
    //    cout << "z_coll[" << i_z << "] = " << z_coll[i_z] << endl;
    //    cout << "g_z_coll[" << i_z << "] = " << g_z_coll[i_z] << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

void phasespace_set::calculate_IS_QT(){
  Logger logger("phasespace_set::calculate_IS_QT");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;


  random_qTres = rng.access_random_number(n_random_qTres);
  for (int j = 0; j < IS_qTres.beta.size(); j++){if (random_qTres[0] <= IS_qTres.beta[j]){IS_qTres.channel = j; break;}}
  QT_random_qt2 = (double(IS_qTres.channel) + random_x12[1]) / double(IS_qTres.beta.size());
  g_qTres = 1. / (IS_qTres.alpha[IS_qTres.channel] * IS_qTres.alpha.size());
  g_pdf *= g_qTres;

  /* ///
  QT_random_qt->get_random(QT_random_qt2,QT_g_IS_);
  //  cout << "qt: QT_random_qt2 = " << setw(23) << setprecision(15) << QT_random_qt2 << "   QT_g_IS_ = " << setw(23) << setprecision(15) << QT_g_IS_ << endl;
  g_pdf *= QT_g_IS_;
  */
  
  if (type_contribution == "CT" ||
      type_contribution == "CT2"){
    // the (Bessel transformed) large logarithms vanish up to double precision accuracy for arguments larger than 40 -> use this as upper bound
    // 1.261 ~ b0*b0
    double maxqt2;
    double Qres_tmp=0.0, Qres_lower, Qres_upper;
    
    // Qres appears at two places here: as the reference value for the lower and for the upper qT cut
    // conceptually, these are partially unrelated
    // in fixed order computations, we have to use Qres as the lower cut off scale, because we precompute the qT integrals
    // in resummed computations, we explicitly integrate over qT, so this is unnecessary; in fact
    // it is more efficient to use mF as the lower reference scale, as qT/mF determine the universal limit
    // the upper cut off scale should alway be given by Qres, as otherwise qT/Qres can become huge, resulting in numerical
    // problems in the counterterm weight computation
    
    // Qres != mF does not work at the moment; has to be implemented in process specific cut files
    logger << LOG_DEBUG_VERBOSE << "switch_resummation = " << switch_resummation << endl;
    logger << LOG_DEBUG_VERBOSE << "Qres = " << Qres << endl;

    assert(Qres == 0. || !switch_resummation);
    

    if (dynamical_Qres) {
      Qres_tmp = Qres_prefactor*xbsqrts_all[0][0];
    } else {
      Qres_tmp = Qres;
    }
    if (Qres_tmp == 0) {
      // default choice
      Qres_tmp = xbsqrts_all[0][0];
    }
    
    Qres_upper = Qres_tmp;
    
    if (switch_resummation){
      Qres_lower = xbsqrts_all[0][0];
    } else {
      Qres_lower = Qres_tmp;
    }
    
    if (contribution_order_alpha_s[0] == 1){maxqt2 = 1.261 * 30. * 20. * Qres_upper * Qres_upper;}
    else {maxqt2 = 1.261 * 40. * 50. * Qres_upper * Qres_upper;}
    
    if (switch_qTcut == 1){
      double r0 = min_qTcut / 100.;
      //  version with cut on pT/Qres_cut
      QT_qt2 = r0 * r0 * pow(Qres_lower,2) * exp(log(maxqt2 / r0 / r0 / Qres_lower / Qres_lower) * QT_random_qt2);
//       cout << "qT=" << QT_qt2 << ", Qres=" << Qres_upper << ", " << Qres_lower << ", ratio=" << QT_qt2/Qres_upper << endl;
      QT_jacqt2 = log(maxqt2 / r0 / r0 / Qres_lower / Qres_lower) * QT_qt2;
      //  version with cut on pT/m_inv
      //    QT_qt2 = r0 * r0 * xbs_all[0][0] * exp(log(maxqt2 / r0 / r0 / xbs_all[0][0]) * QT_random_qt2);
      //    QT_jacqt2 = log(maxqt2 / r0 / r0 / xbs_all[0][0]) * QT_qt2;
    }
    else if (switch_qTcut == 2){
      // old version with cut on pT only (not relative to m_inv)
      //   qt2=min_qTcut*min_qTcut*exp(log(maxqt2/min_qTcut/min_qTcut)*random_qt2);
      //   jacqt2=log(maxqt2/min_qTcut/min_qTcut)*qt2;
      logger << LOG_FATAL << "old qT variation is deprecated" << endl;
      exit(0);
    }
  }

  else if (type_contribution == "NLL_LO" || type_contribution == "NLL_NLO" || type_contribution == "NNLL_LO" || type_contribution == "NNLL_NLO" || type_contribution == "NNLL_NNLO"){
    if (switch_resummation) {
      min_qTcut = 0.1; // ??? why is that set manually (and overwrites the set value of min_qTcut) ???
      
      double maxqt2 = pow(2 * E, 2);
      maxqt2 = pow(E, 2); // ???

      if (lambda_qt2 == 0 || lambda_qt2 == 1) { // ??? meaning of lambda_qt2 ??? does it only choose the mapping of QT_qt2 ???
	QT_qt2 = min_qTcut * min_qTcut * exp(log(maxqt2 / min_qTcut / min_qTcut) * QT_random_qt2);
	QT_jacqt2 = log(maxqt2 / min_qTcut / min_qTcut) * QT_qt2;
      }

      //  random_qt2=0;

      //  double lambda=0.0355064;
      //  double C=-1.0/lambda*(exp(-lambda*sqrt(maxqt2))-exp(-lambda*min_qTcut));
      //  double y0=-1.0/lambda/C*exp(-lambda*min_qTcut);
      ////  cout << exp(lambda*sqrt(maxqt2)) << ", " << exp(lambda*min_qTcut) << endl;
      ////  cout << C << ", " << y0 << endl;
      //  double qt=-1.0/lambda*log(-lambda*C*(random_qt2+y0));
      //  double jacqt=C*exp(lambda*qt);
      //  qt2=qt*qt;
      //  jacqt2=jacqt*2*qt;

      else {
	double y0 = 1. / (pow(maxqt2 / min_qTcut / min_qTcut, 1 - lambda_qt2) - 1);
	double C = pow(min_qTcut, 2 * (1 - lambda_qt2)) / (1 - lambda_qt2) / y0;

	QT_qt2 = pow((1 - lambda_qt2) * C * (QT_random_qt2 + y0), 1. / (1 - lambda_qt2));
	QT_jacqt2 = C * pow(QT_qt2, lambda_qt2);

	//    cout << C << ", " << y0 << ", " << (1-lambda_qt2)*C*(random_qt2+y0) << endl;
	//    cout << qt2 << ", " << maxqt2 << endl;
      }

      //  maxqt2 = 1e6;
      //  qt2 = min_qTcut * min_qTcut + random_qt2 * (maxqt2 - min_qTcut * min_qTcut);
      //  jacqt2 = (maxqt2 - min_qTcut * min_qTcut);
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void phasespace_set::output_check_tau_0(){
  Logger logger("phasespace_set::output_check_tau_0");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  if (x_pdf[0] < tau_0_num[tau_0_num.size() - 1]){
    tau_0_num.push_back(x_pdf[0]);
    logger << LOG_INFO << "tau_0 = " << setw(23) << setprecision(15) << tau_0 << "   min(" << setw(3) << tau_0_num.size() - 1 << "): " << setw(23) << setprecision(15) << tau_0_num[tau_0_num.size() - 1] << endl;
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




void phasespace_set::handling_cut_psp(){
  static Logger logger("phasespace_set::handling_cut_psp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (MC_tau.end_optimization == 0){
    MC_tau.n_rej++;
    MC_tau.n_rej_channel[MC_tau.channel]++;
  }

  // count cutted events for weight optimization of dipole mappings
  if (RA_x_a != 0) {
    if ((switch_MC_x_dipole != -1) && csi->class_contribution_CS_real){
      /*
    if ((switch_MC_x_dipole != -1) &&(type_contribution == "RA" || 
					  type_contribution == "RRA")){
      */
      if (MC_x_dipole[RA_x_a].end_optimization == 0){
        MC_x_dipole[RA_x_a].n_rej++;
        MC_x_dipole[RA_x_a].n_rej_channel[MC_x_dipole[RA_x_a].channel]++;
      }
    }
  }
  /*
  if (tau_opt_end == 0){tau_cuts_channel[tau_channel]++;}
  if (x1x2_opt_end == 0){x1x2_cuts_channel[x1x2_channel]++;}
  ///  cuts_channel[MC_phasespace.channel]++;
  */
  
  if (!IS_tau.end_optimization){
    IS_tau.n_rej++;
    IS_tau.n_rej_channel[IS_tau.channel]++;
  }
  
  if (!IS_x1x2.end_optimization){
    IS_x1x2.n_rej++;
    IS_x1x2.n_rej_channel[IS_x1x2.channel]++;
  }

  i_rej++;
  MC_phasespace.n_rej_channel[MC_phasespace.channel]++;
  
  random_manager.increase_cut_counter();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void phasespace_set::handling_techcut_psp(){
  static Logger logger("phasespace_set::handling_techcut_psp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (MC_tau.end_optimization == 0){
    MC_tau.n_acc++;
    MC_tau.n_acc_channel[MC_tau.channel]++;
  }

  // count cutted events for weight optimization of dipole mappings
  if (RA_x_a != 0) {
    if ((switch_MC_x_dipole != -1) && csi->class_contribution_CS_real){
      if (MC_x_dipole[RA_x_a].end_optimization == 0){
        MC_x_dipole[RA_x_a].n_acc++;
        MC_x_dipole[RA_x_a].n_acc_channel[MC_x_dipole[RA_x_a].channel]++;
      }
    }
  }

  if (!IS_tau.end_optimization){
    IS_tau.n_acc++;
    IS_tau.n_acc_channel[IS_tau.channel]++;
  }
  
  if (!IS_x1x2.end_optimization){
    IS_x1x2.n_acc++;
    IS_x1x2.n_acc_channel[IS_x1x2.channel]++;
  }

  i_acc++;
  MC_phasespace.n_acc_channel[MC_phasespace.channel]++;
  
  random_manager.increase_counter_n_acc();
  //  random_manager.increase_cut_counter();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

//#include "OV.phasespace.set.cpp"
//#include "OV2.phasespace.set.cpp"


/*
double phasespace_set::get_random(int x_a, int x_t, int x_i){
  static Logger logger("phasespace_set::get_random(int x_a, int x_t, int x_i)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  
  logger << LOG_DEBUG_VERBOSE << "x_a = " << x_a << "   x_t = " << x_t << "   x_i = " << x_i << "   switch_IS_mode_phasespace = " << switch_IS_mode_phasespace << endl;
  if (switch_IS_mode_phasespace == 3 || switch_IS_mode_phasespace == 4){
    double temp_r = random_manager.random_psp[map_type_to_list_MC_IS[vector<int> {x_a, x_t, x_i}]].get_random();
    return temp_r;
  }
  else if (switch_IS_mode_phasespace == 1 || switch_IS_mode_phasespace == 3){
    return r[++random_manager.counter_mode_12];
  }
  else {
    logger << LOG_DEBUG << "r.size() = " << r.size() << "   random_manager.counter_mode_12 = " << random_manager.counter_mode_12 << endl;
logger << LOG_DEBUG << "counter_mode_12 = " << random_manager.counter_mode_12 + 1 << "   r.size() = " << r.size() << "   r[" << random_manager.counter_mode_12 + 1 << "] = " << r[random_manager.counter_mode_12] << endl;
    return r[++random_manager.counter_mode_12];   
  }
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/
