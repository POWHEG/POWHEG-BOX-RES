#ifndef RANDOMMANAGER_H
#define RANDOMMANAGER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "logger.h"
#include "randomvariable.h"

//using std::vector;

class randomvariable;
class phasespace_set;

class randommanager{
public:
  randommanager();
  randommanager(phasespace_set & psi);
  ///  randommanager(vector<double> sran, std::string _weightdir, std::string _processname, phasespace_set & psi);
  ~randommanager();

  int active_optimization;
  int end_optimization;

  vector<randomvariable> random_psp;

  void register_variable(randomvariable* new_var);
  void initialization_random_psp(int x_r);
  double get_random(int x_a, int type, int number);

  void optimize(const double psp_weight, const double psp_weight2);
  void psp_IS_optimization(double & psp_weight, double & psp_weight2);
  
  void increase_cut_counter();
  void increase_counter_n_acc();

  void do_optimization_step(int events);
  void proceeding_out(std::ofstream &out_proceeding);
  void proceeding_in(int &proc, vector<std::string> &readin);
  void check_proceeding_in(int &int_end, int &proc, vector<std::string> &readin);
  void proceeding_save();

  void readin_weights();
  void writeout_weights();

  void add_var_to_queue(randomvariable* variable);
  void nancut();

  void output_used();

 
  phasespace_set *psi;
  int counter_mode_12;

  //  random_number_generator *rng;

 private:

  string weightdir;
  string processname;

  int save_end_optimization;

  vector<randomvariable*> used_queue;
  int queue_counter;
  int queue_threshold;

  vector<string> readin;

};

#endif
