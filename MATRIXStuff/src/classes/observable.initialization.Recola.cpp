#ifdef RECOLA
#include "header.hpp"
#include "definitions.phasespace.set.cxx"
//#include "../../external/recola-1.3.6/include/recola.h"

void observable_set::initialization_Recola_input(inputparameter_set & isi){
  Logger logger("observable_set::initialization_Recola_input (isi)");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  RCL_parameter = isi.RCL_parameter;
  RCL_value = isi.RCL_value;
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void observable_set::initialization_Recola_parameter(phasespace_set & psi){
  static Logger logger("observable_set::initialization_Recola_parameter");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ////////////////////////////////////
  //  N_nondecoupled determination  //
  ////////////////////////////////////

  //  string LHAPDFversion = LHAPDF::version();
#ifdef LHAPDF5
  string LHAPDFversion = "5";
#else
  string LHAPDFversion = "6";
#endif

  logger << LOG_INFO << "LHAPDFversion = " << LHAPDFversion << endl;
  logger << LOG_INFO << "LHAPDFversion.substr(0, 1) = " << LHAPDFversion.substr(0, 1) << endl;

  int LHAPDF_N_nondecoupled = LHAPDF::getNf();

  if (LHAPDFversion.substr(0, 1) == "6"){
    logger << LOG_INFO << "LHAPDF version 6 (" << LHAPDFversion << ") is used." << endl;
    N_nondecoupled = LHAPDF_N_nondecoupled;
    if (LHAPDFname.substr(0, 10) == "NNPDF21_lo"){N_nondecoupled = 6;}
  }
  else if (LHAPDFversion.substr(0, 1) == "5"){
    logger << LOG_INFO << "LHAPDF version 5 (" << LHAPDFversion << ") is used." << endl;

    logger << LOG_INFO << "LHAPDFname = " << LHAPDFname << endl;
    logger << LOG_INFO << "LHAPDF_N_nondecoupled = " << LHAPDF_N_nondecoupled << endl;
    if (LHAPDFname.substr(0, 7) == "NNPDF21"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 6;
      for (int i_s = LHAPDFname.size() - 3; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 3);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "NF3"){N_nondecoupled = 3;}
	else if (temp_Nf == "NF4"){N_nondecoupled = 4;}
	else if (temp_Nf == "NF5"){N_nondecoupled = 5;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }
   else if (LHAPDFname.substr(0, 7) == "NNPDF23"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 6;
      for (int i_s = LHAPDFname.size() - 3; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 3);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "NF3"){N_nondecoupled = 3;}
	else if (temp_Nf == "NF4"){N_nondecoupled = 4;}
	else if (temp_Nf == "NF5"){N_nondecoupled = 5;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if (LHAPDFname.substr(0, 6) == "NNPDF3"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 5;
      for (int i_s = LHAPDFname.size() - 4; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 4);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 4) = " << temp_Nf << endl;
	if (temp_Nf == "nf_3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf_4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf_6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }
   else if (LHAPDFname.substr(0, 4) == "CT14"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 5;
      for (int i_s = LHAPDFname.size() - 3; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 3);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "NF3"){N_nondecoupled = 3;}
	else if (temp_Nf == "NF4"){N_nondecoupled = 4;}
	else if (temp_Nf == "NF6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if (LHAPDFname.substr(0, 8) == "MMHT2014"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 5;
      for (int i_s = LHAPDFname.size() - 3; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 3);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "nf3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if (LHAPDFname.substr(0, 9) == "PDF4LHC15"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 5;
      for (int i_s = LHAPDFname.size() - 3; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 3);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "nf3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf6"){N_nondecoupled = 6;}
      }
  //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }

    else if (LHAPDFname.substr(0, 6) == "LUXqed"){
      logger << LOG_DEBUG << "LHAPDFname = " << LHAPDFname << endl;
      N_nondecoupled = 5;
      for (int i_s = LHAPDFname.size() - 3; i_s >=0; i_s--){
	string temp_Nf = LHAPDFname.substr(i_s, 3);
	logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 3) = " << temp_Nf << endl;
	if (temp_Nf == "nf3"){N_nondecoupled = 3;}
	else if (temp_Nf == "nf4"){N_nondecoupled = 4;}
	else if (temp_Nf == "nf6"){N_nondecoupled = 6;}
      }
      //      logger << LOG_DEBUG << "N_nondecoupled = " << N_nondecoupled << endl;
    }
    
    else {
      N_nondecoupled = LHAPDF_N_nondecoupled;
    }
  }

  if (LHAPDFname.substr(0, 8) == "MSTW2008" || LHAPDFname.substr(0, 8) == "MMHT2014"){
    for (int i_s = LHAPDFname.size() - 6; i_s >=0; i_s--){
      string temp_Nf = LHAPDFname.substr(i_s, 6);
      logger << LOG_DEBUG << "LHAPDFname.substr(" << i_s << ", 6) = " << temp_Nf << endl;
      if (temp_Nf == "nf4as5"){N_nondecoupled = 5;}
    }
  }
  
  if (N_nondecoupled != LHAPDF_N_nondecoupled){
    logger << LOG_WARN << "N_nondecoupled has been changed wrt. LHAPDF_N_nondecoupled in LHAPDF " << LHAPDFversion << endl;
  }
  logger << LOG_INFO << "N_nondecoupled = " << N_nondecoupled << endl;


  
  set_alphas_rcl(alpha_S, scale_ren, N_nondecoupled);
  //  set_alphas_rcl(alpha_S, scale_ren, N_quarks);

  double pole_UV_rcl = 0.;
  set_delta_uv_rcl(pole_UV_rcl);
  logger << LOG_INFO << "set_delta_uv_rcl(" << pole_UV_rcl << ");" << endl;

  double pole1_IR_rcl = 0.;
  double pole2_IR_rcl = 0.;
  set_delta_ir_rcl(pole1_IR_rcl, pole2_IR_rcl + (1. - switch_polenorm) * pi2_6);
  logger << LOG_INFO << "set_delta_ir_rcl(" << pole1_IR_rcl << ", " << pole2_IR_rcl + (1. - switch_polenorm) * pi2_6 << ");" << endl;

  double temp_mu_uv = scale_ren;
  set_mu_uv_rcl(temp_mu_uv);
  logger << LOG_INFO << "set_mu_uv_rcl(" << temp_mu_uv << ");" << endl;

  double temp_mu_ir = scale_ren;
  set_mu_ir_rcl(temp_mu_ir);
  logger << LOG_INFO << "set_mu_ir_rcl(" << temp_mu_ir << ");" << endl;
  
  use_dim_reg_soft_rcl();

  // boson masses
  set_pole_mass_z_rcl(msi.M[23], msi.Gamma[23]);
  set_pole_mass_w_rcl(msi.M[24], msi.Gamma[24]);
  set_pole_mass_h_rcl(msi.M[25], msi.Gamma[25]);
  // quark massese
  set_pole_mass_top_rcl(msi.M[6], msi.Gamma[6]); 
  set_pole_mass_bottom_rcl(msi.M[5], msi.Gamma[5]);
  set_pole_mass_charm_rcl(msi.M[4], msi.Gamma[4]);
  // lepton masses
  set_pole_mass_tau_rcl(msi.M[15], msi.Gamma[15]);
  set_pole_mass_muon_rcl(msi.M[13], msi.Gamma[13]);
  //  set_pole_mass_tau_rcl(1.777000e+00, psi_Gamma[15] );  //psi_M[15] = 0?

  set_complex_mass_scheme_rcl();
  
  set_dynamic_settings_rcl(1);

  ///  set_compute_ir_poles_rcl(1);


  // To use Rept1l-generated kernels throughout (should be slightly slower - to circumvent a bug in Recola 2.2.0):
  use_recola_base_rcl(0);

  // tensor reduction from DD:
  //  set_collier_mode_rcl(2);
  // tensor reduction from COLI:
  //  set_collier_mode_rcl(1);
  
  set_output_file_rcl("log/recola.parameters." + name_process + ".txt");

  set_print_level_squared_amplitude_rcl(0);
  if (Log::getLogThreshold() == 0){set_print_level_squared_amplitude_rcl(3);}
  if (Log::getLogThreshold() == 1){set_print_level_squared_amplitude_rcl(2);}
  //  LogLevel temp_LL = Log::getLogThreshold();
  //  logger << LOG_DEBUG << "logger.getLogThreshold() = " << temp_LL << endl;

    
  for (int i_rcl = 0; i_rcl < RCL_parameter.size(); i_rcl++){
    logger << LOG_DEBUG << "RCL_parameter[" << i_rcl << "] = " << RCL_parameter[i_rcl] << " = " << RCL_value[i_rcl] << endl;
  }

  logger << LOG_INFO << "msi.ew_scheme = " << msi.ew_scheme << endl;
  
  if (msi.ew_scheme == 0){
    use_alpha0_scheme_rcl(msi.alpha_e_0);
  }
  else if (msi.ew_scheme == -1){
    //    use_gfermi_scheme_and_set_gfermi_rcl(msi.G_F);
    use_gfermi_scheme_and_set_alpha_rcl(msi.alpha_e_Gmu);
 }
  else if (msi.ew_scheme == 1){
    use_gfermi_scheme_and_set_gfermi_rcl(msi.G_F);
  }
  else if (msi.ew_scheme == 2){
    use_alphaz_scheme_rcl(msi.alpha_e_MZ);
  }
    
  logger << LOG_INFO << "MUNICH:  msi.alpha_e     = " << setw(23) << setprecision(15) << msi.alpha_e << "   1 / msi.alpha_e     = " << setw(23) << setprecision(15) << 1. / msi.alpha_e << endl;
  if (msi.ew_scheme == 1 || msi.use_adapted_ew_coupling == 1){
    logger << LOG_INFO << "MUNICH:  msi.alpha_e_Gmu = " << setw(23) << setprecision(15) << msi.alpha_e_Gmu << "   1 / msi.alpha_e_Gmu = " << setw(23) << setprecision(15) << 1. / msi.alpha_e_Gmu << endl;
  }
  if (msi.ew_scheme == 0 || msi.use_adapted_ew_coupling == 0){
    logger << LOG_INFO << "MUNICH:  msi.alpha_e_0   = " << setw(23) << setprecision(15) << msi.alpha_e_0 << "   1 / msi.alpha_e_0   = " << setw(23) << setprecision(15) << 1. / msi.alpha_e_0 << endl;
  }
  if (msi.ew_scheme == 2 || msi.use_adapted_ew_coupling == 2){
    logger << LOG_INFO << "MUNICH:  msi.alpha_e_MZ  = " << setw(23) << setprecision(15) << msi.alpha_e_MZ << "   1 / msi.alpha_e_MZ  = " << setw(23) << setprecision(15) << 1. / msi.alpha_e_MZ << endl;
  }
 
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

  
void observable_set::initialization_Recola_process(phasespace_set & psi){
  static Logger logger("observable_set::initialization_Recola_process");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  initialization_Recola_parameter(psi);


  
  logger << LOG_DEBUG << "csi->type_perturbative_order         = " << csi->type_perturbative_order << endl;
  logger << LOG_DEBUG << "csi->type_contribution               = " << csi->type_contribution << endl;
  logger << LOG_DEBUG << "csi->type_correction                 = " << csi->type_correction << endl;
  logger << LOG_DEBUG << "csi->contribution_order_alpha_s      = " << csi->contribution_order_alpha_s << endl;
  logger << LOG_DEBUG << "csi->contribution_order_alpha_e      = " << csi->contribution_order_alpha_e << endl;
  logger << LOG_DEBUG << "csi->contribution_order_interference = " << csi->contribution_order_interference << endl;
  
  //  process_id = -1;
  //  int process_id = -1;
  //  int order_alpha_e_rcl =  csi->contribution_order_alpha_e;
  //  int order_alpha_s_rcl =  csi->contribution_order_alpha_s;
  //  int order_g_s_rcl =  2.*order_alpha_s_rcl;
  int id = 0;

  if (csi->type_contribution == "born" || 
      csi->type_contribution == "RT" ||
      csi->type_contribution == "RJ"){
    int type_amplitude = 1; //  1 for tree, 2 for loop-induced born
    if (user.string_value[user.string_map["model"]] == "Bornloop"){type_amplitude = 2;}
    register_RCL_subprocess(0, type_amplitude, id);

    logger << LOG_DEBUG << "csi->contribution_order_interference  = " << csi->contribution_order_interference << endl;
    if (csi->contribution_order_interference == 0){
      //      register_RCL_subprocess(0, type_amplitude, id);
      if (type_amplitude == 1){
	unselect_all_gs_powers_BornAmpl_rcl(id);
	logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
      }
      else {
	unselect_all_gs_powers_LoopAmpl_rcl(id);
	logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
      }
    }
    else if (csi->contribution_order_interference > 0){
      //      register_RCL_subprocess(0, type_amplitude, id);
      if (type_amplitude == 1){
	unselect_all_gs_powers_BornAmpl_rcl(id);
	logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
	select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
      }
      else {
	unselect_all_gs_powers_LoopAmpl_rcl(id);
	logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
	select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
      }
    }
    else {
      logger << LOG_FATAL << "No valid value: csi->contribution_order_interference = " << csi->contribution_order_interference << endl;
      exit(1);
    }
    
    register_RCL_subprocess(0, type_amplitude, id);
    
    logger << LOG_DEBUG << "process_id (bare born)           = " << id << endl;
  }
  /*
    order_alpha_s_rcl =  csi->contribution_order_alpha_s + 1; //????
    logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s + 1 << " (only to check if ME2 == 0.)" << endl;  //???
    order_g_s_rcl =  order_alpha_s_rcl * 2.;
    register_RCL_subprocess(0, 2, id);//????
    unselect_all_gs_powers_BornAmpl_rcl(id);
    logger << LOG_DEBUG << "process_id (colour correlations) = " << id << endl; //???
        int i = 0;
      while (i <= order_g_s_rcl){
      select_gs_power_BornAmpl_rcl(id, i);
        i++;}

    unselect_all_gs_powers_LoopAmpl_rcl(id);
    while (i <= order_g_s_rcl){
      select_gs_power_LoopAmpl_rcl(id, i);
      i++;}
  */
  

  else if (csi->type_contribution == "CT" ||
	   csi->type_contribution == "CJ" ||
	   csi->type_contribution == "L2CT" ||
	   csi->type_contribution == "L2CJ"){
    int type_amplitude = 1; //  1 for tree, 2 for loop-induced born
    if (csi->type_contribution == "L2CT" || 
	csi->type_contribution == "L2CJ" || 
	user.string_value[user.string_map["model"]] == "Bornloop"){type_amplitude = 2;}
    register_RCL_subprocess(0, type_amplitude, id);
    if (type_amplitude == 1){
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(" << id << ");" << endl;
      if (csi->type_correction == "QCD"){
	select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
	logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
      }
      else if (csi->type_correction == "QEW"){
	select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
	logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
      }
      else {
	logger << LOG_ERROR << "csi->type_correction = " << csi->type_correction << "   is not defined for csi->type_contribution = " << csi->type_contribution << " ." << endl;
	exit(1);
      }
    }
    else if (type_amplitude == 2){
      unselect_all_gs_powers_BornAmpl_rcl(id);
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(" << id << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
    }
  }

  else if (csi->type_contribution == "CT2" ||
	   csi->type_contribution == "CJ2"){
    int type_amplitude = 2;
    if (csi->type_correction == "QCD"){
      register_RCL_subprocess(0, type_amplitude, id);
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 2);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 2 << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
    }
    else if (csi->type_correction == "QEW"){
      logger << LOG_ERROR << "csi->type_correction = " << csi->type_correction << " has not been defined yet for csi->type_contribution = " << csi->type_contribution << " ." << endl;
      exit(1);
     }
    else if (csi->type_correction == "MIX"){
      logger << LOG_ERROR << "csi->type_correction = " << csi->type_correction << " has not been defined yet for csi->type_contribution = " << csi->type_contribution << " ." << endl;
      exit(1);
     }
    else {
      logger << LOG_ERROR << "csi->type_correction = " << csi->type_correction << " is not defined." << endl;
      exit(1);
    }
  }

  
  
  else if (csi->type_contribution == "VT" ||
	   csi->type_contribution == "VJ"){
    // identical to VA (QCD) !!!
    int type_amplitude = 2;
    register_RCL_subprocess(0, type_amplitude, id);

    if (csi->type_correction == "QCD"){
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + 1);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s + 1 << ");" << endl;
    }
    else if (csi->type_correction == "QEW"){
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
    }
    else if (csi->type_correction == "MIX"){
      /*
      select_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "select_all_gs_powers_BornAmpl_rcl(id);" << endl;
      select_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "select_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      */
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      //      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
      //      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
      //      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + 1);	
      //      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s + 1 << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
    }
    /*
    register_RCL_subprocess(0, 1, id);
    unselect_all_gs_powers_BornAmpl_rcl(id);
    select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
    */
  }
  
  else if (csi->type_contribution == "VT2" ||
	   csi->type_contribution == "VJ2"){
    int type_amplitude = 2;
    register_RCL_subprocess(0, type_amplitude, id);
    unselect_all_gs_powers_BornAmpl_rcl(id);
    logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
    unselect_all_gs_powers_LoopAmpl_rcl(id);
    logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
    select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 2);
    logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 2 << ");" << endl;
    select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);	
    logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
    // probably not used before -> A1 taken from qqVVamp
    logger << LOG_INFO << "Amplitude identifier Born^2 and loop*Born: " << id << endl; 

    // !!!
    // Workaround: register a second process for the loop-squared amplitude:
    register_RCL_subprocess(0, type_amplitude, id);
    logger << LOG_INFO << "Amplitude identifier loop-squared: " << id << endl; 
    unselect_all_gs_powers_BornAmpl_rcl(id);
    logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(" << id << ");" << endl;
    unselect_all_gs_powers_LoopAmpl_rcl(id);
    logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(" << id << ");" << endl;
    select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
    logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
  }
  
  else if (csi->type_contribution == "loop" || 
	   csi->type_contribution == "L2I" || 
	   csi->type_contribution == "L2RT" || 
	   csi->type_contribution == "L2RJ"){
    //	   csi->type_contribution == "L2CT" || 
    //	   csi->type_contribution == "L2CJ"){
     logger << LOG_DEBUG << "csi->type_contribution = " << csi->type_contribution << endl;
     //  select_OL_born_mode(QCD_order, QEW_order);
     //  ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);//amplitude squared
     //ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
     //logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
     
     register_RCL_subprocess(0, 2, id);
     unselect_all_gs_powers_BornAmpl_rcl(id);
     unselect_all_gs_powers_LoopAmpl_rcl(id);
     logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
     select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
     logger << LOG_DEBUG << "process_id = " << process_id << endl;
   }
  
   else if (csi->type_contribution == "L2VT" || 
	    csi->type_contribution == "L2VJ" || 
	    csi->type_contribution == "L2VA"){
     logger << LOG_DEBUG << "csi->type_contribution = " << csi->type_contribution << endl;

     register_RCL_subprocess(0, 2, id);
     unselect_all_gs_powers_BornAmpl_rcl(id);
     unselect_all_gs_powers_LoopAmpl_rcl(id);
     logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
     select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
     logger << LOG_DEBUG << "process_id = " << process_id << endl;
   }
  
   else if (csi->type_contribution == "CA" || 
	    csi->type_contribution == "RCA" || 
	    csi->type_contribution == "RCJ"){
    int type_amplitude = 1;
    if (user.string_value[user.string_map["model"]] == "Bornloop"){type_amplitude = 2;}

    if (csi->type_correction == "QCD"){
      if (csi->contribution_order_interference == 0){
	register_RCL_subprocess(0, type_amplitude, id);
	if (type_amplitude == 1){
	  unselect_all_gs_powers_BornAmpl_rcl(id);
	  logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
	  select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
	}
	else if (type_amplitude == 2){
	  unselect_all_gs_powers_LoopAmpl_rcl(id);
	  logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
	  select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
	}
	//process_id = register_OL_subprocess(0, type_amplitude); // temporary, to force the use of correct colour correlations !!!
      }
      else if (csi->contribution_order_interference == 1){
	/*
	ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
	logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
	process_id = register_OL_subprocess(0, type_amplitude); // temporary, to force the use of correct colour correlations !!!
	logger << LOG_DEBUG << "process_id (colour correlations) = " << process_id << endl;
	
	ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e);
	logger << LOG_DEBUG << "order_ew   set to   " << csi->contribution_order_alpha_e << endl;
	process_id = register_OL_subprocess(0, 1); // no colour correlations needed in EW corrections !!! adapt for loop-induced processes !!!
	logger << LOG_DEBUG << "process_id (bare born)           = " << process_id << endl;
	// if (process_id == 2) different libraries are needed for different Born contributions
	*/
      }
      else {
	logger << LOG_FATAL << "to be solved " << endl;
	exit(1);
      }
    }
    else if (csi->type_correction == "QEW"){
      if (csi->contribution_order_interference == 0){
	register_RCL_subprocess(0, type_amplitude, id);
	if (type_amplitude == 1){
	  unselect_all_gs_powers_BornAmpl_rcl(id);
	  logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	  select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
	}
	else if (type_amplitude == 2){
	  unselect_all_gs_powers_LoopAmpl_rcl(id);
	  logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	  select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
	}
      }
      else if (csi->contribution_order_interference == 1){
      }
      
      /*
      ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      //      ol_setparameter_int(stch("order_ew"), csi->contribution_order_alpha_e - 1);
      logger << LOG_DEBUG << "order_qcd   set to   " << csi->contribution_order_alpha_s << endl;
      process_id = register_OL_subprocess(0, 1); // temporary, to force the use of correct colour correlations !!!
    }
    else {logger << LOG_FATAL << "Wrong correction type: " << csi->type_contribution << " - " << csi->type_correction << " does not exist." << endl;}
    //    process_id = register_OL_subprocess(0, 1);
    //    process_id = register_OL_subprocess(0, 2);
    //    process_id = register_OL_subprocess(0, 11); // temporary, to force the use of correct colour correlations !!!
    */
    }
  }
  else if (csi->type_contribution == "L2CA"){
    int type_amplitude = 2;
    register_RCL_subprocess(0, type_amplitude, id);
    if (csi->type_correction == "QCD"){
      //   ol_setparameter_int(stch("order_qcd"), csi->contribution_order_alpha_s);
      if (csi->contribution_order_interference == 0){
	unselect_all_gs_powers_BornAmpl_rcl(id);
	unselect_all_gs_powers_LoopAmpl_rcl(id);
	logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
	select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - 1);	
      }
    }
  }

  else if (csi->type_contribution == "VA" || 
	   csi->type_contribution == "RVA" || 
	   csi->type_contribution == "RVJ"){
    int type_amplitude = 2;
    register_RCL_subprocess(0, type_amplitude, id);

    if (csi->type_correction == "QCD"){
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + 1);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s + 1 << ");" << endl;
    }
    else if (csi->type_correction == "QEW"){
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
    }
    else if (csi->type_correction == "MIX"){
      /*
      select_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "select_all_gs_powers_BornAmpl_rcl(id);" << endl;
      select_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "select_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      */
      unselect_all_gs_powers_BornAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_BornAmpl_rcl(id);" << endl;
      unselect_all_gs_powers_LoopAmpl_rcl(id);
      logger << LOG_DEBUG << "unselect_all_gs_powers_LoopAmpl_rcl(id);" << endl;
      //      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
      //      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
      //      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + 1);	
      //      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s + 1 << ");" << endl;
      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);	
      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
    }
    /*
    register_RCL_subprocess(0, 1, id);
    unselect_all_gs_powers_BornAmpl_rcl(id);
    select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
    */
  }

  else if (csi->type_contribution == "RA" || 
	   csi->type_contribution == "RRA" || 
	   csi->type_contribution == "RRJ" || 
	   csi->type_contribution == "L2RA"){
    logger << LOG_DEBUG_VERBOSE << "(R)RA:   (*RA_dipole).size() = " << (*RA_dipole).size() << endl;
    int type_amplitude = 1;
    if (csi->type_contribution == "L2RA" || 
	user.string_value[user.string_map["model"]] == "Bornloop"){type_amplitude = 2;}
    for (int i_a = 0; i_a < (*RA_dipole).size(); i_a++){
      //      logger << LOG_DEBUG << "i_a = " << i_a << "   id = " << id << endl;
      register_RCL_subprocess(i_a, type_amplitude, id);
      //      logger << LOG_DEBUG << "i_a = " << i_a << "   id = " << id << endl;
      if (csi->contribution_order_interference == 0){
	//	logger << LOG_DEBUG << "csi->type_correction = " << csi->type_correction << endl;
	//	logger << LOG_DEBUG << "type_amplitude = " << type_amplitude << endl;
	if (i_a == 0){
	  if (type_amplitude == 1){
	    unselect_all_gs_powers_BornAmpl_rcl(id);
	    //	    logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	    select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
	    logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
	  }
	  else if (type_amplitude == 2){
	    unselect_all_gs_powers_BornAmpl_rcl(id);
	    unselect_all_gs_powers_LoopAmpl_rcl(id);
	    //	    logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	    select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
	    logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
	  }
	  //	  logger << LOG_DEBUG << "i_a = " << i_a << "   id = " << id << endl;
	}
	else {
    	  if (csi->type_correction == "QCD"){
	    if (type_amplitude == 1){
	      unselect_all_gs_powers_BornAmpl_rcl(id);
	      //	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
	      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
	      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
	    }
	    else if (type_amplitude == 2){
	      unselect_all_gs_powers_LoopAmpl_rcl(id);
	      //	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
	      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
	      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
	    }
	  }
	  else if (csi->type_correction == "QEW"){
	    if (type_amplitude == 1){
	      unselect_all_gs_powers_BornAmpl_rcl(id);
	      //	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
	      logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
	    }
	    else if (type_amplitude == 2){
	      unselect_all_gs_powers_BornAmpl_rcl(id);
	      unselect_all_gs_powers_LoopAmpl_rcl(id);
	      //	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
	      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
	      logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
	    }
	  }
	  else if (csi->type_correction == "MIX"){
	    if ((*RA_dipole)[i_a].type_correction() == 1){
	      if (type_amplitude == 1){
		unselect_all_gs_powers_BornAmpl_rcl(id);
		//		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
		select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
		logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
	      }
	      else if (type_amplitude == 2){
		unselect_all_gs_powers_BornAmpl_rcl(id);
		unselect_all_gs_powers_LoopAmpl_rcl(id);
		//		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - 1 << endl;
		select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - 1);
		logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s - 1 << ");" << endl;
	      }
	    }
	    else if ((*RA_dipole)[i_a].type_correction() == 2){
	      if (type_amplitude == 1){
		unselect_all_gs_powers_BornAmpl_rcl(id);
		//		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
		select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s);
		logger << LOG_DEBUG << "select_gs_power_BornAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
	      }
	      else if (type_amplitude == 2){
		unselect_all_gs_powers_LoopAmpl_rcl(id);
		//		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s << endl;
		select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
		logger << LOG_DEBUG << "select_gs_power_LoopAmpl_rcl(" << id << ", " << csi->contribution_order_alpha_s << ");" << endl;
	      }
	    }
	    else {
	      logger << LOG_FATAL << "Wrong correction type: " << (*RA_dipole)[i_a].type_correction() << " does not exist." << endl;
	      exit(1);
	    }
	  }
	}
      }
      else if (csi->contribution_order_interference > 0){
	if (i_a == 0){
	  if (type_amplitude == 1){
	    unselect_all_gs_powers_BornAmpl_rcl(id);
	    logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s + csi->contribution_order_interference << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference << endl;
	    select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
	    select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
	  }
	  else if (type_amplitude == 2){
	    unselect_all_gs_powers_LoopAmpl_rcl(id);
	    logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s + csi->contribution_order_interference << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference << endl;
	    select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
	    select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
	    //	    select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s);
	  }
	}
	else {
	  if (csi->type_correction == "QCD"){
	    if (type_amplitude == 1){
	      unselect_all_gs_powers_BornAmpl_rcl(id);
	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference - 1 << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference - 1 << endl;
	      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference - 1);
	      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference - 1);
	    }
	    else if (type_amplitude == 2){
	      unselect_all_gs_powers_LoopAmpl_rcl(id);
	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference - 1 << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference - 1 << endl;
	      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference - 1);
	      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference - 1);
	    }
	  }
	  else if (csi->type_correction == "QEW"){
	    if (type_amplitude == 1){
	      unselect_all_gs_powers_BornAmpl_rcl(id);
	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference  << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference  << endl;
	      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
	      select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
	    }
	    else if (type_amplitude == 2){
	      unselect_all_gs_powers_LoopAmpl_rcl(id);
	      logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference  << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference  << endl;
	      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
	      select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
	    }
	  }
	  else if (csi->type_correction == "MIX"){
	    if ((*RA_dipole)[i_a].type_correction() == 1){
	      if (type_amplitude == 1){
		unselect_all_gs_powers_BornAmpl_rcl(id);
		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference - 1 << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference - 1 << endl;
		select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference - 1);
		select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference - 1);
	      }
	      else if (type_amplitude == 2){
		unselect_all_gs_powers_LoopAmpl_rcl(id);
		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference - 1 << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference - 1 << endl;
		select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference - 1);
		select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference - 1);
	      }
	    }
	    else if ((*RA_dipole)[i_a].type_correction() == 2){
	      if (type_amplitude == 1){
		unselect_all_gs_powers_BornAmpl_rcl(id);
		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_BornAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference  << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference  << endl;
		select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
		select_gs_power_BornAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
	      }
	      else if (type_amplitude == 2){
		unselect_all_gs_powers_LoopAmpl_rcl(id);
		logger << LOG_DEBUG << "exponent of g_s in RECOLA (select_gs_power_LoopAmpl_rcl) is set to   " << csi->contribution_order_alpha_s - csi->contribution_order_interference  << " or " << csi->contribution_order_alpha_s + csi->contribution_order_interference  << endl;
		select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s + csi->contribution_order_interference);
		select_gs_power_LoopAmpl_rcl(id, csi->contribution_order_alpha_s - csi->contribution_order_interference);
	      }
	    }
	    else {
	      logger << LOG_FATAL << "Wrong correction type: " << (*RA_dipole)[i_a].type_correction() << " does not exist." << endl;
	      exit(1);
	    }
	  }
	}

      }
      else {
	logger << LOG_FATAL << "No valid value: csi->contribution_order_interference = " << csi->contribution_order_interference << endl;
	exit(1);
      }
    }
  }
  /*
  else if (csi->type_contribution == "L2RA"){
    logger << LOG_DEBUG_VERBOSE << "(L2RA:   (*RA_dipole).size() = " << (*RA_dipole).size() << endl;
    int type_amplitude = 2;
    for (int i_a = 0; i_a < (*RA_dipole).size(); i_a++){
      if (i_a == 0){

      }
    }
    
  }
  */
  else {
    logger << LOG_ERROR << "Contribution " << csi->type_contribution << " not defined!" << endl;
    exit(1);
  }



  /*
  double pole_UV_rcl = 1.;
  set_delta_uv_rcl(pole_UV_rcl);
  */
  generate_processes_rcl();

  
  /* double p[6][4] =
   {{           250.,              0.,              0.,            250.},
   {           250.,              0.,              0.,           -250.},
   {  191.521737019,   -72.989672519,    23.203308008,   -28.573588393},
   {  187.323053415,    41.294104557,    43.349089605,   -38.824472950},
   {  67.060209357,     -5.935268585,   -28.633411230,    60.180744654},
    {  54.095000210,     37.630836547,   -37.918986384,     7.217316689}};
  Recola::compute_running_alphas_rcl(p[0][0]+p[1][0],-1,1);
  double A2[2];
  Recola::compute_process_rcl(1,p,"NLO",A2);
  cout << "A2: " << A2[0] << "  " <<A2[1]<< endl;
  A2[0] = 0.;
  A2[1] = 0.;
  Recola::get_squared_amplitude_rcl(1,4,"LO",A2[0]);
  Recola::get_squared_amplitude_rcl(1,5,"NLO",A2[1]);*/
  //reset_recola_rcl(); 

  logger << LOG_DEBUG << "finished" << endl;
  
}

void observable_set::register_RCL_subprocess(int i_a, int amptype, int & process_id_rcl){
  static Logger logger("register_RCL_subprocess");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  stringstream temp_processname_ss;
  for (int i_p = 1; i_p < csi->type_parton[i_a].size(); i_p++){
    if (csi->type_parton[i_a][i_p] == 0){temp_processname_ss << "g";}
    // pdf convention:
    // W+ = +24 !!!
    // W- = -24 !!!
    else if (csi->type_parton[i_a][i_p] == 22){temp_processname_ss << "A";}
    else if (csi->type_parton[i_a][i_p] == 23){temp_processname_ss << "Z";}
    else if (csi->type_parton[i_a][i_p] == 24){temp_processname_ss << "W-";}
    else if (csi->type_parton[i_a][i_p] == -24){temp_processname_ss << "W+";}
    else if (csi->type_parton[i_a][i_p] == 25){temp_processname_ss << "H";}
    else if (csi->type_parton[i_a][i_p] == 1){temp_processname_ss << "d";}
    else if (csi->type_parton[i_a][i_p] == 2){temp_processname_ss << "u";}
    else if (csi->type_parton[i_a][i_p] == 3){temp_processname_ss << "s";}
    else if (csi->type_parton[i_a][i_p] == 4){temp_processname_ss << "c";}
    else if (csi->type_parton[i_a][i_p] == 5){temp_processname_ss << "b";}
    else if (csi->type_parton[i_a][i_p] == 6){temp_processname_ss << "t";}
    else if (csi->type_parton[i_a][i_p] == -1){temp_processname_ss << "d~";}
    else if (csi->type_parton[i_a][i_p] == -2){temp_processname_ss << "u~";}
    else if (csi->type_parton[i_a][i_p] == -3){temp_processname_ss << "s~";}
    else if (csi->type_parton[i_a][i_p] == -4){temp_processname_ss << "c~";}
    else if (csi->type_parton[i_a][i_p] == -5){temp_processname_ss << "b~";}
    else if (csi->type_parton[i_a][i_p] == -6){temp_processname_ss << "t~";}
    else if (csi->type_parton[i_a][i_p] == 11){temp_processname_ss << "e-";}
    else if (csi->type_parton[i_a][i_p] == 12){temp_processname_ss << "nu_e";}
    else if (csi->type_parton[i_a][i_p] == 13){temp_processname_ss << "mu-";}
    else if (csi->type_parton[i_a][i_p] == 14){temp_processname_ss << "nu_mu";}
    else if (csi->type_parton[i_a][i_p] == 15){temp_processname_ss << "tau-";}
    else if (csi->type_parton[i_a][i_p] == 16){temp_processname_ss << "nu_tau";}
    else if (csi->type_parton[i_a][i_p] == -11){temp_processname_ss << "e+";}
    else if (csi->type_parton[i_a][i_p] == -12){temp_processname_ss << "nu_e~";}
    else if (csi->type_parton[i_a][i_p] == -13){temp_processname_ss << "mu+";}
    else if (csi->type_parton[i_a][i_p] == -14){temp_processname_ss << "nu_mu~";}
    else if (csi->type_parton[i_a][i_p] == -15){temp_processname_ss << "tau+";}
    else if (csi->type_parton[i_a][i_p] == -16){temp_processname_ss << "nu_tau~";}
    // else if (csi->type_parton[i_a][i_p] == 101){temp_processname_ss << "p";}
    // else if (csi->type_parton[i_a][i_p] == -101){temp_processname_ss << "p~";}
    //else if (csi->type_parton[i_a][i_p] == 100){temp_processname_ss << "j";} 
    // recola manual doens't have proton and jet?

    //   else {temp_processname_ss << csi->type_parton[i_a][i_p];}
    if (i_p == 2){temp_processname_ss << " -> ";}
    else if (i_p == csi->type_parton[i_a].size() - 1){}
    else {temp_processname_ss << " ";}
  }
  string temp_processname = temp_processname_ss.str();
  //  int new_id = process_id_rcl + 1;

  // Amplitude number could be identified with phase-space number !!!
  /*
  if (csi->type_contribution == "RA" || 
      csi->type_contribution == "RRA" || 
      csi->type_contribution == "RRJ" || 
      csi->type_contribution == "L2RA"){
    process_id_rcl = i_a + 1;
    if (amptype == 1){define_process_rcl(process_id_rcl, temp_processname, "LO");}
    else if (amptype == 2){define_process_rcl(process_id_rcl, temp_processname, "NLO");}
    else {logger << LOG_ERROR << "Wrong amplitude type in RECOLA!" << endl; exit(1);}
    logger << LOG_DEBUG << "Initialization of " << temp_processname << " (id = " << process_id_rcl << ")" << endl;
  }
  else {
  */
  if (amptype == 1){
    define_process_rcl(++process_id_rcl, temp_processname, "LO");
    logger << LOG_DEBUG << "define_process_rcl(" << process_id_rcl << ", " << char(34) << temp_processname << char(34) << ", " << char(34) << "LO" << char(34) << ");" << endl;
  }
  else if (amptype == 2){
    define_process_rcl(++process_id_rcl, temp_processname, "NLO");
    logger << LOG_DEBUG << "define_process_rcl(" << process_id_rcl << ", " << char(34) << temp_processname << char(34) << ", " << char(34) << "NLO" << char(34) << ");" << endl;
  }
  else {logger << LOG_ERROR << "Wrong amplitude type in RECOLA!" << endl; exit(1);}
  //  logger << LOG_DEBUG << "Initialization of " << temp_processname << " (id = " << process_id_rcl << ")" << endl;
    //  }
  
  //  if (amptype == 1){define_process_rcl(new_id, temp_processname, "LO");}
  //  else if (amptype == 2){define_process_rcl(new_id, temp_processname, "NLO");}
  
  //  return new_id;
  //  logger << LOG_DEBUG << "finished" << endl; 
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
#endif
