#include "header.hpp"
#include "definitions.phasespace.set.cxx"

void observable_set::output_collinear(){
  static Logger logger("observable_set::output_collinear");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_c = 0; i_c < (*CA_collinear).size(); i_c++){
    for (int j_c = 0; j_c < (*CA_collinear)[i_c].size(); j_c++){
      stringstream temp;
      if      ((*CA_collinear)[i_c][j_c].type_correction() == 1){temp << "QCD ";}
      else if ((*CA_collinear)[i_c][j_c].type_correction() == 2){temp << "QEW ";}
      temp << "collinear dipole " << i_c << ", " << j_c << ":   " << setw(15) << left << (*CA_collinear)[i_c][j_c].name() << ":   ";
      temp << "csi->type_parton = ";
      for (int i_p = 1; i_p < (*CA_collinear)[i_c][j_c].type_parton().size(); i_p++){temp << (*CA_collinear)[i_c][j_c].type_parton()[i_p] << "   ";}
      //      for (int i_p = 1; i_p < csi->type_parton[0].size(); i_p++){temp << (*CA_collinear)[i_c][j_c].type_parton()[i_p] << "   ";}
      temp << "no_prc = " << (*CA_collinear)[i_c][j_c].no_prc() << "   ";
      logger << LOG_INFO << temp.str() << endl;
      temp.str("");
      temp << setw(48) << "";
      temp << "charge_factor = " << setw(23) << setprecision(15) << (*CA_collinear)[i_c][j_c].charge_factor() << "   ";
      if (CA_Q2f.size() > 0){
	temp << "charge_factor_fi / Qf2 = " << setw(23) << setprecision(15) << (*CA_collinear)[i_c][j_c].charge_factor_fi() / CA_Q2f[i_c][0][0] << "   ";
      }
      logger << LOG_INFO << temp.str() << endl;
      temp.str("");
      temp << setw(48) << "";
      temp << "type = " << (*CA_collinear)[i_c][j_c].type() << "   ";
      temp << "in_collinear = ";
      for (int i_em = 0; i_em < 3; i_em++){temp << (*CA_collinear)[i_c][j_c].in_collinear()[i_em] << "   ";}
      temp << "massive = " << (*CA_collinear)[i_c][j_c].massive() << "   ";
      logger << LOG_INFO << temp.str() << endl;
      temp.str("");
      temp << setw(48) << "";
      temp << "no_emitter = " << (*CA_collinear)[i_c][j_c].no_emitter() << "   ";
      temp << "no_spectator = " << (*CA_collinear)[i_c][j_c].no_spectator() << "   ";
      temp << "pair = ";
      for (int i_p = 0; i_p < 2; i_p++){temp << (*CA_collinear)[i_c][j_c].pair()[i_p] << "   ";}
      logger << LOG_INFO << temp.str() << endl;
      for (int i_x = 0; i_x < (*CA_collinear)[i_c][j_c].all_name().size(); i_x++){
	temp.str("");
	temp << setw(48) << "";
	temp << "pdf contributions: " << setw(15) << left << (*CA_collinear)[i_c][j_c].all_name()[i_x] << ":   ";
	for (int i_yxy = 0; i_yxy < 3; i_yxy++){temp << setw(2) << right << (*CA_collinear)[i_c][j_c].all_pdf()[i_x][i_yxy] << "  ";}
	//	temp << "( Q2f = " << CA_Q2f[i_c][0][0] << " )   ";
	logger << LOG_INFO << temp.str() << endl;
      }
    }
  }
  logger.newLine(LOG_INFO);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void observable_set::output_collinear_pdf(){
  static Logger logger("observable_set::output_collinear_pdf");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_c = 0; i_c < (*CA_collinear).size(); i_c++){
    for (int i_i = 0; i_i < CA_combination_pdf[i_c].size(); i_i++){
      logger << LOG_INFO << "pdf contributions: " << setw(15) << left << (*CA_collinear)[i_c][0].all_name()[i_i] << ":   " << endl;
      for (int i_x = 0; i_x < CA_combination_pdf[i_c][i_i].size(); i_x++){
	stringstream temp;
	temp.str("");
	temp << setw(19) << "";
	temp << "CA_Q2f[" << setw(2) << i_c << "][" << setw(2) << i_i << "][" << setw(2) << i_x << "] = " << setw(10) << setprecision(8) << CA_Q2f[i_c][i_i][i_x] << "     "; 
	temp << "CA_combination_pdf[" << setw(2) << i_c << "][" << setw(2) << i_i << "][" << setw(2) << i_x << "] = "; 
	for (int i_y = 0; i_y < CA_combination_pdf[i_c][i_i][i_x].size(); i_y++){temp << setw(2) << right << CA_combination_pdf[i_c][i_i][i_x][i_y] << "  ";}
	logger << LOG_INFO << temp.str() << endl;
      }
    }
    logger.newLine(LOG_INFO);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
