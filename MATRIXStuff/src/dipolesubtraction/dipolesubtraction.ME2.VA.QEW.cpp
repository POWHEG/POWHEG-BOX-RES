#include "header.hpp"
#include "definitions.observable.set.cxx"

void calculate_ME2_ioperator_VA_QEW(observable_set & oset){
  static Logger logger("calculate_ME2_ioperator_VA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi_VA_ioperator.size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
      osi_VA_ME2_cf[i_a].resize(osi_VA_ioperator[i_a].size());
      osi_VA_I_ME2_cf[i_a].resize(osi_VA_ioperator[i_a].size());
    }
    initialization = 0;
  }

#ifdef OPENLOOPS
  if (oset.switch_OL){

  static int n_momentum = 5 * (osi_n_particle + 2);

  static double *P;
  P = new double[n_momentum];
  for (int i = 1; i < osi_p_parton[0].size(); i++){
    P[5 * (i - 1)]     = osi_p_parton[0][i].x0();
    P[5 * (i - 1) + 1] = osi_p_parton[0][i].x1();
    P[5 * (i - 1) + 2] = osi_p_parton[0][i].x2();
    P[5 * (i - 1) + 3] = osi_p_parton[0][i].x3();
    P[5 * (i - 1) + 4] = osi_p_parton[0][i].m();
  }

  ol_evaluate_tree(1, P, &osi_VA_b_ME2);

  delete [] P;

  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
      osi_VA_ME2_cf[i_a][j_a] = osi_VA_ioperator[i_a][j_a].charge_factor() * osi_VA_b_ME2;
      ///      logger << LOG_INFO << "osi_VA_ioperator[" << i_a << "][" << j_a << "].charge_factor() = " << osi_VA_ioperator[i_a][j_a].charge_factor() << endl;
    }
  }

  }
#endif
#ifdef RECOLA
  if (oset.switch_RCL){

  double P_rec[osi_p_parton[0].size()][4];
  for (int i = 1; i < osi_p_parton[0].size(); i++){
    P_rec[i - 1][0] = osi_p_parton[0][i].x0();
    P_rec[i - 1][1] = osi_p_parton[0][i].x1();
    P_rec[i - 1][2] = osi_p_parton[0][i].x2();
    P_rec[i - 1][3] = osi_p_parton[0][i].x3();
  }

  for (int i = 1; i < osi_p_parton[0].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }
  
  if (oset.switch_VI == 2){
    // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.
    set_alphas_rcl(oset.var_alpha_S_reference, osi_var_mu_ren, oset.N_nondecoupled);
    logger << LOG_DEBUG << "set_alphas_rcl(" << setprecision(15) << oset.var_alpha_S_reference << ", " << setprecision(15) << osi_var_mu_ren << ", " << oset.N_nondecoupled << ");" << endl;
    compute_process_rcl(1, P_rec, "LO");
    logger << LOG_DEBUG << "compute_process_rcl(1, P_rec, " << char(34) << "LO" << char(34) << ");" << endl;
    // needed ???
    get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "LO", osi_VA_b_ME2);
    logger << LOG_DEBUG << "osi_VA_b_ME2 = " << osi_VA_b_ME2 << endl;
  }



  
  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
      logger << LOG_DEBUG << "osi_VA_ioperator[" << i_a << "][" << j_a << "].charge_factor() = " << osi_VA_ioperator[i_a][j_a].charge_factor() << endl;
      osi_VA_ME2_cf[i_a][j_a] = osi_VA_ioperator[i_a][j_a].charge_factor() * osi_VA_b_ME2;
      logger << LOG_DEBUG << "osi_VA_ME2_cf[" << i_a << "][" << j_a << "] = " << osi_VA_ME2_cf [i_a][j_a] << endl;
    }
  }

  }
#endif
  
  oset.calculate_ioperator_QEW();
  /*
  if (osi_massive_QEW){oset.calculate_ioperator_QEW_CDST();}
  else {oset.calculate_ioperator_QEW_CS();}
  */
  
  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi_VA_I_ME2_cf[i_a].begin(), osi_VA_I_ME2_cf[i_a].end(), 0.);
    logger << LOG_DEBUG << "I_ME2_emitter[" << i_a << "] = " << I_ME2_emitter[i_a] << endl;
      
  }
  osi_VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);
  logger << LOG_DEBUG << "osi_VA_I_ME2 = " << osi_VA_I_ME2 << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void calculate_ME2_VA_QEW(observable_set & oset){
  static Logger logger("calculate_ME2_VA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi_switch_VI == 0 || osi_switch_VI == 1){

#ifdef OPENLOOPS
    if (oset.switch_OL){

    static double one = 1;
    static int n_momentum = 5 * (osi_n_particle + 2);
    double *P;
    P = new double[n_momentum];
    for (int i = 1; i < osi_p_parton[0].size(); i++){
      P[5 * (i - 1)]     = osi_p_parton[0][i].x0();
      P[5 * (i - 1) + 1] = osi_p_parton[0][i].x1();
      P[5 * (i - 1) + 2] = osi_p_parton[0][i].x2();
      P[5 * (i - 1) + 3] = osi_p_parton[0][i].x3();
      P[5 * (i - 1) + 4] = osi_p_parton[0][i].m();
    }
    static char * OL_mu_ren = stch("muren");
    static char * OL_mu_reg = stch("mureg");
    static char * fact_uv = stch("fact_uv");
    static char * fact_ir = stch("fact_ir");
	
    ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
    //  ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
    logger << LOG_DEBUG << "osi_user_double_value[osi_user_double_map[mureg]] = " << osi_user_double_value[osi_user_double_map["mu_reg"]] << endl;
    if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
    else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
    else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
    logger << LOG_DEBUG_VERBOSE << "osi_user_double_value[osi_user_double_map[mureg]] = " << osi_user_double_value[osi_user_double_map["mu_reg"]] << endl;
	
    ol_setparameter_double(fact_uv, one);
    ol_setparameter_double(fact_ir, one);

    static double acc;
    static double M2L0;
    double *M2L1;
    double *IRL1;
    M2L1 = new double[3];
    IRL1 = new double[3];
    double *M2L2;
    double *IRL2;
    M2L2 = new double[5];
    IRL2 = new double[5];
    logger << LOG_DEBUG_VERBOSE << "before" << endl;

    /////
    /////    int CT_on = 1; // splitting into V and X does not work properly in present OpenLoops version !!!
    /////    ol_setparameter_int(stch("ct_on"), CT_on);
    /*// for ol_evaluate_ct check !!!
    int CT_on = 0;
    ol_setparameter_int(stch("ct_on"), CT_on);
    *///
   
    /*
    int CT_on = 1; // splitting into V and X does not work properly in present OpenLoops version !!!
    ol_setparameter_int(stch("ct_on"), CT_on);
    ol_evaluate_full(1, P, &osi_VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);
    *//*
    logger << LOG_INFO << "w/ CT   M2L1[0]  = " << M2L1[0] << endl;
    logger << LOG_INFO << "w/ CT   M2L1[1]  = " << M2L1[1] << endl;
    logger << LOG_INFO << "w/ CT   M2L1[2]  = " << M2L1[2] << endl;
    if (osi_VA_DeltaIR1 == 0. && osi_VA_DeltaIR2 == 0.){osi_VA_I_ME2 = IRL1[0];}
    else {osi_VA_I_ME2 = IRL1[0] + osi_VA_DeltaIR1 * IRL1[1] + osi_VA_DeltaIR2 * IRL1[2];}
    logger << LOG_INFO << "V+X_ME2+I_ME2 = " << M2L1[0] + osi_VA_I_ME2 << endl;
    logger.newLine(LOG_INFO);
    ///
    *//*
    osi_VA_V_ME2 = M2L1[0];
    osi_VA_X_ME2 = 0.;
    
    CT_on = 0;
    ol_setparameter_int(stch("ct_on"), CT_on);
    */
    
    // actual version (3 lines):
    ol_evaluate_full(1, P, &osi_VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);
    osi_VA_V_ME2 = M2L1[0];
    ol_evaluate_ct(1, P, &M2L0, &osi_VA_X_ME2);
    
    
    /*
    logger << LOG_INFO << "w/o CT  M2L1[0]  = " << M2L1[0] << endl;
    logger << LOG_INFO << "w/o CT  M2L1[1]  = " << M2L1[1] << endl;
    logger << LOG_INFO << "w/o CT  M2L1[2]  = " << M2L1[2] << endl;
    // I temporary here !!!
    if (osi_VA_DeltaIR1 == 0. && osi_VA_DeltaIR2 == 0.){osi_VA_I_ME2 = IRL1[0];}
    else {osi_VA_I_ME2 = IRL1[0] + osi_VA_DeltaIR1 * IRL1[1] + osi_VA_DeltaIR2 * IRL1[2];}
    logger << LOG_INFO << "osi_VA_V_ME2  = " << osi_VA_V_ME2 << endl;
    logger << LOG_INFO << "osi_VA_X_ME2  = " << osi_VA_X_ME2 << endl;
    logger << LOG_INFO << "osi_VA_I_ME2  = " << osi_VA_I_ME2 << endl;
    logger << LOG_INFO << "V+X+I_ME2     = " << osi_VA_V_ME2 + osi_VA_X_ME2 + osi_VA_I_ME2 << endl;
    */
    
    /////    osi_VA_X_ME2 = 0.;
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_B  = " << setw(23) << setprecision(15) << osi_VA_b_ME2 << endl; 
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_VX = " << setw(23) << setprecision(15) << osi_VA_V_ME2 + osi_VA_X_ME2 << endl;
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_V  = " << setw(23) << setprecision(15) << osi_VA_V_ME2 << endl; 
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_CT = " << setw(23) << setprecision(15) << osi_VA_X_ME2 << endl; 
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_VX = " << setw(23) << setprecision(15) << osi_VA_V_ME2 + osi_VA_X_ME2 << endl;
 

      if (osi_switch_VI == 0){
      // I-operator in OpenLoops not defined correctly for external photons:
      // check if still correct without external photons !!!
      
      /*
      calculate_ME2_ioperator_VA_QEW(oset);
      logger << LOG_INFO << setw(10) << oset.psi->i_acc << "   I-operator (SK)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
      osi_VA_I_ME2 = 0.;
      */
      
      //      if (osi_VA_delta_flag){
      ///            if (0 == 1){
	if (oset.user.switch_map["I_Munich"]){
	  calculate_ME2_ioperator_VA_QEW(oset);
	  logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_I  = " << setw(23) << setprecision(15) << osi_VA_I_ME2 << "   (from Munich)" << endl;
	}
	else {
	  // original version (2 lines):
	  if (osi_VA_DeltaIR1 == 0. && osi_VA_DeltaIR2 == 0.){osi_VA_I_ME2 = IRL1[0];}
	  else {osi_VA_I_ME2 = IRL1[0] + osi_VA_DeltaIR1 * IRL1[1] + osi_VA_DeltaIR2 * IRL1[2];}
	  logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_I  = " << setw(23) << setprecision(15) << osi_VA_I_ME2 << "   (from OpenLoops)" << endl;
	}

	
	//  osi_VA_I_ME2 = i_DeltaIR1 * IR1 + osi_VA_DeltaIR2 * IR2; // !!! I-operator switched off !!!

	/*
	// alternative version using MUNICH I-operator
	calculate_ME2_ioperator_VA_QEW(oset);
	*/
	
	/////	logger << LOG_DEBUG << setw(10) << oset.psi->i_acc << "   I-operator (OL)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
	/*///
      }
      else {
	calculate_ME2_ioperator_VA_QEW(oset);
	logger << LOG_DEBUG << setw(10) << oset.psi->i_acc << "   I-operator (SK)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
      }
	*/	///
	// !!!

      /* // commented out now: should only check if poles fit with SK I-operator
	// Shift V by the difference between I_OL and I_SK
	double I_ME2_OL = osi_VA_I_ME2;
	osi_VA_I_ME2 = 0.;
	calculate_ME2_ioperator_VA_QEW(oset);
	logger << LOG_DEBUG << setw(10) << oset.psi->i_acc << "   I-operator (SK)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
	osi_VA_V_ME2 += I_ME2_OL - osi_VA_I_ME2;
      */ //
	/*
      osi_VA_I_ME2 = 0.;
      calculate_ME2_ioperator_VA_QEW(oset);
      logger << LOG_INFO << setw(10) << oset.psi->i_acc << "   I-operator (SK)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
      */
	/*
	logger << LOG_INFO << "osi_VA_delta_flag = " << osi_VA_delta_flag << endl;
	logger << LOG_INFO << "osi_VA_DeltaUV    = " << osi_VA_DeltaUV << endl;
	logger << LOG_INFO << "osi_VA_DeltaIR1   = " << osi_VA_DeltaIR1 << endl;
	logger << LOG_INFO << "osi_VA_DeltaIR2   = " << osi_VA_DeltaIR2 << endl;
	logger << LOG_INFO << "I-operator (OL)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
	osi_VA_I_ME2 = 0.;
	calculate_ME2_ioperator_VA_QEW(oset);
	logger << LOG_INFO << "I-operator (SK)   = " << setprecision(15) << osi_VA_I_ME2 << endl;
	*/
      }
      else if (osi_switch_VI == 1){
	osi_VA_I_ME2 = 0.;
      }
      logger << LOG_DEBUG_POINT << "OpenLoops:  ME2_I  = " << setw(23) << setprecision(15) << osi_VA_I_ME2 << endl; 

    //    cout << right << setw(5) << "I1" << " = " << setprecision(15) << setw(23) << left << IRL1[1] << endl;
    //    cout << right << setw(5) << "I2" << " = " << setprecision(15) << setw(23) << left << IRL1[2] << endl;

      // Shifted to above:
    ///    ol_evaluate_ct(1, P, &M2L0, &osi_VA_X_ME2);
    /////    osi_VA_X_ME2 = 0.;
    if (osi_switch_CV){
      for (int s = 0; s < osi_n_scales_CV; s++){
        double inv_factor_CV = osi_var_mu_ren_CV[s] / osi_var_mu_ren;
        ol_setparameter_double(OL_mu_ren, osi_var_mu_ren_CV[s]);
	if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren_CV[s]);}
	else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
	else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}

        ol_setparameter_double(fact_uv, inv_factor_CV);
        ol_setparameter_double(fact_ir, inv_factor_CV);
	ol_evaluate_ct(1, P, &M2L0, &osi_VA_X_ME2_CV[s]);
	logger << LOG_DEBUG_POINT << "OpenLoops:  VA_X_ME2_CV[" << s << "] = " << setw(23) << setprecision(15) << osi_VA_X_ME2_CV[s] + osi_VA_V_ME2 << endl;	 
	/////	osi_VA_X_ME2_CV[s] = 0.;
	logger << LOG_DEBUG_VERBOSE << "after ol_evaluate_ct s = " << s << endl;
      }
    }
    if (osi_switch_TSV){
      for (int i_v = 0; i_v < osi_max_dyn_ren + 1; i_v++){
	for (int i_r = 0; i_r < osi_n_scale_dyn_ren[i_v]; i_r++){
	  double inv_factor_TSV = osi_value_scale_ren[0][i_v][i_r] / osi_var_mu_ren;
	  ol_setparameter_double(OL_mu_ren, osi_value_scale_ren[0][i_v][i_r]);
	  if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_value_scale_ren[0][i_v][i_r]);}
	  else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
	  else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
	  
	  ol_setparameter_double(fact_uv, inv_factor_TSV);
	  ol_setparameter_double(fact_ir, inv_factor_TSV);
	  ol_evaluate_ct(1, P, &M2L0, &osi_value_ME2term_ren[0][i_v][i_r]);
	  logger << LOG_DEBUG_POINT << "OpenLoops:  VA_VX_ME2_TSV[" << i_v << "][" << i_r << "] = " << osi_value_ME2term_ren[0][i_v][i_r] + osi_VA_V_ME2 << endl; 
	  osi_value_ME2term_ren[0][i_v][i_r] += osi_VA_V_ME2 + osi_VA_I_ME2;
	  /////	  osi_value_ME2term_ren[0][i_v][i_r] = osi_VA_V_ME2 + osi_VA_I_ME2;
	  logger << LOG_DEBUG_VERBOSE << "after ol_evaluate_ct i_v = " << i_v << "   i_r = " << i_r << endl;
	}
      }
    }
    delete [] M2L2;
    delete [] IRL2;
    delete [] M2L1;
    delete [] IRL1;
    delete [] P;
    }
#endif
#ifdef RECOLA
    if (oset.switch_RCL){
   
    double P_rec[osi_p_parton[0].size()][4];
    for (int i = 1; i < osi_p_parton[0].size(); i++){
      P_rec[i - 1][0] = osi_p_parton[0][i].x0();
      P_rec[i - 1][1] = osi_p_parton[0][i].x1();
      P_rec[i - 1][2] = osi_p_parton[0][i].x2();
      P_rec[i - 1][3] = osi_p_parton[0][i].x3();
    }
    
    for (int i = 1; i < osi_p_parton[0].size(); i++){
      stringstream temp_ss;
      for (int j = 0; j < 4; j++){
	temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
      }
      logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
    }

    if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){
      set_mu_uv_rcl(osi_var_mu_ren);
      set_mu_ir_rcl(osi_var_mu_ren);
    }
    else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){
      set_mu_uv_rcl(osi_var_mu_ren);
      set_mu_ir_rcl(osi_var_mu_ren);
    }
    else {
      set_mu_uv_rcl(osi_user_double_value[osi_user_double_map["mu_reg"]]);
      set_mu_ir_rcl(osi_user_double_value[osi_user_double_map["mu_reg"]]);
    }
    /*
    set_mu_uv_rcl(osi_var_mu_ren);
    logger << LOG_DEBUG_VERBOSE << "set_mu_uv_rcl(" << setprecision(15) << osi_var_mu_ren << ");" << endl;

    set_mu_ir_rcl(osi_var_mu_ren);
    logger << LOG_DEBUG_VERBOSE << "set_mu_ir_rcl(" << setprecision(15) << osi_var_mu_ren << ");" << endl;
    */
    set_alphas_rcl(oset.var_alpha_S_reference, osi_var_mu_ren, oset.N_nondecoupled);
    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << oset.var_alpha_S_reference << ", " << setprecision(15) << osi_var_mu_ren << ", " << oset.N_nondecoupled << ");" << endl;
    
    compute_process_rcl(1, P_rec, "NLO");
    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_rec, " << char(34) << "NLO" << char(34) << ");" << endl;
    
    get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "LO", osi_VA_b_ME2);
    /////      get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s - 1, "LO", osi_VA_b_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi_VA_b_ME2 << ");" << endl;
    /////      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s - 1 << ", " << char(34) << "LO" << char(34) << ", " << setprecision(15) << osi_VA_b_ME2 << ");" << endl;
    //      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi_VA_V_ME2 + osi_VA_X_ME2 << endl; 
    osi_VA_b_ME2 = osi_VA_b_ME2 / osi_var_rel_alpha_S;
    /////      osi_VA_b_ME2 = osi_VA_b_ME2 * (oset.var_alpha_S_reference / osi_alpha_S) / osi_var_rel_alpha_S;
    logger << LOG_DEBUG_POINT << "Recola:     ME2_B  = " << setw(23) << setprecision(15) << osi_VA_b_ME2 << "   (corrected for var_rel_alpha_S)" << endl; 
    
    osi_VA_V_ME2 = 0.;
    get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO", osi_VA_X_ME2);
    logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi_VA_X_ME2 << ");" << endl;
    //      logger << LOG_DEBUG_POINT << "Recola:     ME2_B  = " << setw(23) << setprecision(15) << osi_VA_b_ME2 << endl; 
    osi_VA_X_ME2 = osi_VA_X_ME2 / osi_var_rel_alpha_S;
    logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi_VA_V_ME2 + osi_VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl; 
    
    if (oset.switch_output_comparison){
      double V_ME2_D4 = 0.;
      get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO-D4", V_ME2_D4);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "NLO-D4" << char(34) << ", " << setprecision(15) << V_ME2_D4 << ");" << endl;
      V_ME2_D4 = V_ME2_D4 / osi_var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_D4 = " << setw(23) << setprecision(15) << V_ME2_D4 << "   (corrected for var_rel_alpha_S)" << endl; 
      
      double V_ME2_R2 = 0.;
      get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO-R2", V_ME2_R2);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "NLO-R2" << char(34) << ", " << setprecision(15) << V_ME2_R2 << ");" << endl;
      V_ME2_R2 = V_ME2_R2 / osi_var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_R2 = " << setw(23) << setprecision(15) << V_ME2_R2 << "   (corrected for var_rel_alpha_S)" << endl; 
      
      double V_ME2_CT = 0.;
      get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO-CT", V_ME2_CT);
      logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "NLO-CT" << char(34) << ", " << setprecision(15) << V_ME2_CT << ");" << endl;
      V_ME2_CT = V_ME2_CT / osi_var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_CT = " << setw(23) << setprecision(15) << V_ME2_CT << "   (corrected for var_rel_alpha_S)" << endl;
      
      //	logger << LOG_DEBUG << "" << endl;
      
      // in order to match the OpenLoops output:
      
      osi_VA_X_ME2 = V_ME2_CT;
      osi_VA_V_ME2 = V_ME2_D4 + V_ME2_R2;
      
      logger << LOG_DEBUG_POINT << "Recola:     ME2_VX = " << setw(23) << setprecision(15) << osi_VA_V_ME2 + osi_VA_X_ME2 << "   (corrected for var_rel_alpha_S)" << endl; 
    }
    
    osi_VA_I_ME2 = 0.;
    if (osi_switch_VI == 0 || osi_switch_VI == 2){
      calculate_ME2_ioperator_VA_QEW(oset);
      //      logger << LOG_DEBUG << "Recola:     ME2_I  = " << setw(23) << setprecision(15) << osi_VA_I_ME2 << endl;
      // don't know why for EW !!! comment out:
      //	osi_VA_I_ME2 = osi_VA_I_ME2 / osi_var_rel_alpha_S;
      /////      osi_VA_I_ME2 = osi_VA_I_ME2 * (oset.var_alpha_S_reference / osi_alpha_S) / osi_var_rel_alpha_S;
      logger << LOG_DEBUG_POINT << "Recola:     ME2_I  = " << setw(23) << setprecision(15) << osi_VA_I_ME2 << "   (corrected for var_rel_alpha_S)" << endl;
    }
    
    
    if (osi_switch_CV){
      for (int s = 0; s < osi_n_scales_CV; s++){
	set_alphas_rcl(oset.var_alpha_S_CV[s], osi_var_mu_ren_CV[s], oset.N_nondecoupled);
	logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << oset.var_alpha_S_CV[s] << ", " << setprecision(15) << osi_var_mu_ren_CV[s] << ", " << oset.N_nondecoupled << ");" << endl;
	
	rescale_process_rcl(1, "NLO");
	logger << LOG_DEBUG_VERBOSE << "rescale_process_rcl(1, " << char(34) << "NLO" << char(34) << ");" << endl;
	
	/*
	  double temp_alphaS = osi_alpha_S * pow(osi_var_rel_alpha_S_CV[s], double(oset.csi->contribution_order_alpha_s - 1) / oset.csi->contribution_order_alpha_s);
	  set_alphas_rcl(temp_alphaS, osi_var_mu_ren_CV[s], oset.N_nondecoupled);
	  compute_process_rcl(1, P_rec, "NLO");
	  */
	  //	  set_alphas_rcl(osi_var_rel_alpha_S_CV[s], osi_var_mu_ren_CV[s], oset.N_nondecoupled);
	  //	  compute_process_rcl(1, P_rec, "NLO");

	osi_VA_X_ME2_CV[s] = 0.;
	//	  get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO-CT", osi_VA_X_ME2_CV[s]);
	
	get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO", osi_VA_X_ME2_CV[s]);
	logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi_VA_X_ME2_CV[s] << ");" << endl;
	osi_VA_X_ME2_CV[s] = osi_VA_X_ME2_CV[s] / oset.var_rel_alpha_S_CV[s];
	logger << LOG_DEBUG_POINT << "Recola:     VA_X_ME2_CV[" << s << "] = " << setw(23) << setprecision(15) << osi_VA_X_ME2_CV[s] << "   (after removing alpha_S factor)" << endl;	 
      }
    }
    
    
    if (osi_switch_TSV){
      for (int i_v = 0; i_v < osi_max_dyn_ren + 1; i_v++){
	for (int i_r = 0; i_r < osi_n_scale_dyn_ren[i_v]; i_r++){
	  set_alphas_rcl(osi_value_alpha_S_TSV[0][i_v][i_r], osi_value_scale_ren[0][i_v][i_r], oset.N_nondecoupled);
	  logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << osi_value_alpha_S_TSV[0][i_v][i_r] << ", " << setprecision(15) << osi_value_scale_ren[0][i_v][i_r] << ", " << oset.N_nondecoupled << ");" << endl;
	  
	  rescale_process_rcl(1, "NLO");
	  logger << LOG_DEBUG_VERBOSE << "rescale_process_rcl(1, " << char(34) << "NLO" << char(34) << ");" << endl;
	  
	  get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "NLO", osi_value_ME2term_ren[0][i_v][i_r]);
	  logger << LOG_DEBUG_VERBOSE << "get_squared_amplitude_rcl(1, " << oset.csi->contribution_order_alpha_s << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi_value_ME2term_ren[0][i_v][i_r] << ");" << endl;
	  
	  osi_value_ME2term_ren[0][i_v][i_r] = osi_value_ME2term_ren[0][i_v][i_r] / osi_value_relative_factor_alpha_S[0][i_v][i_r];
	  logger << LOG_DEBUG_POINT << "Recola:     ME2_VX_TSV[" << i_v << "][" << i_r << "] = " << setw(23) << setprecision(15) << osi_value_ME2term_ren[0][i_v][i_r] << "   (corrected for var_rel_alpha_S)" << endl; 
	  osi_value_ME2term_ren[0][i_v][i_r] += osi_VA_I_ME2;
	}
      }
    }
    }
#endif   
  }
  else if (osi_switch_VI == 2){
    logger << LOG_DEBUG_VERBOSE << "OL: osi_VA_I_ME2 = " << osi_VA_I_ME2 << endl;
    //  osi_VA_I_ME2 = 0.;
    //  osi_VA_V_ME2 = 0.;
    //  osi_VA_X_ME2 = 0.;
    //  for (int s = 0; s < osi_n_scales_CV; s++){osi_VA_X_ME2_CV[s] = 0.;}
    //  I-operator evaluation -> osi_VA_I_ME2
    calculate_ME2_ioperator_VA_QEW(oset);
    for (int i_v = 0; i_v < osi_max_dyn_ren + 1; i_v++){
      for (int i_r = 0; i_r < osi_n_scale_dyn_ren[i_v]; i_r++){
        osi_value_ME2term_ren[0][i_v][i_r] = osi_VA_I_ME2;
      }
    }
    logger << LOG_DEBUG << "SK: osi_VA_I_ME2 = " << osi_VA_I_ME2 << endl;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void calculate_ME2check_VA_QEW(observable_set & oset){
  static Logger logger("calculate_ME2check_VA_QEW");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (oset.switch_output_comparison){

    oset.xmunich->generic.phasespacepoint_psp(oset);
    
    if (osi_p_parton[0][0].x0() == 0.){
      if (oset.switch_OL){oset.testpoint_from_OL_rambo();}
    }
   
    if (osi_p_parton[0][0].x0() != 0.){
      ofstream out_comparison;
      out_comparison.open(osi_filename_comparison.c_str(), ofstream::out | ofstream::app);  
      
      perform_event_selection(oset, oset.xmunich->generic);
      if (oset.cut_ps[0] == -1){
	out_comparison << "Phase-space 0 is cut. -> Default scale is used." << endl;
	for (int sd = 1; sd < oset.max_dyn_ren + 1; sd++){
	  for (int ss = 0; ss < oset.n_scale_dyn_ren[sd]; ss++){
	    oset.value_scale_ren[0][sd][ss] = 100.;
	  }
	}
	for (int sd = 1; sd < oset.max_dyn_fact + 1; sd++){
	  for (int ss = 0; ss < oset.n_scale_dyn_fact[sd]; ss++){
	    oset.value_scale_fact[0][sd][ss] = 100.;
	  }
	}
	osi_var_mu_ren = 100.;
	osi_var_mu_fact = 100.;
	
	oset.cut_ps[0] = 0;
      }
      else {
	// Maybe not valid any longer:
	// 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
	// 2 - Testpoint is calculated at output scale.
	oset.xmunich->generic.calculate_dynamic_scale(0, oset);
	oset.xmunich->generic.calculate_dynamic_scale_TSV(0, oset);
	oset.determine_scale();
	/////  }
	logger << LOG_DEBUG_VERBOSE << "back" << endl;
      }
      
      static char * OL_mu_ren = stch("muren");
      static char * OL_mu_reg = stch("mureg");
      
      static char * pole_uv = stch("pole_uv");
      static char * pole_ir1 = stch("pole_ir1");
      static char * pole_ir2 = stch("pole_ir2");
      //  static char * me_cache = stch("me_cache");
      
      static char * OL_alpha_s = stch("alpha_s");
      
      osi_VA_delta_flag = 1;
      string s_Delta;
      osi_VA_b_ME2 = 0.;
      osi_VA_V_ME2 = 0.;
      osi_VA_X_ME2 = 0.;
      osi_VA_I_ME2 = 0.;
      osi_VA_X_ME2_CV.resize(osi_n_scales_CV, 0.);
    
    /*
    if (osi_p_parton[0][0].x0() != 0.){
      ofstream out_comparison;
      out_comparison.open(osi_filename_comparison.c_str(), ofstream::out | ofstream::app);  
    */
    
      double coefficient_B = 0.;
      double coefficient_VF = 0.;
      double coefficient_V1 = 0.;
      double coefficient_V2 = 0.;
      
      if (oset.switch_output_comparison == 2){
	ol_setparameter_double(OL_alpha_s, oset.var_alpha_S_reference);
      }

      double i_Delta;
      osi_VA_DeltaUV = 0.;
      osi_VA_DeltaIR1 = 0.;
      osi_VA_DeltaIR2 = 0.;

      ///    ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      ///    if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
      ///    else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
      logger << LOG_DEBUG_VERBOSE << "before switch_OL" << endl;

      double temp_mu_reg = 0.;
#ifdef OPENLOOPS
      if (oset.switch_OL){
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      //      ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
      if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	temp_mu_reg = osi_var_mu_ren;
      }
      else {
	ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);
	temp_mu_reg = osi_user_double_value[osi_user_double_map["mu_reg"]];
      }
      
      ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
      ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
      ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
      //  ol_setparameter_double(me_cache, 0);
      }
#endif
#ifdef RECOLA
      if (oset.switch_RCL){
      set_delta_uv_rcl(osi_VA_DeltaUV);
      set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
      }
#endif
    
#ifdef OPENLOOPS
      if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}
#endif

      
      /*
      double temp_mu_reg = 0.;
      ol_getparameter_double(OL_mu_reg, &temp_mu_reg);
      */
      
      logger << LOG_DEBUG_VERBOSE << "before calculate_ME2_VA_QEW" << endl;
      calculate_ME2_VA_QEW(oset);
      logger << LOG_DEBUG_VERBOSE << "after calculate_ME2_VA_QEW" << endl;

      coefficient_B = osi_VA_b_ME2;
      coefficient_VF = osi_VA_V_ME2 + osi_VA_X_ME2;

      out_comparison << "Settings: " << endl << endl;
#ifdef OPENLOOPS
      if (oset.switch_OL){
	out_comparison << setw(12) << "  mu_reg" << " = " << setprecision(15) << setw(23) << temp_mu_reg << "  ( = mu_ren by default)" << endl;
      }
#endif
#ifdef RECOLA
      if (oset.switch_RCL){
	get_mu_uv_rcl(temp_mu_reg);
	out_comparison << setw(12) << "  mu_reg_UV" << " = " << setprecision(15) << setw(23) << temp_mu_reg << "  ( = mu_ren by default)" << endl;
	get_mu_ir_rcl(temp_mu_reg);
	out_comparison << setw(12) << "  mu_reg_IR" << " = " << setprecision(15) << setw(23) << temp_mu_reg << "  ( = mu_ren by default)" << endl;
      }
#endif
      //      out_comparison << setw(12) << "  mu_reg  = " << setprecision(15) << setw(23) << osi_var_mu_ren << "  ( = mu_ren by default)" << endl;
      out_comparison << setw(12) << "  mu_ren" << " = " << setprecision(15) << setw(23) << osi_var_mu_ren << "  " << "alpha_S(mu_ren) = " << osi_alpha_S << endl;
      out_comparison << setw(12) << "  mu_fact" << " = " << setprecision(15) << setw(23) << osi_var_mu_fact << endl;
      out_comparison << endl;
    
      out_comparison << "Absolute results: " << endl << endl;
      oset.output_testpoint_VA_result(out_comparison);
      double OL_I_ME2 = osi_VA_I_ME2;
      //      if (osi_switch_VI != 2){
      {
	osi_VA_I_ME2 = 0.;
#ifdef RECOLA
	if (oset.switch_RCL){
	set_alphas_rcl(oset.var_alpha_S_reference, osi_var_mu_ren, oset.N_nondecoupled);
	rescale_process_rcl(1, "LO");
	}
#endif
	calculate_ME2_ioperator_VA_QEW(oset);
#ifdef RECOLA
	if (oset.switch_RCL){
	osi_VA_I_ME2 = osi_VA_I_ME2 / osi_var_rel_alpha_S;
	/// remove rel. alpha_S factor      osi_VA_I_ME2 = osi_VA_I_ME2 * (oset.var_alpha_S_reference / osi_alpha_S) / osi_var_rel_alpha_S;
	}
#endif
	//      double SK_I_ME2 = osi_VA_I_ME2;
	oset.output_testpoint_VA_ioperator(out_comparison);
	osi_VA_I_ME2 = OL_I_ME2;
      }
      /* // not needed, thus output switched off
	 osi_VA_V_ME2 = osi_VA_V_ME2 / osi_VA_b_ME2;
	 osi_VA_X_ME2 = osi_VA_X_ME2 / osi_VA_b_ME2;
	 osi_VA_I_ME2 = osi_VA_I_ME2 / osi_VA_b_ME2;
	 //    osi_VA_b_ME2 = osi_VA_b_ME2;
	 out_comparison << "Relative results (corrections devided by ME2_born, ME2_born divided by coupling constants): " << endl << endl;
	 oset.output_testpoint_VA_result(out_comparison);
	 out_comparison << endl;
      */
      out_comparison << "Particle momenta: " << endl << endl;
      output_momenta(out_comparison, oset);
      
      out_comparison << "Numerical check of (UV and IR) finiteness: " << endl << endl;

      int temp_switch_check_IRneqUV = 1;

      if (temp_switch_check_IRneqUV){
      s_Delta = "Delta_UV";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi_VA_DeltaUV = i_Delta;
	
	logger << LOG_DEBUG << "osi_VA_DeltaUV  = " << osi_VA_DeltaUV << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR1 = " << osi_VA_DeltaIR1 << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR2 = " << osi_VA_DeltaIR2 << endl;
	
#ifdef OPENLOOPS
	if (oset.switch_OL){
	  ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	  ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	  ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	  ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	  ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	  //  ol_setparameter_double(me_cache, 0);
	}
#endif
#ifdef RECOLA
	if (oset.switch_RCL){
	  set_delta_uv_rcl(osi_VA_DeltaUV);
	  set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif
	
	calculate_ME2_VA_QEW(oset);
	// use SK I-operator (needed for external photons by now) !!!
	///  calculate_ME2_ioperator_VA_QEW(oset);
	oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      osi_VA_DeltaUV = 0.;
      out_comparison << endl;
      
      s_Delta = "Delta_IR_1";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi_VA_DeltaIR1 = i_Delta;
	
	logger << LOG_DEBUG << "osi_VA_DeltaUV  = " << osi_VA_DeltaUV << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR1 = " << osi_VA_DeltaIR1 << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR2 = " << osi_VA_DeltaIR2 << endl;
	
#ifdef OPENLOOPS
	if (oset.switch_OL){
	ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	//  ol_setparameter_double(me_cache, 0);
	}
#endif
#ifdef RECOLA
	if (oset.switch_RCL){
	set_delta_uv_rcl(osi_VA_DeltaUV);
	set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif

	calculate_ME2_VA_QEW(oset);
	// use SK I-operator (needed for external photons by now) !!!
	///  calculate_ME2_ioperator_VA_QEW(oset);
	oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      coefficient_V1 = osi_VA_V_ME2 + osi_VA_X_ME2 - coefficient_VF;
      osi_VA_DeltaIR1 = 0.;
      out_comparison << endl;
      }
    
      s_Delta = "Delta_IR_2";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
      
	osi_VA_DeltaIR2 = i_Delta;
	
	logger << LOG_DEBUG << "osi_VA_DeltaUV  = " << osi_VA_DeltaUV << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR1 = " << osi_VA_DeltaIR1 << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR2 = " << osi_VA_DeltaIR2 << endl;
	
#ifdef OPENLOOPS
	if (oset.switch_OL){
	ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	//  ol_setparameter_double(me_cache, 0);
	}
#endif
#ifdef RECOLA
	if (oset.switch_RCL){
	set_delta_uv_rcl(osi_VA_DeltaUV);
	set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif

	calculate_ME2_VA_QEW(oset);
	// use SK I-operator (needed for external photons by now) !!!
	///  calculate_ME2_ioperator_VA_QEW(oset);
	oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      coefficient_V2 = osi_VA_V_ME2 + osi_VA_X_ME2 - coefficient_VF;
    
      osi_VA_DeltaIR2 = 0.;
      out_comparison << endl;
      s_Delta = "Delta_UV = Delta_IR_1";
      for (int i = 0; i < 3; i++){
	i_Delta = (double(i) - 1.) * 1.;
	osi_VA_DeltaUV = i_Delta;
	osi_VA_DeltaIR1 = i_Delta;
	
	logger << LOG_DEBUG << "osi_VA_DeltaUV  = " << osi_VA_DeltaUV << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR1 = " << osi_VA_DeltaIR1 << endl;
	logger << LOG_DEBUG << "osi_VA_DeltaIR2 = " << osi_VA_DeltaIR2 << endl;
	
#ifdef OPENLOOPS
	if (oset.switch_OL){
	ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	//  ol_setparameter_double(me_cache, 0);
	}
#endif
#ifdef RECOLA
	if (oset.switch_RCL){
	set_delta_uv_rcl(osi_VA_DeltaUV);
	set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif

	calculate_ME2_VA_QEW(oset);
	// use SK I-operator (needed for external photons by now) !!!
	///  calculate_ME2_ioperator_VA_QEW(oset);
	oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
      }
      if (!temp_switch_check_IRneqUV){
	coefficient_V1 = osi_VA_V_ME2 + osi_VA_X_ME2 - coefficient_VF;
      }
      osi_VA_DeltaUV = 0.;
      osi_VA_DeltaIR1 = 0.;
      out_comparison << endl;

      logger << LOG_DEBUG << "osi_VA_DeltaUV  = " << osi_VA_DeltaUV << endl;
      logger << LOG_DEBUG << "osi_VA_DeltaIR1 = " << osi_VA_DeltaIR1 << endl;
      logger << LOG_DEBUG << "osi_VA_DeltaIR2 = " << osi_VA_DeltaIR2 << endl;

#ifdef OPENLOOPS
      if (oset.switch_OL){
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
      ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
      ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
      ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
      //  ol_setparameter_double(me_cache, 0);
      }
#endif
#ifdef RECOLA
      if (oset.switch_RCL){
      set_delta_uv_rcl(osi_VA_DeltaUV);
      set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
      }
#endif

      calculate_ME2_VA_QEW(oset);
      // in order to reset Delta-dependent SK I-operator settings !!!
      ///  calculate_ME2_ioperator_VA_QEW(oset);
      osi_VA_delta_flag = 0;
    
      out_comparison << endl;
      out_comparison << endl;
      out_comparison << "Coefficients:" << endl;
      out_comparison << endl;
      out_comparison << right << setw(5) << "B" << " = " << setprecision(15) << setw(23) << left << coefficient_B << endl;
      out_comparison << right << setw(5) << "VF" << " = " << setprecision(15) << setw(23) << left << coefficient_VF << endl;
      out_comparison << right << setw(5) << "V1" << " = " << setprecision(15) << setw(23) << left << coefficient_V1 << endl;
      out_comparison << right << setw(5) << "V2" << " = " << setprecision(15) << setw(23) << left << coefficient_V2 << endl;
      out_comparison << endl;
      out_comparison << endl;
      
      out_comparison << "#" << right
		     << setprecision(6) << setw(11) << "lam"
		     << setprecision(15) << setw(23) << "B"
		     << setprecision(15) << setw(23) << "VF"
		     << setprecision(15) << setw(23) << "V1"
		     << setprecision(15) << setw(23) << "V2"
		     << setprecision(15) << setw(23) << "VF/(B*alpha)"
		     << setprecision(15) << setw(23) << "V1/(B*alpha)"
		     << setprecision(15) << setw(23) << "V2/(B*alpha)"
		     << endl;

      // hard-coded alpha value ???
      out_comparison << right
		     << setprecision(7) << setw(12) << oset.msi.alpha_e / 0.0075552541674291547
		     << setprecision(15) << setw(23) << coefficient_B
		     << setprecision(15) << setw(23) << coefficient_VF
		     << setprecision(15) << setw(23) << coefficient_V1
		     << setprecision(15) << setw(23) << coefficient_V2 
		     << setprecision(15) << setw(23) << coefficient_VF / coefficient_B / oset.msi.alpha_e
		     << setprecision(15) << setw(23) << coefficient_V1 / coefficient_B / oset.msi.alpha_e
		     << setprecision(15) << setw(23) << coefficient_V2 / coefficient_B / oset.msi.alpha_e
		     << endl;
      
      out_comparison.close();
    
      if (oset.switch_output_comparison == 2){
#ifdef OPENLOOPS
	if (oset.switch_OL){ol_setparameter_double(OL_alpha_s, osi_alpha_S);}
#endif
      }
    }
    else {
#ifdef OPENLOOPS
      if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}
#endif
    }
    
    oset.switch_output_comparison = 0;
  }
  else {
#ifdef OPENLOOPS
    if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}
#endif
  }
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



