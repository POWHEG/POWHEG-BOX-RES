#include "header.hpp"

void observable_set::initialization_CS_splitting_coefficients_QEW(){
  static Logger logger("observable_set::calculate_splitting_coefficients");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  switch_convention_P_qq = 0; // 
    
  double sum_f_charge2 = 0.;
  for (int i_g = 1; i_g < 4; i_g++){
    // charged leptons
    if (M[11 + (i_g - 1) * 2] == 0.){sum_f_charge2 += 1.;}
    // neutrinos
    if (M[12 + (i_g - 1) * 2] == 0.){sum_f_charge2 += 0.;}
    // down-type quarks
    if (M[1 + (i_g - 1) * 2] == 0.){sum_f_charge2 += N_c * (1. / 9.);}
    // up-type quarks
    if (M[2 + (i_g - 1) * 2] == 0.){sum_f_charge2 += N_c * (4. / 9.);}
  }
  /*
  CS_gamma_a = -(2. / 3.) * sum_f_charge2;
  CS_K_a = -(10. / 9.) * sum_f_charge2;
  */
  // Identical content, just different names:
  CS_QEW_gamma_a = -(2. / 3.) * sum_f_charge2;
  CS_QEW_K_a = -(10. / 9.) * sum_f_charge2;

  CS_QEW_gamma_q = (3. / 2.); // C_F -> Q²_f
  CS_QEW_K_q = (7. / 2.) - pi2_6; // C_F -> Q²_f
  
  // Only K+P terms (CA):

  // P
  // Allow for different conventions i P_qq:
  if (switch_convention_P_qq){CS_QEW_P_qq_delta = (3. / 2.);}
  else {CS_QEW_P_qq_delta = 0.;}
  
  CS_QEW_P_aa_reg = 0.; // C_A -> 0
  CS_QEW_P_aa = 0.; // C_A -> 0
  CS_QEW_P_aa_plus = 0.; // C_A -> 0
  CS_QEW_intP_aa_plus = 0.; // C_A -> 0
  CS_QEW_P_aa_delta = (-2. / 3.) * sum_f_charge2; // C_A -> 0, T_R -> sum_f (N_c,f * Q²_f)

  // Kbar
  CS_QEW_Kbar_qq_delta = -(5. - pi2); // C_F -> [Q²_q]

  CS_QEW_Kbar_aa = 0.;
  CS_QEW_Kbar_aa_plus = 0.;
  CS_QEW_intKbar_aa_plus = 0.;
  CS_QEW_Kbar_aa_delta = (16. / 9.) * sum_f_charge2;

  // Kt
  //  CS_QEW_Kt_qa = 0.;
  //  CS_QEW_Kt_aq = 0.;

  CS_QEW_Kt_qq_delta = -pi2 / 3.; // C_F -> [Q²_q]

  CS_QEW_Kt_aa = 0.;
  CS_QEW_Kt_aa_plus = 0.;
  CS_QEW_Kt_aa_delta = 0.;
  CS_QEW_intKt_aa_plus = 0.;
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

// P
double observable_set::CS_QEW_P_qa(double & x){return (1. + pow(1. - x, 2)) / x;} // C_F -> [Q²_q]
double observable_set::CS_QEW_P_aq(double & x){return N_c * (pow(x, 2) + pow(1. - x, 2));} // T_R -> N_c * [Q²_q]
double observable_set::CS_QEW_P_qq_reg(double & x){return -(1. + x);} // C_F -> [Q²_q]
// Allow for different conventions via switch: affects P_qq (apart from P_qq_reg)
double observable_set::CS_QEW_P_qq(double & x){
  if (switch_convention_P_qq){return -(1. + x);} // C_F -> [Q²_q]
  else {return 0.;}
}
double observable_set::CS_QEW_P_qq_plus(double & x){
  if (switch_convention_P_qq){return 2. / (1. - x);} // C_F -> [Q²_q]
  else {return (1. + pow(x, 2)) / (1. - x);} // C_F -> [Q²_q]
}
//double observable_set::CS_QEW_P_qq_delta(double & x){return 0.;}
double observable_set::CS_QEW_intP_qq_plus(double & x){
  if (switch_convention_P_qq){return -2 * log(1. - x);} // C_F -> [Q²_q]
  else {return -.5 * pow(x, 2) - x - 2 * log(1. - x);} // C_F -> [Q²_q]
}

// Kbar
double observable_set::CS_QEW_Kbar_qa(double & x){return CS_QEW_P_qa(x) * log((1. - x) / x) + x;} // C_F -> [Q²_q]
double observable_set::CS_QEW_Kbar_aq(double & x){return CS_QEW_P_aq(x) * log((1. - x) / x) + 2 * N_c * x * (1. - x);} // T_R -> N_c * [Q²_q]

double observable_set::CS_QEW_Kbar_qq(double & x){return (-(1. + x) * log((1. - x) / x) + (1. - x));} // C_F -> [Q²_q]
double observable_set::CS_QEW_Kbar_qq_plus(double & x){return ((2 / (1. - x)) * log((1. - x) / x));} // C_F -> [Q²_q]
double observable_set::CS_QEW_intKbar_qq_plus(double & x){return (-pow(log(1. - x), 2) - 2 * gsl_sf_dilog(1. - x) + pi2_3);} // C_F -> [Q²_q]

// Kt
double observable_set::CS_QEW_Kt_qa(double & x){return CS_QEW_P_qa(x) * log(1. - x);} // C_F -> [Q²_f]

double observable_set::CS_QEW_Kt_aq(double & x){return CS_QEW_P_aq(x) * log(1. - x);} // T_R -> N_c * [Q²_f]

double observable_set::CS_QEW_Kt_qq(double & x){return CS_QEW_P_qq_reg(x) * log(1. - x);} // C_F -> [Q²_q]
double observable_set::CS_QEW_Kt_qq_plus(double & x){return (2 / (1. - x)) * log(1. - x);} // C_F -> [Q²_q]
double observable_set::CS_QEW_intKt_qq_plus(double & x){return -pow(log(1. - x), 2);} // C_F -> [Q²_q]



void observable_set::initialization_CS_splitting_coefficients_QCD(){
  static Logger logger("observable_set::calculate_splitting_coefficients");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  /*
  CS_gamma_g = (11. / 6.) * C_A - (2. / 3.) * T_R * N_f;
  CS_gamma_g_ferm = - (2. / 3.) * T_R * N_f;
  
  CS_K_g = (67. / 18. - pi2_6) * C_A - (10. / 9.) * T_R * N_f;
  CS_K_g_ferm = - (10. / 9.) * T_R * N_f;
  */


  
  // Identical content, just different names:
  CS_QCD_gamma_g = (11. / 6.) * C_A - (2. / 3.) * T_R * N_f;
  CS_QCD_gamma_g_ferm = - (2. / 3.) * T_R * N_f;
  CS_QCD_gamma_g_bos = (11. / 6.) * C_A;

  CS_QCD_K_g = (67. / 18. - pi2_6) * C_A - (10. / 9.) * T_R * N_f;
  CS_QCD_K_g_ferm = - (10. / 9.) * T_R * N_f;
  CS_QCD_K_g_bos = (67. / 18. - pi2_6) * C_A;
 
  CS_QCD_gamma_q = (3. / 2.) * C_F;
  CS_QCD_K_q = (7. / 2. - pi2_6) * C_F;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
