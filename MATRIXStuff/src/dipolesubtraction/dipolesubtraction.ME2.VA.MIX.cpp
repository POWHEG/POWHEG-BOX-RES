#include "header.hpp"
#include "definitions.observable.set.cxx"

void calculate_ME2_ioperator_VA_MIX(observable_set & oset){
  static Logger logger("calculate_ME2_ioperator_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  static int initialization = 1;
  static vector<double> I_ME2_emitter(osi_VA_ioperator.size());
  if (initialization == 1){
    for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
      osi_VA_ME2_cf[i_a].resize(osi_VA_ioperator[i_a].size());
      osi_VA_I_ME2_cf[i_a].resize(osi_VA_ioperator[i_a].size());
    }
    initialization = 0;
  }

#ifdef OPENLOOPS
  if (oset.switch_OL){

  static int n_momentum = 5 * (osi_n_particle + 2);

  static double *P;
  P = new double[n_momentum];
  for (int i = 1; i < osi_p_parton[0].size(); i++){
    P[5 * (i - 1)]     = osi_p_parton[0][i].x0();
    P[5 * (i - 1) + 1] = osi_p_parton[0][i].x1();
    P[5 * (i - 1) + 2] = osi_p_parton[0][i].x2();
    P[5 * (i - 1) + 3] = osi_p_parton[0][i].x3();
    P[5 * (i - 1) + 4] = osi_p_parton[0][i].m();
  }

  ////  ol_evaluate_tree(1, P, &osi_VA_b_ME2);
  static int n_cc = (osi_n_particle + 2) * (osi_n_particle + 1) / 2;
  static double ewcc = 0.;
  static double *M2cc;
  M2cc = new double[n_cc];
  //  ol_evaluate_cc(1, P, M2cc);
  ////  ol_evaluate_cc(1, P, &osi_VA_b_ME2, M2cc, &ewcc);

  int flag_QCD = 0;
  int flag_QEW = 0;
  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
      if (flag_QCD == 0){
	if (osi_VA_ioperator[i_a][j_a].type_correction() == 1){
	  double dummy_b_ME2;
	  ol_evaluate_cc(osi_VA_ioperator[i_a][j_a].process_id, P, &dummy_b_ME2, M2cc, &ewcc);
	  flag_QCD = 1;
	}
      }
      if (flag_QEW == 0){
	if (osi_VA_ioperator[i_a][j_a].type_correction() == 2){
	  ol_evaluate_tree(osi_VA_ioperator[i_a][j_a].process_id, P, &osi_VA_b_ME2);
	  flag_QEW = 1;
	}
      }
    }
  }

  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    if (osi_VA_ioperator[i_a][0].type_correction() == 1){
      for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
        osi_VA_ME2_cf[i_a][j_a] = M2cc[osi_VA_ioperator[i_a][j_a].no_BLHA_entry];
      }
    }
    else if (osi_VA_ioperator[i_a][0].type_correction() == 2){
      for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
        osi_VA_ME2_cf[i_a][j_a] = osi_VA_ioperator[i_a][j_a].charge_factor() * osi_VA_b_ME2;
      }
    }
  }
  delete [] M2cc;
  delete [] P;
  
  /*
  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
      logger << LOG_DEBUG_VERBOSE << "osi_VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi_VA_ME2_cf[i_a][j_a] << endl;
    }
  }
  */
  }
#endif
#ifdef RECOLA
  if (oset.switch_RCL){

  double P_rec[osi_p_parton[0].size()][4];
  for (int i = 1; i < osi_p_parton[0].size(); i++){
    P_rec[i - 1][0] = osi_p_parton[0][i].x0();
    P_rec[i - 1][1] = osi_p_parton[0][i].x1();
    P_rec[i - 1][2] = osi_p_parton[0][i].x2();
    P_rec[i - 1][3] = osi_p_parton[0][i].x3();
  }

  for (int i = 1; i < osi_p_parton[0].size(); i++){
    stringstream temp_ss;
    for (int j = 0; j < 4; j++){
      temp_ss << setw(23) << setprecision(15) << P_rec[i - 1][j] << "   ";
    }
    logger << LOG_DEBUG_VERBOSE << "P_rec[" << i << "] = " << temp_ss.str() << endl;
  }

  // Correct sub-amplitudes need to be selected (only copied from QCD and QEW versions so far...) !!!
  
  int flag_QCD = 0;
  int flag_QEW = 0;
  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
      if (flag_QCD == 0){
	if (osi_VA_ioperator[i_a][j_a].type_correction() == 1){
	  //	  double dummy_b_ME2;
	  //	  ol_evaluate_cc(osi_VA_ioperator[i_a][j_a].process_id, P, &dummy_b_ME2, M2cc, &ewcc);
	  if (oset.switch_VI == 2){
	    // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.
	    set_alphas_rcl(oset.var_alpha_S_reference, osi_var_mu_ren, oset.N_nondecoupled);
	    logger << LOG_DEBUG_VERBOSE << "set_alphas_rcl(" << setprecision(15) << oset.var_alpha_S_reference << ", " << setprecision(15) << osi_var_mu_ren << ", " << oset.N_nondecoupled << ");" << endl;
	    compute_process_rcl(1, P_rec, "LO");
	    logger << LOG_DEBUG_VERBOSE << "compute_process_rcl(1, P_rec, " << char(34) << "LO" << char(34) << ");" << endl;
	  }
	  compute_all_colour_correlations_rcl(1, P_rec);
	  logger << LOG_DEBUG_VERBOSE << "compute_all_colour_correlations_rcl(1, P_rec);" << endl;

	  flag_QCD = 1;
	}
      }
      if (flag_QEW == 0){
	if (osi_VA_ioperator[i_a][j_a].type_correction() == 2){
	  ///	  ol_evaluate_tree(osi_VA_ioperator[i_a][j_a].process_id, P, &osi_VA_b_ME2);
	  if (oset.switch_VI == 2){
	    // if only I-operator is integrated; otherwise, these amplitudes have already been calculated.
	    set_alphas_rcl(oset.var_alpha_S_reference, osi_var_mu_ren, oset.N_nondecoupled);
	    logger << LOG_DEBUG << "set_alphas_rcl(" << setprecision(15) << oset.var_alpha_S_reference << ", " << setprecision(15) << osi_var_mu_ren << ", " << oset.N_nondecoupled << ");" << endl;
	    compute_process_rcl(1, P_rec, "LO");
	    logger << LOG_DEBUG << "compute_process_rcl(1, P_rec, " << char(34) << "LO" << char(34) << ");" << endl;
	    // needed ???
	    get_squared_amplitude_rcl(1, oset.csi->contribution_order_alpha_s, "LO", osi_VA_b_ME2);
	    logger << LOG_DEBUG << "osi_VA_b_ME2 = " << osi_VA_b_ME2 << endl;
	  }

	  flag_QEW = 1;
	}
      }
    }
  }

  
  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    if (osi_VA_ioperator[i_a][0].type_correction() == 1){
      for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
	///        osi_VA_ME2_cf[i_a][j_a] = M2cc[osi_VA_ioperator[i_a][j_a].no_BLHA_entry];
	get_colour_correlation_rcl(1, oset.csi->contribution_order_alpha_s - 1, osi_VA_ioperator[i_a][j_a].no_emitter(), osi_VA_ioperator[i_a][j_a].no_spectator(), "NLO", osi_VA_ME2_cf[i_a][j_a]);
	logger << LOG_DEBUG_VERBOSE << "get_colour_correlation_rcl(1, " << oset.csi->contribution_order_alpha_s - 1 << ", " << osi_VA_ioperator[i_a][j_a].no_emitter() << ", " << osi_VA_ioperator[i_a][j_a].no_spectator() << ", " << char(34) << "NLO" << char(34) << ", " << setprecision(15) << osi_VA_ME2_cf[i_a][j_a] << ");" << endl;
	logger << LOG_DEBUG_VERBOSE << "osi_VA_ioperator[" << i_a << "][" << j_a << "].colour_factor() = " <<  osi_VA_ioperator[i_a][j_a].colour_factor() << endl;
	osi_VA_ME2_cf[i_a][j_a] = osi_VA_ME2_cf[i_a][j_a] * osi_VA_ioperator[i_a][j_a].colour_factor();
	logger << LOG_DEBUG_POINT << "Recola:     VA_ME2_cf[" << i_a << "][" << j_a << "] = " << setw(23) << setprecision(15) << osi_VA_ME2_cf[i_a][j_a] << endl;
      }
    }
    else if (osi_VA_ioperator[i_a][0].type_correction() == 2){
      for (int j_a = 0; j_a < osi_VA_ioperator[i_a].size(); j_a++){
	///        osi_VA_ME2_cf[i_a][j_a] = osi_VA_ioperator[i_a][j_a].charge_factor() * osi_VA_b_ME2;
	logger << LOG_DEBUG << "osi_VA_ioperator[" << i_a << "][" << j_a << "].charge_factor() = " << osi_VA_ioperator[i_a][j_a].charge_factor() << endl;
	osi_VA_ME2_cf[i_a][j_a] = osi_VA_ioperator[i_a][j_a].charge_factor() * osi_VA_b_ME2;
	logger << LOG_DEBUG << "osi_VA_ME2_cf[" << i_a << "][" << j_a << "] = " << osi_VA_ME2_cf [i_a][j_a] << endl;
      }
    }
  }

  }
#endif
  
  

  if (osi_massive_QCD){oset.calculate_ioperator_QCD_CDST();}
  else {oset.calculate_ioperator_QCD_CS();}
  
  if (osi_massive_QEW){oset.calculate_ioperator_QEW_CDST();}
  else {oset.calculate_ioperator_QEW_CS();}

  for (int i_a = 0; i_a < osi_VA_ioperator.size(); i_a++){
    I_ME2_emitter[i_a] = accumulate(osi_VA_I_ME2_cf[i_a].begin(), osi_VA_I_ME2_cf[i_a].end(), 0.);
  }
  osi_VA_I_ME2 = accumulate(I_ME2_emitter.begin(), I_ME2_emitter.end(), 0.);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void calculate_ME2_VA_MIX(observable_set & oset){
  static Logger logger("calculate_ME2_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (osi_switch_VI == 0 || osi_switch_VI == 1){

#ifdef OPENLOOPS
    if (oset.switch_OL){

    static double one = 1;
    static int n_momentum = 5 * (osi_n_particle + 2);
    double *P;
    P = new double[n_momentum];
    for (int i = 1; i < osi_p_parton[0].size(); i++){
      P[5 * (i - 1)]     = osi_p_parton[0][i].x0();
      P[5 * (i - 1) + 1] = osi_p_parton[0][i].x1();
      P[5 * (i - 1) + 2] = osi_p_parton[0][i].x2();
      P[5 * (i - 1) + 3] = osi_p_parton[0][i].x3();
      P[5 * (i - 1) + 4] = osi_p_parton[0][i].m();
    }
    //    static char * renscale = stch("renscale");
    static char * OL_mu_ren = stch("muren");
    static char * OL_mu_reg = stch("mureg");
    static char * fact_uv = stch("fact_uv");
    static char * fact_ir = stch("fact_ir");
    ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
    if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
    else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
    else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}

    ol_setparameter_double(fact_uv, one);
    ol_setparameter_double(fact_ir, one);

    static double acc;
    static double M2L0;
    double *M2L1;
    double *IRL1;
    M2L1 = new double[3];
    IRL1 = new double[3];
    double *M2L2;
    double *IRL2;
    M2L2 = new double[5];
    IRL2 = new double[5];
    ol_evaluate_full(1, P, &osi_VA_b_ME2, M2L1, IRL1, M2L2, IRL2, &acc);
 
    osi_VA_V_ME2 = M2L1[0];
    if (osi_switch_VI == 0){
      if (osi_VA_DeltaIR1 == 0. && osi_VA_DeltaIR2 == 0.){osi_VA_I_ME2 = IRL1[0];}
      else {osi_VA_I_ME2 = IRL1[0] + osi_VA_DeltaIR1 * IRL1[1] + osi_VA_DeltaIR2 * IRL1[2];}
      //  osi_VA_I_ME2 = i_DeltaIR1 * IR1 + osi_VA_DeltaIR2 * IR2; // !!! I-operator switched off !!!
    }

    ol_evaluate_ct(1, P, &M2L0, &osi_VA_X_ME2);
    if (osi_switch_CV){
      for (int s = 0; s < osi_n_scales_CV; s++){
        double inv_factor_CV = osi_var_mu_ren_CV[s] / osi_var_mu_ren;
        ol_setparameter_double(OL_mu_ren, osi_var_mu_ren_CV[s]);
	if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren_CV[s]);}
	else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
	else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}

        ol_setparameter_double(fact_uv, inv_factor_CV);
        ol_setparameter_double(fact_ir, inv_factor_CV);
	ol_evaluate_ct(1, P, &M2L0, &osi_VA_X_ME2_CV[s]);
	logger << LOG_DEBUG_VERBOSE << "after ol_evaluate_ct s = " << s << endl;
      }
    }
    if (osi_switch_TSV){
      for (int i_v = 0; i_v < osi_max_dyn_ren + 1; i_v++){
	for (int i_r = 0; i_r < osi_n_scale_dyn_ren[i_v]; i_r++){
	  double inv_factor_TSV = osi_value_scale_ren[0][i_v][i_r] / osi_var_mu_ren;
	  ol_setparameter_double(OL_mu_ren, osi_value_scale_ren[0][i_v][i_r]);
	  if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_value_scale_ren[0][i_v][i_r]);}
	  else if (osi_user_double_value[osi_user_double_map["mu_reg"]] == -1.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
	  else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
	  ol_setparameter_double(fact_uv, inv_factor_TSV);
	  ol_setparameter_double(fact_ir, inv_factor_TSV);
	  ol_evaluate_ct(1, P, &M2L0, &osi_value_ME2term_ren[0][i_v][i_r]);
	  osi_value_ME2term_ren[0][i_v][i_r] += osi_VA_V_ME2 + osi_VA_I_ME2;
	  logger << LOG_DEBUG_VERBOSE << "after ol_evaluate_ct i_v = " << i_v << "   i_r = " << i_r << endl;
	}
      }
    }
    delete [] M2L2;
    delete [] IRL2;
    delete [] M2L1;
    delete [] IRL1;
    delete [] P;
    }    
#endif
#ifdef RECOLA
    if (oset.switch_RCL){
    }
#endif
  }
  else if (osi_switch_VI == 2){
#ifdef OPENLOOPS
    if (oset.switch_OL){
    logger << LOG_DEBUG_VERBOSE << "OL: osi_VA_I_ME2 = " << osi_VA_I_ME2 << endl;
    //  osi_VA_I_ME2 = 0.;
    //  osi_VA_V_ME2 = 0.;
    //  osi_VA_X_ME2 = 0.;
    //  for (int s = 0; s < osi_n_scales_CV; s++){osi_VA_X_ME2_CV[s] = 0.;}
    //  I-operator evaluation -> osi_VA_I_ME2
    calculate_ME2_ioperator_VA_MIX(oset);
    for (int i_v = 0; i_v < osi_max_dyn_ren + 1; i_v++){
      for (int i_r = 0; i_r < osi_n_scale_dyn_ren[i_v]; i_r++){
        osi_value_ME2term_ren[0][i_v][i_r] = osi_VA_I_ME2;
      }
    }
    logger << LOG_DEBUG_VERBOSE << "SK: osi_VA_I_ME2 = " << osi_VA_I_ME2 << endl;
    }
#endif
#ifdef RECOLA
    if (oset.switch_RCL){
    }
#endif
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}



void calculate_ME2check_VA_MIX(observable_set & oset){
  static Logger logger("calculate_ME2check_VA_MIX");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  // Not yet updated to RECOLA and scale-dependent testpoint evaluation, etc.
#ifdef OPENLOOPS

  if (oset.switch_output_comparison){

    oset.xmunich->generic.phasespacepoint_psp(oset);
    
    if (osi_p_parton[0][0].x0() == 0.){
      if (oset.switch_OL){oset.testpoint_from_OL_rambo();}
    }
   
    if (osi_p_parton[0][0].x0() != 0.){
      ofstream out_comparison;
      out_comparison.open(osi_filename_comparison.c_str(), ofstream::out | ofstream::app);  
      
      perform_event_selection(oset, oset.xmunich->generic);
      if (oset.cut_ps[0] == -1){
	out_comparison << "Phase-space 0 is cut. -> Default scale is used." << endl;
	for (int sd = 1; sd < oset.max_dyn_ren + 1; sd++){
	  for (int ss = 0; ss < oset.n_scale_dyn_ren[sd]; ss++){
	    oset.value_scale_ren[0][sd][ss] = 100.;
	  }
	}
	for (int sd = 1; sd < oset.max_dyn_fact + 1; sd++){
	  for (int ss = 0; ss < oset.n_scale_dyn_fact[sd]; ss++){
	    oset.value_scale_fact[0][sd][ss] = 100.;
	  }
	}
	osi_var_mu_ren = 100.;
	osi_var_mu_fact = 100.;
	
	oset.cut_ps[0] = 0;
      }
      else {
	// Maybe not valid any longer:
	// 1 - Testpoint is calculated at basic fixed scale (prefactor * scale_ren).
	// 2 - Testpoint is calculated at output scale.
	oset.xmunich->generic.calculate_dynamic_scale(0, oset);
	oset.xmunich->generic.calculate_dynamic_scale_TSV(0, oset);
	oset.determine_scale();
	/////  }
	logger << LOG_DEBUG_VERBOSE << "back" << endl;
      }
      //    static char * renscale = stch("renscale");
      static char * OL_mu_ren = stch("muren");
      static char * OL_mu_reg = stch("mureg");
      
      static char * pole_uv = stch("pole_uv");
      static char * pole_ir1 = stch("pole_ir1");
      static char * pole_ir2 = stch("pole_ir2");
      //  static char * me_cache = stch("me_cache");
      
      static char * stability_mode = stch("stability_mode");
      static char * redlib1 = stch("redlib1");
      static char * redlib2 = stch("redlib2");

      static char * OL_alpha_s = stch("alpha_s");

      osi_VA_delta_flag = 1;
      string s_Delta;
      osi_VA_b_ME2 = 0.;
      osi_VA_V_ME2 = 0.;
      osi_VA_X_ME2 = 0.;
      osi_VA_I_ME2 = 0.;
      osi_VA_X_ME2_CV.resize(osi_n_scales_CV, 0.);
      ///  if (osi_p_parton[0][0].x0() != 0.){
      ///    ofstream out_comparison;
      ///    out_comparison.open(osi_filename_comparison.c_str(), ofstream::out | ofstream::app);  
      
      if (oset.switch_output_comparison == 2){
	ol_setparameter_double(OL_alpha_s, oset.var_alpha_S_reference);
      }


      double i_Delta;
      osi_VA_DeltaUV = 0.;
      osi_VA_DeltaIR1 = 0.;
      osi_VA_DeltaIR2 = 0.;
      
      ///      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      ///    if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
      ///    else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}

      double temp_mu_reg = 0.;
      if (oset.switch_OL){
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      //      ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
      if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	temp_mu_reg = osi_var_mu_ren;
      }
      else {
	ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);
	temp_mu_reg = osi_user_double_value[osi_user_double_map["mu_reg"]];
      }

      ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
      ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
      ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
      //  ol_setparameter_double(me_cache, 0);
      }
      
#ifdef RECOLA
      if (oset.switch_RCL){
      set_delta_uv_rcl(osi_VA_DeltaUV);
      set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
      }
#endif
    
      if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}

      calculate_ME2_VA_MIX(oset);
      //    OLP_PrintParameter(stch("olparameters.txt"));
      //    if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}
      //    double ME2 = 0;

      
    out_comparison << "Absolute results: " << endl << endl;
    oset.output_testpoint_VA_result(out_comparison);
    double OL_I_ME2 = osi_VA_I_ME2;
    if (osi_switch_VI != 2){
      osi_VA_I_ME2 = 0.;
      calculate_ME2_ioperator_VA_MIX(oset);
     //      double SK_I_ME2 = osi_VA_I_ME2;
      oset.output_testpoint_VA_ioperator(out_comparison);
      osi_VA_I_ME2 = OL_I_ME2;
    }
    osi_VA_V_ME2 = osi_VA_V_ME2 / osi_VA_b_ME2;
    osi_VA_X_ME2 = osi_VA_X_ME2 / osi_VA_b_ME2;
    osi_VA_I_ME2 = osi_VA_I_ME2 / osi_VA_b_ME2;
    //    osi_VA_b_ME2 = osi_VA_b_ME2;
    out_comparison << "Relative results (corrections devided by ME2_born, ME2_born divided by coupling constants): " << endl << endl;
    oset.output_testpoint_VA_result(out_comparison);
    out_comparison << endl;
    out_comparison << "Particle momenta: " << endl << endl;
    output_momenta(out_comparison, oset);
    ///    out_comparison << "On-shell-projected particle momenta: " << endl << endl;
    ///    output_momenta(out_comparison, oset);
    out_comparison << "Numerical check of (UV and IR) finiteness: " << endl << endl;

    /*
    // Delta_UV =/= Delta_IR not allowed in CutTOols !!!
    int set_OL_stability_mode;
    ol_getparameter_int(stability_mode, &set_OL_stability_mode);
    int temp_OL_stability_mode = set_OL_stability_mode;
    int set_OL_redlib1;
    ol_getparameter_int(redlib1, &set_OL_redlib1);
    int temp_OL_redlib1 = set_OL_redlib1;
    int set_OL_redlib2;
    ol_getparameter_int(redlib2, &set_OL_redlib2);
    int temp_OL_redlib2 = set_OL_redlib2;
    //    char* set_OL_model;
    //    ol_getparameter_string("model", set_OL_model);
    string set_OL_model;
    for (int i_o = 0; i_o < osi_OL_parameter.size(); i_o++){
      if (osi_OL_parameter[i_o] == "model"){set_OL_model = osi_OL_value[i_o];}
    }
    string temp_OL_model = set_OL_model;

    logger << LOG_DEBUG << "set_OL_stability_mode  = " << set_OL_stability_mode << endl;
    logger << LOG_DEBUG << "set_OL_redlib1         = " << set_OL_redlib1 << endl;
    logger << LOG_DEBUG << "set_OL_redlib2         = " << set_OL_redlib2 << endl;
    logger << LOG_DEBUG << "set_OL_model           = " << set_OL_model << endl;
    */
    int temp_switch_check_IRneqUV = 1;
    /*
    if (set_OL_model == "heft"){temp_switch_check_IRneqUV = 0;}
    else if (set_OL_stability_mode < 20){temp_switch_check_IRneqUV = 0;}
    else {
      if (set_OL_stability_mode == 23){
	temp_OL_stability_mode = 21;
	if      (set_OL_redlib1 == 5 && set_OL_redlib2 == 1){temp_OL_redlib1 = 7;}
	else if (set_OL_redlib1 == 5 && set_OL_redlib2 == 7){temp_OL_redlib1 = 1;}
	else if (set_OL_redlib2 == 5 && set_OL_redlib1 == 1){temp_OL_redlib2 = 7;}
	else if (set_OL_redlib2 == 5 && set_OL_redlib1 == 7){temp_OL_redlib2 = 1;}

	ol_setparameter_int(stch(stability_mode), temp_OL_stability_mode);
	ol_setparameter_int(stch(redlib1), temp_OL_redlib1);
	ol_setparameter_int(stch(redlib2), temp_OL_redlib2);
      }
      logger << LOG_DEBUG << "temp_OL_stability_mode = " << temp_OL_stability_mode << endl;
      logger << LOG_DEBUG << "temp_OL_redlib1        = " << temp_OL_redlib1 << endl;
      logger << LOG_DEBUG << "temp_OL_redlib2        = " << temp_OL_redlib2 << endl;
      logger << LOG_DEBUG << "temp_OL_model          = " << temp_OL_model << endl;
    }
*/
      
    if (temp_switch_check_IRneqUV){
    s_Delta = "Delta_UV";
    for (int i = 0; i < 3; i++){
      i_Delta = (double(i) - 1.) * 1.;
      osi_VA_DeltaUV = i_Delta;

      /*
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
      else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
      */
        if (oset.switch_OL){
	ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	//  ol_setparameter_double(me_cache, 0);
	}
#ifdef RECOLA
	if (oset.switch_RCL){
	set_delta_uv_rcl(osi_VA_DeltaUV);
	set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif

      calculate_ME2_VA_MIX(oset);
      oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
    }
    osi_VA_DeltaUV = 0.;
    out_comparison << endl;

    s_Delta = "Delta_IR_1";
    for (int i = 0; i < 3; i++){
      i_Delta = (double(i) - 1.) * 1.;
      osi_VA_DeltaIR1 = i_Delta;
      /*
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
      else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
      */
	if (oset.switch_OL){
	ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	//  ol_setparameter_double(me_cache, 0);
	}
      
#ifdef RECOLA
	if (oset.switch_RCL){
	set_delta_uv_rcl(osi_VA_DeltaUV);
	set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif

	calculate_ME2_VA_MIX(oset);
      oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
    }
    osi_VA_DeltaIR1 = 0.;
    out_comparison << endl;
    }

    /*
    if (set_OL_stability_mode != temp_OL_stability_mode){
      ol_setparameter_int(stability_mode, set_OL_stability_mode);
      ol_setparameter_int(redlib1, set_OL_redlib1);
      ol_setparameter_int(redlib2, set_OL_redlib2);
    }
    */

    s_Delta = "Delta_IR_2";
    for (int i = 0; i < 3; i++){
      i_Delta = (double(i) - 1.) * 1.;
      osi_VA_DeltaIR2 = i_Delta;
      /*
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
      else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
      */
#ifdef OPENLOOPS
      if (oset.switch_OL){
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
      ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
      ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
      ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
      //  ol_setparameter_double(me_cache, 0);
      }
#endif
#ifdef RECOLA
      if (oset.switch_RCL){
      set_delta_uv_rcl(osi_VA_DeltaUV);
      set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
      }
#endif

      calculate_ME2_VA_MIX(oset);
      oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
    }
    osi_VA_DeltaIR2 = 0.;
    out_comparison << endl;
    s_Delta = "Delta_UV = Delta_IR_1";
    for (int i = 0; i < 3; i++){
      i_Delta = (double(i) - 1.) * 1.;
      osi_VA_DeltaUV = i_Delta;
      osi_VA_DeltaIR1 = i_Delta;
      /*
      ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
      if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
      else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}
      */
	if (oset.switch_OL){
	ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
	ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);
	ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
	ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
	ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
	//  ol_setparameter_double(me_cache, 0);
	}
#ifdef RECOLA
	if (oset.switch_RCL){
	set_delta_uv_rcl(osi_VA_DeltaUV);
	set_delta_ir_rcl(osi_VA_DeltaIR1, osi_VA_DeltaIR2 + (1. - osi_switch_polenorm) * pi2_6);
	}
#endif

	calculate_ME2_VA_MIX(oset);
      oset.output_testpoint_VA_Delta(out_comparison, i, i_Delta, s_Delta);
    }
    osi_VA_DeltaUV = 0.;
    osi_VA_DeltaIR1 = 0.;
    out_comparison << endl;

    ol_setparameter_double(OL_mu_ren, osi_var_mu_ren);
    if (osi_user_double_value[osi_user_double_map["mu_reg"]] == 0.){ol_setparameter_double(OL_mu_reg, osi_var_mu_ren);}
    else {ol_setparameter_double(OL_mu_reg, osi_user_double_value[osi_user_double_map["mu_reg"]]);}

    ol_setparameter_double(pole_uv, osi_VA_DeltaUV);
    ol_setparameter_double(pole_ir1, osi_VA_DeltaIR1);
    ol_setparameter_double(pole_ir2, osi_VA_DeltaIR2);
    //  ol_setparameter_double(me_cache, 0);
    calculate_ME2_VA_MIX(oset);
    osi_VA_delta_flag = 0;
    out_comparison.close();
    }
    else {
      if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}
    }
    
    oset.switch_output_comparison = 0;
  }
  else {
    if (oset.switch_OL){OLP_PrintParameter(stch("log/olparameters." + osi_name_process + ".txt"));}
  }
#endif
  

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}




