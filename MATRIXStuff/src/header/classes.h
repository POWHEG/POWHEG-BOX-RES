//  classes/header/...
#include "fourvector.h"
#include "particle.h"
#include "contribution.set.h"
#include "define.particle.set.h"
#include "fiducialcut.h"
#include "event.set.h"
#include "multicollinear.set.h"
#include "user.defined.h"
#include "inputparameter.set.h"
#include "xdistribution.h"
#include "dddistribution.h"

//  dipolesubtraction/... (should be moved there!!!)
#include "dipole.set.h"
#include "ioperator.set.h"
#include "collinear.set.h"

//  classes/...
#include "correlationoperator.set.h"
#include "model.set.h"

//  phasespace/...
#include "random.number.generator.h"
#include "multichannel.set.h"
#include "importancesampling.set.h"
#include "phasespace.set.h"
#include "randomvariable.h"
#include "randommanager.h"

//  classes/header/...
#include "observable.set.h"
#include "runresumption.set.h"
#include "logger.h"
#include "call.generic.h"
#include "topwidth.h"

// summary/...
#include "summary.subprocess.h"
#include "summary.contribution.h"
#include "summary.list.h"
#include "summary.order.h"
#include "summary.generic.h"

//  munich/...
#include "munich.h"

