#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <algorithm> 

#include "header.hpp" // -> This includes everything you need, together with .h file with classes declarations
#include "pplla23.amplitude.doublevirtual.h" // -> MATRIX function declarations      
#include "ppllll24.amplitude.doublevirtual.h" // -> MATRIX function declarations      
#include "ggllll34.amplitude.doublevirtual.h" // -> MATRIX function declarations      


inputparameter_set my_iset;
contribution_set my_cset;
observable_set my_oset(my_iset,my_cset);

extern "C"
{
  int static length;

  // Internal utilities

  bool is_there_v(int flav[])
  {
      for(int i=2; i<length;i++)
	{
	  if(abs(flav[i])==12 || abs(flav[i])==14 || abs(flav[i])==16){return true;}
	}
    return false;
  }

  bool same_lepton(int flav[])
  {
    int first_flav=flav[2];
      for(int i=2; i<length;i++)
	{
	  if(abs(flav[i])!=abs(first_flav)){return false;}
	}
    return true;
  }

  void query_neutrinos(int &nu, int flav[])
  {
    // it works with two neutrinos 
    int neutrinos[2],leptons[2];
    int v_count=0,l_count=0;
    for(int i=2; i<length;i++)
      {
	if(abs(flav[i])==12 || abs(flav[i])==14 || abs(flav[i])==16)
	  { neutrinos[v_count]=flav[i]; v_count+=1; }
	else if(abs(flav[i])==11 || abs(flav[i])==13 || abs(flav[i])==15)
	  {  leptons[l_count]=flav[i]; l_count+=1; }
      }

    if(v_count!=2){assert(false&&"Error while counting neutrinos");}
    else if(l_count!=2){assert(false&&"Error while counting leptons");}
    
    if( abs(neutrinos[0]) != abs(neutrinos[1]) ) {nu=1;}
    else if( abs(neutrinos[0])==12 && abs(leptons[0])==11 || abs(neutrinos[0])==14 && abs(leptons[0])==13 || abs(neutrinos[0])==16 && abs(leptons[0])==15 ) {nu=2;}
    else  {nu=3;}
      
  }

  void count_same_leptons(int &equal_l, int flav[])
  {
    int first_flav=flav[2];
    equal_l=1;
    for(int i=2; i<length;i++)
      {
	if(abs(flav[i])==abs(first_flav)){equal_l+=1;}
      }
  }


  
  extern void matrix_init_parameters_(char * process_class_in,double & Zmass,double & Gamma_Z,double & Wmass,double & Gamma_W,double & alpha_em,int & nparticles, int & nf,bool& cms)
  {
    string process_class = process_class_in;
    length=nparticles;
    my_oset.csi->process_class = process_class;
    my_oset.msi.M_Z=Zmass;
    my_oset.msi.Gamma_Z=Gamma_Z;
    my_oset.msi.M_W=Wmass;
    my_oset.msi.Gamma_W=Gamma_W;
    my_oset.msi.alpha_e=alpha_em;
    my_oset.N_f=nf;

    if(cms){
      complex<double> c_Zmass2(Zmass*Zmass,- Zmass * Gamma_Z);
      complex<double> c_Wmass2(Wmass*Wmass,- Wmass * Gamma_W);
      my_oset.msi.csin_w=sqrt(1.0-c_Wmass2/c_Zmass2);}
    else { 
      my_oset.msi.csin_w=sqrt(1.0-pow(Wmass,2)/pow(Zmass,2));
    }
    my_oset.msi.ccos_w=sqrt(1.0-pow(my_oset.msi.csin_w,2));
    my_oset.p_parton.resize(4, vector<fourvector> (nparticles+1, fourvector ()));
  }

  extern void matrix_update_alphas_(double & alphaS)
  {
    my_oset.alpha_S=alphaS;
  }


  int flav0,flav1;
  
  extern void matrix_update_kinematics_(int flav[],double p[][4])
  { 

    flav0=flav[0];
    flav1=flav[1];
    
    if(my_oset.csi->process_class == "pp-emepa+X"){ // Z -> e+ e- gamma 
	if(abs(flav[0])==1){  my_oset.name_process = "dd~_emepa";}
	else if(abs(flav[0])==2){  my_oset.name_process = "uu~_emepa";}
	else if(abs(flav[0])==3){  my_oset.name_process = "ss~_emepa";}
	else if(abs(flav[0])==4){  my_oset.name_process = "cc~_emepa";}
	else if(abs(flav[0])==5){  my_oset.name_process = "bb~_emepa";}
	else {assert(false&&"Error: unknown flavour");}
      }
    else if (my_oset.csi->process_class == "pp-veve~a+X"){ // Z -> ne ne~ gamma 
	if(abs(flav[0])==1){  my_oset.name_process = "dd~_veve~a";}
	else if(abs(flav[0])==2){  my_oset.name_process = "uu~_veve~a";}
	else if(abs(flav[0])==3){  my_oset.name_process = "ss~_veve~a";}
	else if(abs(flav[0])==4){  my_oset.name_process = "cc~_veve~a";}
	else if(abs(flav[0])==5){  my_oset.name_process = "bb~_veve~a";}
	else {assert(false&&"Error: unknown flavour");}
    }
    else if(my_oset.csi->process_class == "pp-emmupvmve~+X" || my_oset.csi->process_class == "pp-emmumepmup+X" || my_oset.csi->process_class == "pp-emepmupvm+X")
      {
	if(flav[0]==0 && flav[0]==-flav[1])
	  {
	    if(!is_there_v(flav)) // process with 4 leptons
	      {
		if(same_lepton(flav)){my_oset.name_process ="gg_ememepep";}
		else{my_oset.name_process = "gg_emmumepmup";}
	      }
	    else // process with neutrinos
	      {
		int nu;
		query_neutrinos(nu,flav);
		if(nu==1){my_oset.name_process ="gg_emmupvmve~";} //WW
		else if(nu==2){my_oset.name_process ="gg_emepveve~";}
		else if(nu==3){my_oset.name_process ="gg_emepvmvm~";}
		else {assert(false&&"Error: unknown neutrino configuration");}
	      }
	  }
	else if(flav[0]==-flav[1])
	  {
	    if(abs(flav[0])==5) // bbb
	      {
		if(!is_there_v(flav))
		  {
		    if(same_lepton(flav)){my_oset.name_process ="bb~_ememepep";}
		    else{my_oset.name_process = "bb~_emmumepmup";}
		  }
		else // process with neutrinos
		  {
		    int nu;
		    query_neutrinos(nu,flav);
		    if(nu==1){my_oset.name_process ="bb~_emmupvmve~";} //WW
		    else if(nu==2){my_oset.name_process ="bb~_emepveve~";}
		    else if(nu==3){my_oset.name_process ="bb~_emepvmvm~";}
		    else {assert(false&&"Error: unknown neutrino configuration");}
		  }
	      }
	    else if((abs(flav[0])%2)==0) // uub - ccb
	      {
		if(!is_there_v(flav))
		  {
		    if(same_lepton(flav)){my_oset.name_process ="uu~_ememepep";}
		    else{my_oset.name_process = "uu~_emmumepmup";}
		  }
		else // process with neutrinos
		  {
		    int nu;
		    query_neutrinos(nu,flav);
		    if(nu==1){my_oset.name_process ="uu~_emmupvmve~";} //WW
		    else if(nu==2){my_oset.name_process ="uu~_emepveve~";}
		    else if(nu==3){my_oset.name_process ="uu~_emepvmvm~";}
		    else {assert(false&&"Error: unknown neutrino configuration");}
		  }	
	      }
	    else // ddb - ssb  
	      {
		if(!is_there_v(flav))
		  {
		    if(same_lepton(flav)){my_oset.name_process ="dd~_ememepep";}
		    else{my_oset.name_process = "dd~_emmumepmup";}
		  }
		else // process with neutrinos
		  {
		    int nu;
		    query_neutrinos(nu,flav);
		    if(nu==1){my_oset.name_process ="dd~_emmupvmve~";} //WW
		    else if(nu==2){my_oset.name_process ="dd~_emepveve~";}
		    else if(nu==3){my_oset.name_process ="dd~_emepvmvm~";}
		    else {assert(false&&"Error: unknown neutrino configuration");}
		  }
	      }	    
	  }
	else if(((abs(flav[0])%2)==0 && flav[0]>0 && flav[1]<0) || ((abs(flav[1])%2)==0 && flav[1]>0 && flav[0]<0)) //ZW^+
	  {
	    int equal_l;
	    count_same_leptons(equal_l,flav);
	      if(equal_l==2){my_oset.name_process ="ud~_emepmupvm";} 
	      else if(equal_l==3){my_oset.name_process ="ud~_emepepve";}
	      else {assert(false&&"Error: unknown lepton configuration");}
	  }
	else if(((abs(flav[0])%2)==1 && flav[0]>0 && flav[1]<0) || ((abs(flav[1])%2)==1 && flav[1]>0 && flav[0]<0)) //ZW^-
	  {
	    int equal_l;
	    count_same_leptons(equal_l,flav);
	      if(equal_l==2){my_oset.name_process ="du~_emmumepvm~";} 
	      else if(equal_l==3){my_oset.name_process ="du~_ememepve~";}
	      else {assert(false&&"Error: unknown lepton configuration");}
	  }
	else {assert(false&&"Error: unknown flavour");}
      }
	  
    my_oset.p_parton[0][0]  = fourvector( p[0][0]+p[1][0] , p[0][1]+p[1][1] , p[0][2]+p[1][2] ,  p[0][3]+p[1][3]  );
    for (int i=0;i<length;i++){
      my_oset.p_parton[0][i+1]  = fourvector( p[i][0] , p[i][1] , p[i][2] ,  p[i][3]    );
    }

    if(flav[0]<0){
      my_oset.p_parton[0][1]  = fourvector( p[1][0] , p[1][1] , p[1][2] ,  p[1][3]    ); // plus direction 
      my_oset.p_parton[0][2]  = fourvector( p[0][0] , p[0][1] , p[0][2] ,  p[0][3]    ); // minus direction  
    }
    // Specific WW momentum flips to match POWHEG conventions
    // POWHEG mu+(2) vm(3)  e-(4) ve~(5)  
    // MATRIX e-(3) mu+(4) vm(5) ve~(6) 
    if(my_oset.csi->process_class == "pp-emmupvmve~+X"){
	my_oset.p_parton[0][3]  = fourvector( p[4][0] , p[4][1] , p[4][2] ,  p[4][3]    );
	my_oset.p_parton[0][4]  = fourvector( p[2][0] , p[2][1] , p[2][2] ,  p[2][3]    );
	my_oset.p_parton[0][5]  = fourvector( p[3][0] , p[3][1] , p[3][2] ,  p[3][3]    );
    }

    // Specific ZZ momentum flips to match POWHEG conventions
   if(my_oset.csi->process_class == "pp-emmumepmup+X"){
     if(my_oset.name_process == "bb~_emmumepmup" || my_oset.name_process == "uu~_emmumepmup"
	|| my_oset.name_process == "dd~_emmumepmup" || my_oset.name_process == "bb~_ememepep" 
	|| my_oset.name_process == "uu~_ememepep" || my_oset.name_process == "dd~_ememepep"){
       // 4l DF + 4l SF (no reshuffling needed for 2l2nu DF and 2l2n SF)
       // POWHEG e-(2) e+(3)  mu-(4) mu+(5)  
       // MATRIX e-(3) mu-(4) e+(5) mu+(6) 
       my_oset.p_parton[0][4]  = fourvector( p[4][0] , p[4][1] , p[4][2] ,  p[4][3]    );
       my_oset.p_parton[0][5]  = fourvector( p[3][0] , p[3][1] , p[3][2] ,  p[3][3]    );}
   }

   if(my_oset.csi->process_class == "pp-emepmupvm+X"){
     if(((abs(flav[0])%2)==0 && flav[0]>0 && flav[1]<0) || ((abs(flav[1])%2)==0 && flav[1]>0 && flav[0]<0)){ 
       // ZW^+ contribution "ud~_emepmupvm" "ud~_emepepve"
       // POWHEG mu+(2) vm(3)  e-(4)  e+(5)  
       // MATRIX e-(3) e+(4)  mu+(5)  vm(6) 
	my_oset.p_parton[0][3]  = fourvector( p[4][0] , p[4][1] , p[4][2] ,  p[4][3]    );
	my_oset.p_parton[0][4]  = fourvector( p[5][0] , p[5][1] , p[5][2] ,  p[5][3]    );
	my_oset.p_parton[0][5]  = fourvector( p[2][0] , p[2][1] , p[2][2] ,  p[2][3]    );
	my_oset.p_parton[0][6]  = fourvector( p[3][0] , p[3][1] , p[3][2] ,  p[3][3]    );}
     else if(((abs(flav[0])%2)==1 && flav[0]>0 && flav[1]<0) || ((abs(flav[1])%2)==1 && flav[1]>0 && flav[0]<0)){ 
       // ZW^- contribution "du~_emmumepvm~" "du~_ememepve~"
       // POWHEG mu-(2) vm~(3)  e-(4)  e+(5)  
       // MATRIX e-(3) mu-(4)  e+(5)  vm~(6) 
	my_oset.p_parton[0][3]  = fourvector( p[4][0] , p[4][1] , p[4][2] ,  p[4][3]    );
	my_oset.p_parton[0][4]  = fourvector( p[2][0] , p[2][1] , p[2][2] ,  p[2][3]    );
	my_oset.p_parton[0][5]  = fourvector( p[5][0] , p[5][1] , p[5][2] ,  p[5][3]    );
	my_oset.p_parton[0][6]  = fourvector( p[3][0] , p[3][1] , p[3][2] ,  p[3][3]    );}
     }

  }



  extern struct{
    int mode,use_mcfm;
  } rescue_2loop_;
  
  int H2_mode=0;
  bool use_mcfm = true;

  void calculate_H2(observable_set & my_oset){
#ifdef PPLLA23
    pplla23_calculate_H2(my_oset);
#elif PPLLLL24
    H2_mode=rescue_2loop_.mode;
    use_mcfm = rescue_2loop_.use_mcfm;
    ppllll24_calculate_H2(my_oset);
#elif GGLLLL34
    ggllll34_calculate_H1(my_oset);    
#endif
  }



  void fill_hist(int flav0,int flav1,double val,double approx,double exact){
    static bool ini[4]={true,true,true,true};
    static bool ini_raw=true,ini_test=true;
    static int count=0;
    const int nbins=325;
    static int max_count=1000;
    const int dc=1000;
    static double y[4][nbins];
    static double x[nbins+1];
    int index;
    static vector<vector<double>>vec_val(4);
    static vector<vector<double>>vec_approx(4),vec_exact(4);
    static int bad_points[4],ibad_points[4];
    static int bad_rescued_points[4],ibad_rescued_points[4];
    static int rescued_points[4];
    bool rescued=(approx>500 || approx<-100) ;
    ofstream bad_file;
    //**********************************************//
    // THIS IS FOR TESTING INTERPOLATOR PERFORMANCES IN PRESENCE OF THE RESCUE SYSTEM
    //**********************************************//
    //if(rescued){val=0.0;}
    //**********************************************//

    if(flav0==-4 && flav1==4){++count;}
    if(flav0==-4 && flav1==4){index=0;}
    else if(flav0==-3 && flav1==3){index=1;}
    else if(flav0==1 && flav1==-1){index=2;}
    else if(flav0==2 && flav1==-2){index=3;}

    if(ini[index]){
      ini[index]=false;
      for(int i=0;i<nbins;++i){y[index][i]=0;}
      for(int i=0;i<nbins+1;++i){x[i]=-1+i*(2./nbins);}
      bad_points[index]=0;
      bad_rescued_points[index]=0;
      ibad_points[index]=0;
      ibad_rescued_points[index]=0;
      rescued_points[index]=0;
      bad_file.open("bad_points_info.dat");
      bad_file.close();
    }
    

    for(int i=0;i<nbins;++i){
      if(val>=x[i] && val<x[i+1]){y[index][i]+=1./max_count;}
    }
    vec_val[index].push_back(val);

    vec_approx[index].push_back(approx);
    vec_exact[index].push_back(exact);

    if(abs(val)>1){
      bad_points[index]+=1;
      if(rescued){bad_rescued_points[index]+=1;}
      else{
	bad_file.open("bad_points_info.dat",ios::out | ios::app);
	bad_file<<index<<") UNRESCUED |err|>100% "<<val*100<<" --> I1 "<<approx<<" E "<<exact<<"\n";
	bad_file.close();
      }
    }
    else if(abs(val)>=0.25){
      ibad_points[index]+=1;
      if(rescued){ibad_rescued_points[index]+=1;}
      else{
	bad_file.open("bad_points_info.dat",ios::out | ios::app);
	bad_file<<index<<") UNRESCUED 25%<|err|<100% "<<val*100<<" --> I1 "<<approx<<" E "<<exact<<"\n";
	bad_file.close();
      }
    }
    else if(rescued){
      rescued_points[index]+=1;
      bad_file.open("bad_points_info.dat",ios::out | ios::app);
      bad_file<<index<<") GOOD RESCUED "<<val*100<<" --> I1 "<<approx<<" E "<<exact<<"\n";
      bad_file.close();
    }

    if(count==max_count && (flav0==2 && flav1==-2)){
      ofstream raw_file[4];
      if(ini_raw){
	raw_file[0].open("raw_data_-44.dat");
	raw_file[1].open("raw_data_-33.dat");
	raw_file[2].open("raw_data_1-1.dat");
	raw_file[3].open("raw_data_2-2.dat");
	ini_raw=false;
      }
      else{
	raw_file[0].open("raw_data_-44.dat",ios::out | ios::app);
	raw_file[1].open("raw_data_-33.dat",ios::out | ios::app);
	raw_file[2].open("raw_data_1-1.dat",ios::out | ios::app);
	raw_file[3].open("raw_data_2-2.dat",ios::out | ios::app);
      }
      for(int idx=0;idx<4;++idx){
	raw_file[idx].setf(ios::fixed, ios::floatfield);
	raw_file[idx].setf(ios::showpoint);
	for(int i=0; i<dc;++i){
	  raw_file[idx]<<showpos<<setprecision(9)<<vec_val[idx][i]<<"\n";
	}
	vec_val[idx].clear();
	raw_file[idx].close();
      }

      ofstream raw_test[4];
      if(ini_test){
	raw_test[0].open("raw_test_-44.dat");
	raw_test[1].open("raw_test_-33.dat");
	raw_test[2].open("raw_test_1-1.dat");
	raw_test[3].open("raw_test_2-2.dat");
	ini_test=false;
      }
      else{
	raw_test[0].open("raw_test_-44.dat",ios::out | ios::app);
	raw_test[1].open("raw_test_-33.dat",ios::out | ios::app);
	raw_test[2].open("raw_test_1-1.dat",ios::out | ios::app);
	raw_test[3].open("raw_test_2-2.dat",ios::out | ios::app);
      }
      for(int idx=0;idx<4;++idx){
	raw_test[idx].setf(ios::fixed, ios::floatfield);
	raw_test[idx].setf(ios::showpoint);
	for(int i=0; i<dc;++i){
	  raw_test[idx]<<showpos<<setprecision(9)<<vec_approx[idx][i]<<" "<<vec_exact[idx][i]<<"\n";
	}
	vec_approx[idx].clear();
	vec_exact[idx].clear();

	raw_test[idx].close();
      }



      ofstream file[4];
      file[0].open("interp_-44.dat");
      file[1].open("interp_-33.dat");
      file[2].open("interp_1-1.dat");
      file[3].open("interp_2-2.dat");
      for(int idx=0;idx<4;++idx){
	file[idx]<<"# normalized by  "<<max_count<<"\n\n";
	double sum=0;
	for(int i=0;i<nbins;++i){
	  file[idx].setf(ios::fixed, ios::floatfield);
	  file[idx].setf(ios::showpoint);
	  file[idx]<<showpos<<setprecision(9)<<x[i]<<"   "<<x[i+1]<<"      ";
	  file[idx]<<noshowpos<<y[idx][i]<<"\n";
	  sum+=y[idx][i];
	}
	file[idx]<<"\n\n# ----------------------- INTEGRAL "<<sum<<"\n";
	file[idx]<<"\n\n# ---------- BAD POINTS |err|>100% "<<bad_points[idx]<<"  "<<static_cast<double>(bad_points[idx])/max_count*100<<" %\n";
	file[idx]<<"\n\n# ------------------- bad rescued: "<<bad_rescued_points[idx]<<" "<<static_cast<double>(bad_rescued_points[idx])/bad_points[idx]*100<<" %\n";
	file[idx]<<"\n\n# ------ BAD POINTS 25%<|err|<100% "<<ibad_points[idx]<<"  "<<static_cast<double>(ibad_points[idx])/max_count*100<<" %\n";
	file[idx]<<"\n\n# ------------------- bad rescued: "<<ibad_rescued_points[idx]<<" "<<static_cast<double>(ibad_rescued_points[idx])/ibad_points[idx]*100<<" %\n";
	file[idx]<<"\n\n# ------ good rescued (|err|<25%): "<<rescued_points[idx]<<" "<<static_cast<double>(rescued_points[idx])/max_count*100<<" \n";
	file[idx]<<"\n\n# --------------      TOT rescued: "<<bad_rescued_points[idx]+ibad_rescued_points[idx]+rescued_points[idx]<<" TOT bad "<<bad_points[idx]+ibad_points[idx]<<" \n";
	file[idx].close();
      }

      double old_count=max_count;
      max_count+=dc; //write every dc events
      for(int idx=0;idx<4;++idx){
	for(int i=0;i<nbins;++i){
	  // change normalization of what has been stored so far
	  y[idx][i]*=old_count;
	  y[idx][i]/=max_count;
	}
      }
    
    }

  }

  //
  // For amplitudes using VVamp codes, all results are in qT scheme.
  // For codes such as Zgam, the only just the (2) function will give 
  //   results in qT scheme (the amplitudes V1 and V2 might not be in
  //   the right scheme)
  //


  // (1) Fill all amplitude contributions and hard function terms
  extern void  matrix_2loop_loopinduced_(double & A0, double & A1,  double & H1)
  {
    calculate_H2(my_oset);
    A0=my_oset.QT_A0;
    A1=my_oset.QT_A1 / pow(my_oset.alpha_S/(2*pi),1);
    H1=my_oset.QT_H1_delta ;/// pow(my_oset.alpha_S/(2*pi),1);
  }


  // (1) Fill all amplitude contributions and hard function terms
  extern void  matrix_2loop_all_(double & A0, double & A1,  double & H1,double & H2)
  {
    calculate_H2(my_oset);
    A0=my_oset.QT_A0;
    A1=my_oset.QT_A1 / pow(my_oset.alpha_S/(2*pi),1);
    H1=my_oset.QT_H1_delta ;/// pow(my_oset.alpha_S/(2*pi),1);
    H2=my_oset.QT_H2_delta ;/// pow(my_oset.alpha_S/(2*pi),2);
  }


  // (2) Fill only born contribution and hard function terms -> used in Zgamj
  extern void  matrix_2loop_(double & A0, double & H1,double & H2)
  {

    calculate_H2(my_oset);
    A0=my_oset.QT_A0;
    H1=my_oset.QT_H1_delta ;/// pow(my_oset.alpha_S/(2*pi),1);
    H2=my_oset.QT_H2_delta ;/// pow(my_oset.alpha_S/(2*pi),2);

#ifdef PPLLLL24_test
    static int count=0,count_flav=0;
    static double approx;
    double val,exact;
    if(count==0){
      approx=H2;
      ++count;
    }
    else if(count==1){
      count_flav+=1;
      exact=H2;
      val=1-approx/exact;
      fill_hist(flav0,flav1,val,approx,exact);
      if(count_flav==4){count_flav=0;}
      count=0;
    }
#endif    

  }


  // (3) Fill only born, virtual and first order hard function
  extern void  matrix_1loop_(double & A0, double & H1)
  {
    calculate_H2(my_oset); // for loop-induced this calculates H1
    A0=my_oset.QT_A0;
    H1=my_oset.QT_H1_delta ;/// pow(my_oset.alpha_S/(2*pi),1);
  }

}
