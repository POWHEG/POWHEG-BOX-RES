#include "header.hpp"
#include "ppeexa03.subprocess.h"
void ppeexa03_determination_no_subprocess_doublereal(int & no_map, vector<int> & o_map, int & no_prc, vector<int> & o_prc, double & symmetry_factor, vector<int> & tp, int basic_order_alpha_s, int basic_order_alpha_e, int basic_order_interference){
  static Logger logger("ppeexa03_determination_no_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  no_map = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[6] = 6; out[7] = 7;}
      if (o == 1){out[6] = 7; out[7] = 6;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_map = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_map = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_map = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_map = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_map = 4; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   1)){no_map = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   3)){no_map = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   2)){no_map = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   4)){no_map = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   5)){no_map = 7; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -1)){no_map = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -3)){no_map = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -2)){no_map = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -4)){no_map = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -5)){no_map = 10; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   1)){no_map = 15; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   3)){no_map = 15; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   2)){no_map = 16; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   4)){no_map = 16; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   3)){no_map = 17; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   1)){no_map = 17; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   4)){no_map = 18; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   2)){no_map = 18; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   5)){no_map = 20; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   5)){no_map = 20; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_map = 21; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_map = 21; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_map = 22; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_map = 22; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_map = 23; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_map = 23; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_map = 24; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_map = 24; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_map = 25; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_map = 25; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_map = 26; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_map = 26; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -2)){no_map = 27; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -4)){no_map = 27; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -3)){no_map = 29; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -1)){no_map = 29; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -4)){no_map = 31; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -2)){no_map = 31; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -5)){no_map = 32; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -5)){no_map = 32; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==   2)){no_map = 34; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==   4)){no_map = 34; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){no_map = 35; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==   2)){no_map = 35; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==   5)){no_map = 36; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==   5)){no_map = 36; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){no_map = 37; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){no_map = 37; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_map = 39; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_map = 39; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_map = 40; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_map = 40; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_map = 41; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_map = 41; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_map = 42; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_map = 42; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_map = 43; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_map = 43; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_map = 44; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_map = 44; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){no_map = 45; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){no_map = 45; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -4)){no_map = 47; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -2)){no_map = 47; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){no_map = 48; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){no_map = 48; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==   5)){no_map = 50; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -1)){no_map = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -3)){no_map = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -2)){no_map = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -4)){no_map = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){no_map = 53; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){no_map = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){no_map = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){no_map = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){no_map = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){no_map = 56; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -1)){no_map = 58; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -3)){no_map = 58; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -2)){no_map = 59; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -4)){no_map = 59; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -3)){no_map = 60; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -1)){no_map = 60; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -4)){no_map = 61; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -2)){no_map = 61; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -5)){no_map = 63; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -5)){no_map = 63; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -2)){no_map = 65; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -4)){no_map = 65; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -4)){no_map = 66; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -2)){no_map = 66; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -5)){no_map = 67; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -5)){no_map = 67; break;}
      if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -5) && (tp[out[7]] ==  -5)){no_map = 69; break;}
    }
    if (no_map != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){o_map[o] = o;}
    else {o_map[o] = out[o];}
  }
  no_prc = no_map;
  o_prc = o_map;
  if (no_map == 0){}
  else if (no_map == 2){symmetry_factor = 1;}
  else if (no_map == 3){symmetry_factor = 1;}
  else if (no_map == 4){symmetry_factor = 1;}
  else if (no_map == 5){symmetry_factor = 1;}
  else if (no_map == 6){symmetry_factor = 1;}
  else if (no_map == 7){symmetry_factor = 1;}
  else if (no_map == 8){symmetry_factor = 1;}
  else if (no_map == 9){symmetry_factor = 1;}
  else if (no_map == 10){symmetry_factor = 1;}
  else if (no_map == 15){symmetry_factor = 2;}
  else if (no_map == 16){symmetry_factor = 1;}
  else if (no_map == 17){symmetry_factor = 1;}
  else if (no_map == 18){symmetry_factor = 1;}
  else if (no_map == 20){symmetry_factor = 1;}
  else if (no_map == 21){symmetry_factor = 2;}
  else if (no_map == 22){symmetry_factor = 1;}
  else if (no_map == 23){symmetry_factor = 1;}
  else if (no_map == 24){symmetry_factor = 1;}
  else if (no_map == 25){symmetry_factor = 1;}
  else if (no_map == 26){symmetry_factor = 1;}
  else if (no_map == 27){symmetry_factor = 1;}
  else if (no_map == 29){symmetry_factor = 1;}
  else if (no_map == 31){symmetry_factor = 1;}
  else if (no_map == 32){symmetry_factor = 1;}
  else if (no_map == 34){symmetry_factor = 2;}
  else if (no_map == 35){symmetry_factor = 1;}
  else if (no_map == 36){symmetry_factor = 1;}
  else if (no_map == 37){symmetry_factor = 1;}
  else if (no_map == 39){symmetry_factor = 2;}
  else if (no_map == 40){symmetry_factor = 1;}
  else if (no_map == 41){symmetry_factor = 1;}
  else if (no_map == 42){symmetry_factor = 1;}
  else if (no_map == 43){symmetry_factor = 1;}
  else if (no_map == 44){symmetry_factor = 1;}
  else if (no_map == 45){symmetry_factor = 1;}
  else if (no_map == 47){symmetry_factor = 1;}
  else if (no_map == 48){symmetry_factor = 1;}
  else if (no_map == 50){symmetry_factor = 2;}
  else if (no_map == 51){symmetry_factor = 1;}
  else if (no_map == 52){symmetry_factor = 1;}
  else if (no_map == 53){symmetry_factor = 2;}
  else if (no_map == 54){symmetry_factor = 1;}
  else if (no_map == 55){symmetry_factor = 1;}
  else if (no_map == 56){symmetry_factor = 1;}
  else if (no_map == 58){symmetry_factor = 2;}
  else if (no_map == 59){symmetry_factor = 1;}
  else if (no_map == 60){symmetry_factor = 1;}
  else if (no_map == 61){symmetry_factor = 1;}
  else if (no_map == 63){symmetry_factor = 1;}
  else if (no_map == 65){symmetry_factor = 2;}
  else if (no_map == 66){symmetry_factor = 1;}
  else if (no_map == 67){symmetry_factor = 1;}
  else if (no_map == 69){symmetry_factor = 2;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppeexa03_determination_subprocess_doublereal(int i_a, phasespace_set & psi){
  static Logger logger("ppeexa03_determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<int> tp = psi.csi->basic_type_parton[i_a];
  //  ppeexa03_determination_no_subprocess_doublereal(psi.no_map[i_a], psi.o_map[i_a], psi.no_prc[i_a], psi.o_prc[i_a], psi.symmetry_factor, tp, psi.phasespace_order_alpha_s[i_a], psi.phasespace_order_alpha_e[i_a], psi.phasespace_order_interference[i_a]);

  psi.no_map[i_a] = -1;
  vector<int> out(8);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[6] = 6; out[7] = 7;}
      if (o == 1){out[6] = 7; out[7] = 6;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 4; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   1)){psi.no_map[i_a] = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   3)){psi.no_map[i_a] = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   2)){psi.no_map[i_a] = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   4)){psi.no_map[i_a] = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   5)){psi.no_map[i_a] = 7; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 10; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   1)){psi.no_map[i_a] = 15; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   3)){psi.no_map[i_a] = 15; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   2)){psi.no_map[i_a] = 16; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   4)){psi.no_map[i_a] = 16; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   3)){psi.no_map[i_a] = 17; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   1)){psi.no_map[i_a] = 17; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   4)){psi.no_map[i_a] = 18; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   2)){psi.no_map[i_a] = 18; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==   5)){psi.no_map[i_a] = 20; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==   5)){psi.no_map[i_a] = 20; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){psi.no_map[i_a] = 21; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){psi.no_map[i_a] = 21; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 22; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 22; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 23; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 23; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 24; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 24; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 25; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 25; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 26; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 26; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 27; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 27; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 29; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 29; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 31; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 31; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 32; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 32; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==   2)){psi.no_map[i_a] = 34; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==   4)){psi.no_map[i_a] = 34; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==   4)){psi.no_map[i_a] = 35; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==   2)){psi.no_map[i_a] = 35; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==   5)){psi.no_map[i_a] = 36; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==   5)){psi.no_map[i_a] = 36; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 37; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 37; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){psi.no_map[i_a] = 39; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){psi.no_map[i_a] = 39; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 40; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 40; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 41; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 41; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 42; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 42; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 43; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 43; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 44; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 44; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 45; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 45; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 47; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 47; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 48; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 48; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==   5)){psi.no_map[i_a] = 50; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   0) && (tp[out[7]] ==   0)){psi.no_map[i_a] = 53; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==   5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 56; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 58; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 58; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 59; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 59; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -3)){psi.no_map[i_a] = 60; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -1)){psi.no_map[i_a] = 60; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 61; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 61; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -1) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 63; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -3) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 63; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 65; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 65; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -4)){psi.no_map[i_a] = 66; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -2)){psi.no_map[i_a] = 66; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -2) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 67; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -4) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 67; break;}
      if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[out[6]] ==  -5) && (tp[out[7]] ==  -5)){psi.no_map[i_a] = 69; break;}
    }
    if (psi.no_map[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){psi.o_map[i_a][o] = o;}
    else {psi.o_map[i_a][o] = out[o];}
  }
  psi.no_prc[i_a] = psi.no_map[i_a];
  psi.o_prc[i_a] = psi.o_map[i_a];
  if (psi.no_map[i_a] == 0){}
  else if (psi.no_map[i_a] == 2){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 3){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 4){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 5){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 6){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 7){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 8){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 9){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 10){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 15){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 16){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 17){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 18){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 20){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 21){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 22){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 23){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 24){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 25){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 26){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 27){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 29){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 31){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 32){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 34){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 35){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 36){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 37){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 39){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 40){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 41){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 42){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 43){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 44){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 45){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 47){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 48){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 50){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 51){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 52){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 53){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 54){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 55){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 56){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 58){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 59){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 60){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 61){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 63){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 65){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 66){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 67){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 69){psi.symmetry_factor = 2;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppeexa03_combination_subprocess_doublereal(int i_a, phasespace_set & psi, observable_set & osi){
  static Logger logger("ppeexa03_combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (psi.no_map[i_a] == 0){}
  else if (psi.no_map[i_a] == 2){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  ex a  d  dx 
    osi.combination_pdf[1] = { 1,   0,   0};   // g  g   -> e  ex a  s  sx 
  }
  else if (psi.no_map[i_a] == 3){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  ex a  u  ux 
    osi.combination_pdf[1] = { 1,   0,   0};   // g  g   -> e  ex a  c  cx 
  }
  else if (psi.no_map[i_a] == 4){
    osi.combination_pdf.resize(1);
    osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  ex a  b  bx 
  }
  else if (psi.no_map[i_a] == 5){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,   1};   // g  d   -> e  ex a  g  d  
    osi.combination_pdf[1] = { 1,   0,   3};   // g  s   -> e  ex a  g  s  
    osi.combination_pdf[2] = {-1,   0,   1};   // d  g   -> e  ex a  g  d  
    osi.combination_pdf[3] = {-1,   0,   3};   // s  g   -> e  ex a  g  s  
  }
  else if (psi.no_map[i_a] == 6){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,   2};   // g  u   -> e  ex a  g  u  
    osi.combination_pdf[1] = { 1,   0,   4};   // g  c   -> e  ex a  g  c  
    osi.combination_pdf[2] = {-1,   0,   2};   // u  g   -> e  ex a  g  u  
    osi.combination_pdf[3] = {-1,   0,   4};   // c  g   -> e  ex a  g  c  
  }
  else if (psi.no_map[i_a] == 7){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,   5};   // g  b   -> e  ex a  g  b  
    osi.combination_pdf[1] = {-1,   0,   5};   // b  g   -> e  ex a  g  b  
  }
  else if (psi.no_map[i_a] == 8){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,  -1};   // g  dx  -> e  ex a  g  dx 
    osi.combination_pdf[1] = { 1,   0,  -3};   // g  sx  -> e  ex a  g  sx 
    osi.combination_pdf[2] = {-1,   0,  -1};   // dx g   -> e  ex a  g  dx 
    osi.combination_pdf[3] = {-1,   0,  -3};   // sx g   -> e  ex a  g  sx 
  }
  else if (psi.no_map[i_a] == 9){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,  -2};   // g  ux  -> e  ex a  g  ux 
    osi.combination_pdf[1] = { 1,   0,  -4};   // g  cx  -> e  ex a  g  cx 
    osi.combination_pdf[2] = {-1,   0,  -2};   // ux g   -> e  ex a  g  ux 
    osi.combination_pdf[3] = {-1,   0,  -4};   // cx g   -> e  ex a  g  cx 
  }
  else if (psi.no_map[i_a] == 10){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,  -5};   // g  bx  -> e  ex a  g  bx 
    osi.combination_pdf[1] = {-1,   0,  -5};   // bx g   -> e  ex a  g  bx 
  }
  else if (psi.no_map[i_a] == 15){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   1,   1};   // d  d   -> e  ex a  d  d  
    osi.combination_pdf[1] = { 1,   3,   3};   // s  s   -> e  ex a  s  s  
  }
  else if (psi.no_map[i_a] == 16){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   2};   // d  u   -> e  ex a  d  u  
    osi.combination_pdf[1] = { 1,   3,   4};   // s  c   -> e  ex a  s  c  
    osi.combination_pdf[2] = {-1,   1,   2};   // u  d   -> e  ex a  d  u  
    osi.combination_pdf[3] = {-1,   3,   4};   // c  s   -> e  ex a  s  c  
  }
  else if (psi.no_map[i_a] == 17){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   1,   3};   // d  s   -> e  ex a  d  s  
    osi.combination_pdf[1] = { 1,   3,   1};   // s  d   -> e  ex a  d  s  
  }
  else if (psi.no_map[i_a] == 18){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   4};   // d  c   -> e  ex a  d  c  
    osi.combination_pdf[1] = { 1,   3,   2};   // s  u   -> e  ex a  u  s  
    osi.combination_pdf[2] = {-1,   3,   2};   // u  s   -> e  ex a  u  s  
    osi.combination_pdf[3] = {-1,   1,   4};   // c  d   -> e  ex a  d  c  
  }
  else if (psi.no_map[i_a] == 20){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   5};   // d  b   -> e  ex a  d  b  
    osi.combination_pdf[1] = { 1,   3,   5};   // s  b   -> e  ex a  s  b  
    osi.combination_pdf[2] = {-1,   1,   5};   // b  d   -> e  ex a  d  b  
    osi.combination_pdf[3] = {-1,   3,   5};   // b  s   -> e  ex a  s  b  
  }
  else if (psi.no_map[i_a] == 21){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  g  g  
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  g  g  
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  g  g  
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  g  g  
  }
  else if (psi.no_map[i_a] == 22){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  d  dx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  s  sx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  d  dx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  s  sx 
  }
  else if (psi.no_map[i_a] == 23){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  u  ux 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  c  cx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  u  ux 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  c  cx 
  }
  else if (psi.no_map[i_a] == 24){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  s  sx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  d  dx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  s  sx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  d  dx 
  }
  else if (psi.no_map[i_a] == 25){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  c  cx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  u  ux 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  c  cx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  u  ux 
  }
  else if (psi.no_map[i_a] == 26){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  b  bx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  b  bx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  b  bx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  b  bx 
  }
  else if (psi.no_map[i_a] == 27){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -2};   // d  ux  -> e  ex a  d  ux 
    osi.combination_pdf[1] = { 1,   3,  -4};   // s  cx  -> e  ex a  s  cx 
    osi.combination_pdf[2] = {-1,   1,  -2};   // ux d   -> e  ex a  d  ux 
    osi.combination_pdf[3] = {-1,   3,  -4};   // cx s   -> e  ex a  s  cx 
  }
  else if (psi.no_map[i_a] == 29){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -3};   // d  sx  -> e  ex a  d  sx 
    osi.combination_pdf[1] = { 1,   3,  -1};   // s  dx  -> e  ex a  s  dx 
    osi.combination_pdf[2] = {-1,   3,  -1};   // dx s   -> e  ex a  s  dx 
    osi.combination_pdf[3] = {-1,   1,  -3};   // sx d   -> e  ex a  d  sx 
  }
  else if (psi.no_map[i_a] == 31){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -4};   // d  cx  -> e  ex a  d  cx 
    osi.combination_pdf[1] = { 1,   3,  -2};   // s  ux  -> e  ex a  s  ux 
    osi.combination_pdf[2] = {-1,   3,  -2};   // ux s   -> e  ex a  s  ux 
    osi.combination_pdf[3] = {-1,   1,  -4};   // cx d   -> e  ex a  d  cx 
  }
  else if (psi.no_map[i_a] == 32){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -5};   // d  bx  -> e  ex a  d  bx 
    osi.combination_pdf[1] = { 1,   3,  -5};   // s  bx  -> e  ex a  s  bx 
    osi.combination_pdf[2] = {-1,   1,  -5};   // bx d   -> e  ex a  d  bx 
    osi.combination_pdf[3] = {-1,   3,  -5};   // bx s   -> e  ex a  s  bx 
  }
  else if (psi.no_map[i_a] == 34){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   2,   2};   // u  u   -> e  ex a  u  u  
    osi.combination_pdf[1] = { 1,   4,   4};   // c  c   -> e  ex a  c  c  
  }
  else if (psi.no_map[i_a] == 35){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   2,   4};   // u  c   -> e  ex a  u  c  
    osi.combination_pdf[1] = { 1,   4,   2};   // c  u   -> e  ex a  u  c  
  }
  else if (psi.no_map[i_a] == 36){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,   5};   // u  b   -> e  ex a  u  b  
    osi.combination_pdf[1] = { 1,   4,   5};   // c  b   -> e  ex a  c  b  
    osi.combination_pdf[2] = {-1,   2,   5};   // b  u   -> e  ex a  u  b  
    osi.combination_pdf[3] = {-1,   4,   5};   // b  c   -> e  ex a  c  b  
  }
  else if (psi.no_map[i_a] == 37){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -1};   // u  dx  -> e  ex a  u  dx 
    osi.combination_pdf[1] = { 1,   4,  -3};   // c  sx  -> e  ex a  c  sx 
    osi.combination_pdf[2] = {-1,   2,  -1};   // dx u   -> e  ex a  u  dx 
    osi.combination_pdf[3] = {-1,   4,  -3};   // sx c   -> e  ex a  c  sx 
  }
  else if (psi.no_map[i_a] == 39){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  g  g  
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  g  g  
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  g  g  
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  g  g  
  }
  else if (psi.no_map[i_a] == 40){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  d  dx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  s  sx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  d  dx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  s  sx 
  }
  else if (psi.no_map[i_a] == 41){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  u  ux 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  c  cx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  u  ux 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  c  cx 
  }
  else if (psi.no_map[i_a] == 42){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  s  sx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  d  dx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  s  sx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  d  dx 
  }
  else if (psi.no_map[i_a] == 43){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  c  cx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  u  ux 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  c  cx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  u  ux 
  }
  else if (psi.no_map[i_a] == 44){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  b  bx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  b  bx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  b  bx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  b  bx 
  }
  else if (psi.no_map[i_a] == 45){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -3};   // u  sx  -> e  ex a  u  sx 
    osi.combination_pdf[1] = { 1,   4,  -1};   // c  dx  -> e  ex a  c  dx 
    osi.combination_pdf[2] = {-1,   4,  -1};   // dx c   -> e  ex a  c  dx 
    osi.combination_pdf[3] = {-1,   2,  -3};   // sx u   -> e  ex a  u  sx 
  }
  else if (psi.no_map[i_a] == 47){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -4};   // u  cx  -> e  ex a  u  cx 
    osi.combination_pdf[1] = { 1,   4,  -2};   // c  ux  -> e  ex a  c  ux 
    osi.combination_pdf[2] = {-1,   4,  -2};   // ux c   -> e  ex a  c  ux 
    osi.combination_pdf[3] = {-1,   2,  -4};   // cx u   -> e  ex a  u  cx 
  }
  else if (psi.no_map[i_a] == 48){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -5};   // u  bx  -> e  ex a  u  bx 
    osi.combination_pdf[1] = { 1,   4,  -5};   // c  bx  -> e  ex a  c  bx 
    osi.combination_pdf[2] = {-1,   2,  -5};   // bx u   -> e  ex a  u  bx 
    osi.combination_pdf[3] = {-1,   4,  -5};   // bx c   -> e  ex a  c  bx 
  }
  else if (psi.no_map[i_a] == 50){
    osi.combination_pdf.resize(1);
    osi.combination_pdf[0] = { 1,   5,   5};   // b  b   -> e  ex a  b  b  
  }
  else if (psi.no_map[i_a] == 51){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -1};   // b  dx  -> e  ex a  b  dx 
    osi.combination_pdf[1] = { 1,   5,  -3};   // b  sx  -> e  ex a  b  sx 
    osi.combination_pdf[2] = {-1,   5,  -1};   // dx b   -> e  ex a  b  dx 
    osi.combination_pdf[3] = {-1,   5,  -3};   // sx b   -> e  ex a  b  sx 
  }
  else if (psi.no_map[i_a] == 52){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -2};   // b  ux  -> e  ex a  b  ux 
    osi.combination_pdf[1] = { 1,   5,  -4};   // b  cx  -> e  ex a  b  cx 
    osi.combination_pdf[2] = {-1,   5,  -2};   // ux b   -> e  ex a  b  ux 
    osi.combination_pdf[3] = {-1,   5,  -4};   // cx b   -> e  ex a  b  cx 
  }
  else if (psi.no_map[i_a] == 53){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  g  g  
    osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  ex a  g  g  
  }
  else if (psi.no_map[i_a] == 54){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  d  dx 
    osi.combination_pdf[1] = { 1,   5,  -5};   // b  bx  -> e  ex a  s  sx 
    osi.combination_pdf[2] = {-1,   5,  -5};   // bx b   -> e  ex a  d  dx 
    osi.combination_pdf[3] = {-1,   5,  -5};   // bx b   -> e  ex a  s  sx 
  }
  else if (psi.no_map[i_a] == 55){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  u  ux 
    osi.combination_pdf[1] = { 1,   5,  -5};   // b  bx  -> e  ex a  c  cx 
    osi.combination_pdf[2] = {-1,   5,  -5};   // bx b   -> e  ex a  u  ux 
    osi.combination_pdf[3] = {-1,   5,  -5};   // bx b   -> e  ex a  c  cx 
  }
  else if (psi.no_map[i_a] == 56){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  b  bx 
    osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  ex a  b  bx 
  }
  else if (psi.no_map[i_a] == 58){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -1,  -1};   // dx dx  -> e  ex a  dx dx 
    osi.combination_pdf[1] = { 1,  -3,  -3};   // sx sx  -> e  ex a  sx sx 
  }
  else if (psi.no_map[i_a] == 59){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -2};   // dx ux  -> e  ex a  dx ux 
    osi.combination_pdf[1] = { 1,  -3,  -4};   // sx cx  -> e  ex a  sx cx 
    osi.combination_pdf[2] = {-1,  -1,  -2};   // ux dx  -> e  ex a  dx ux 
    osi.combination_pdf[3] = {-1,  -3,  -4};   // cx sx  -> e  ex a  sx cx 
  }
  else if (psi.no_map[i_a] == 60){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -1,  -3};   // dx sx  -> e  ex a  dx sx 
    osi.combination_pdf[1] = { 1,  -3,  -1};   // sx dx  -> e  ex a  dx sx 
  }
  else if (psi.no_map[i_a] == 61){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -4};   // dx cx  -> e  ex a  dx cx 
    osi.combination_pdf[1] = { 1,  -3,  -2};   // sx ux  -> e  ex a  ux sx 
    osi.combination_pdf[2] = {-1,  -3,  -2};   // ux sx  -> e  ex a  ux sx 
    osi.combination_pdf[3] = {-1,  -1,  -4};   // cx dx  -> e  ex a  dx cx 
  }
  else if (psi.no_map[i_a] == 63){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -5};   // dx bx  -> e  ex a  dx bx 
    osi.combination_pdf[1] = { 1,  -3,  -5};   // sx bx  -> e  ex a  sx bx 
    osi.combination_pdf[2] = {-1,  -1,  -5};   // bx dx  -> e  ex a  dx bx 
    osi.combination_pdf[3] = {-1,  -3,  -5};   // bx sx  -> e  ex a  sx bx 
  }
  else if (psi.no_map[i_a] == 65){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -2,  -2};   // ux ux  -> e  ex a  ux ux 
    osi.combination_pdf[1] = { 1,  -4,  -4};   // cx cx  -> e  ex a  cx cx 
  }
  else if (psi.no_map[i_a] == 66){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -2,  -4};   // ux cx  -> e  ex a  ux cx 
    osi.combination_pdf[1] = { 1,  -4,  -2};   // cx ux  -> e  ex a  ux cx 
  }
  else if (psi.no_map[i_a] == 67){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -2,  -5};   // ux bx  -> e  ex a  ux bx 
    osi.combination_pdf[1] = { 1,  -4,  -5};   // cx bx  -> e  ex a  cx bx 
    osi.combination_pdf[2] = {-1,  -2,  -5};   // bx ux  -> e  ex a  ux bx 
    osi.combination_pdf[3] = {-1,  -4,  -5};   // bx cx  -> e  ex a  cx bx 
  }
  else if (psi.no_map[i_a] == 69){
    osi.combination_pdf.resize(1);
    osi.combination_pdf[0] = { 1,  -5,  -5};   // bx bx  -> e  ex a  bx bx 
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
