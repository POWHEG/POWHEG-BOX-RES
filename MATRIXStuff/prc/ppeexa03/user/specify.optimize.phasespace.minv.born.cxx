static int switch_cut_M_leplep = psi.user.switch_value[psi.user.switch_map["M_leplep"]];
static double cut_min_M_leplep = psi.user.cut_value[psi.user.cut_map["min_M_leplep"]];
static int switch_cut_M_lepgam = psi.user.switch_value[psi.user.switch_map["M_lepgam"]];
static double cut_min_M_lepgam = psi.user.cut_value[psi.user.cut_map["min_M_lepgam"]];

if (switch_cut_M_leplep){psi.sqrtsmin_opt[0][12] = cut_min_M_leplep;}
if (switch_cut_M_lepgam){psi.sqrtsmin_opt[0][20] = cut_min_M_lepgam;}
if (switch_cut_M_lepgam){psi.sqrtsmin_opt[0][24] = cut_min_M_lepgam;}

