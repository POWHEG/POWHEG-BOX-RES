#include "header.hpp"
#include "ppeexa03.subprocess.h"
void ppeexa03_determination_no_subprocess_real(int & no_map, vector<int> & o_map, int & no_prc, vector<int> & o_prc, double & symmetry_factor, vector<int> & tp, int basic_order_alpha_s, int basic_order_alpha_e, int basic_order_interference){
  static Logger logger("ppeexa03_determination_no_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  no_map = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[6] = 6;}
      if (basic_order_alpha_s == 1 && basic_order_alpha_e == 3 && basic_order_interference == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   1)){no_map = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   3)){no_map = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   2)){no_map = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   4)){no_map = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   5)){no_map = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -1)){no_map = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -3)){no_map = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -2)){no_map = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -4)){no_map = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -5)){no_map = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 13; break;}
      }
      else if (basic_order_alpha_s == 0 && basic_order_alpha_e == 4 && basic_order_interference == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   1)){no_map = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   3)){no_map = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   2)){no_map = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   4)){no_map = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   5)){no_map = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -1)){no_map = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -3)){no_map = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -2)){no_map = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -4)){no_map = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -5)){no_map = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){no_map = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){no_map = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){no_map = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){no_map = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){no_map = 23; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){no_map = 24; break;}
      }
      else if (basic_order_alpha_s == 3 && basic_order_alpha_e == 3 && basic_order_interference == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   1)){no_map = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   3)){no_map = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   2)){no_map = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   4)){no_map = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   5)){no_map = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -1)){no_map = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -3)){no_map = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -2)){no_map = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -4)){no_map = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -5)){no_map = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 14; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){no_map = 16; break;}
      }
    }
    if (no_map != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){o_map[o] = o;}
    else {o_map[o] = out[o];}
  }
  no_prc = no_map;
  o_prc = o_map;
  if (basic_order_alpha_s == 1 && basic_order_alpha_e == 3 && basic_order_interference == 0){
    if (no_map == 0){}
    else if (no_map == 2){symmetry_factor = 1;}
    else if (no_map == 3){symmetry_factor = 1;}
    else if (no_map == 4){symmetry_factor = 1;}
    else if (no_map == 5){symmetry_factor = 1;}
    else if (no_map == 6){symmetry_factor = 1;}
    else if (no_map == 7){symmetry_factor = 1;}
    else if (no_map == 9){symmetry_factor = 1;}
    else if (no_map == 11){symmetry_factor = 1;}
    else if (no_map == 13){symmetry_factor = 1;}
  }
  else if (basic_order_alpha_s == 0 && basic_order_alpha_e == 4 && basic_order_interference == 0){
    if (no_map == 0){}
    else if (no_map == 10){symmetry_factor = 1;}
    else if (no_map == 12){symmetry_factor = 1;}
    else if (no_map == 14){symmetry_factor = 1;}
    else if (no_map == 15){symmetry_factor = 1;}
    else if (no_map == 16){symmetry_factor = 1;}
    else if (no_map == 17){symmetry_factor = 1;}
    else if (no_map == 21){symmetry_factor = 2;}
    else if (no_map == 22){symmetry_factor = 2;}
    else if (no_map == 23){symmetry_factor = 2;}
    else if (no_map == 24){symmetry_factor = 2;}
  }
  else if (basic_order_alpha_s == 3 && basic_order_alpha_e == 3 && basic_order_interference == 0){
    if (no_map == 0){}
    else if (no_map == 1){symmetry_factor = 1;}
    else if (no_map == 6){symmetry_factor = 1;}
    else if (no_map == 7){symmetry_factor = 1;}
    else if (no_map == 8){symmetry_factor = 1;}
    else if (no_map == 9){symmetry_factor = 1;}
    else if (no_map == 10){symmetry_factor = 1;}
    else if (no_map == 11){symmetry_factor = 1;}
    else if (no_map == 12){symmetry_factor = 1;}
    else if (no_map == 14){symmetry_factor = 1;}
    else if (no_map == 16){symmetry_factor = 1;}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppeexa03_determination_subprocess_real(int i_a, phasespace_set & psi){
  static Logger logger("ppeexa03_determination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<int> tp = psi.csi->basic_type_parton[i_a];
  //  ppeexa03_determination_no_subprocess_real(psi.no_map[i_a], psi.o_map[i_a], psi.no_prc[i_a], psi.o_prc[i_a], psi.symmetry_factor, tp, psi.phasespace_order_alpha_s[i_a], psi.phasespace_order_alpha_e[i_a], psi.phasespace_order_interference[i_a]);

  psi.no_map[i_a] = -1;
  vector<int> out(7);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){out[6] = 6;}
      if (psi.phasespace_order_alpha_s[i_a] == 1 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   1)){psi.no_map[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   3)){psi.no_map[i_a] = 2; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   2)){psi.no_map[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   4)){psi.no_map[i_a] = 3; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   5)){psi.no_map[i_a] = 4; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -1)){psi.no_map[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -3)){psi.no_map[i_a] = 5; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -2)){psi.no_map[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -4)){psi.no_map[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -5)){psi.no_map[i_a] = 7; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 9; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 9; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 11; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 11; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 13; break;}
      }
      else if (psi.phasespace_order_alpha_s[i_a] == 0 && psi.phasespace_order_alpha_e[i_a] == 4 && psi.phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   1)){psi.no_map[i_a] = 10; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   3)){psi.no_map[i_a] = 10; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   2)){psi.no_map[i_a] = 12; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   4)){psi.no_map[i_a] = 12; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   5)){psi.no_map[i_a] = 14; break;}
        if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -1)){psi.no_map[i_a] = 15; break;}
        if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -3)){psi.no_map[i_a] = 15; break;}
        if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -2)){psi.no_map[i_a] = 16; break;}
        if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -4)){psi.no_map[i_a] = 16; break;}
        if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -5)){psi.no_map[i_a] = 17; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){psi.no_map[i_a] = 21; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){psi.no_map[i_a] = 21; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){psi.no_map[i_a] = 22; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){psi.no_map[i_a] = 22; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){psi.no_map[i_a] = 23; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  22)){psi.no_map[i_a] = 24; break;}
      }
      else if (psi.phasespace_order_alpha_s[i_a] == 3 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 1; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   1)){psi.no_map[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   3)){psi.no_map[i_a] = 6; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   2)){psi.no_map[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   4)){psi.no_map[i_a] = 7; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   5)){psi.no_map[i_a] = 8; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -1)){psi.no_map[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -3)){psi.no_map[i_a] = 9; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -2)){psi.no_map[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -4)){psi.no_map[i_a] = 10; break;}
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==  -5)){psi.no_map[i_a] = 11; break;}
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 12; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 12; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 14; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 14; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22) && (tp[6] ==   0)){psi.no_map[i_a] = 16; break;}
      }
    }
    if (psi.no_map[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){psi.o_map[i_a][o] = o;}
    else {psi.o_map[i_a][o] = out[o];}
  }
  psi.no_prc[i_a] = psi.no_map[i_a];
  psi.o_prc[i_a] = psi.o_map[i_a];
  if (psi.phasespace_order_alpha_s[i_a] == 1 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 2){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 3){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 4){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 5){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 6){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 7){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 9){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 11){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 13){psi.symmetry_factor = 1;}
  }
  else if (psi.phasespace_order_alpha_s[i_a] == 0 && psi.phasespace_order_alpha_e[i_a] == 4 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 10){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 12){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 14){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 15){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 16){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 17){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 21){psi.symmetry_factor = 2;}
    else if (psi.no_map[i_a] == 22){psi.symmetry_factor = 2;}
    else if (psi.no_map[i_a] == 23){psi.symmetry_factor = 2;}
    else if (psi.no_map[i_a] == 24){psi.symmetry_factor = 2;}
  }
  else if (psi.phasespace_order_alpha_s[i_a] == 3 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 1){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 6){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 7){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 8){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 9){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 10){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 11){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 12){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 14){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 16){psi.symmetry_factor = 1;}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppeexa03_combination_subprocess_real(int i_a, phasespace_set & psi, observable_set & osi){
  static Logger logger("ppeexa03_combination_subprocess_real");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (psi.phasespace_order_alpha_s[i_a] == 1 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 2){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,   1};   // g  d   -> e  ex a  d  
      osi.combination_pdf[1] = { 1,   0,   3};   // g  s   -> e  ex a  s  
      osi.combination_pdf[2] = {-1,   0,   1};   // d  g   -> e  ex a  d  
      osi.combination_pdf[3] = {-1,   0,   3};   // s  g   -> e  ex a  s  
    }
    else if (psi.no_map[i_a] == 3){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,   2};   // g  u   -> e  ex a  u  
      osi.combination_pdf[1] = { 1,   0,   4};   // g  c   -> e  ex a  c  
      osi.combination_pdf[2] = {-1,   0,   2};   // u  g   -> e  ex a  u  
      osi.combination_pdf[3] = {-1,   0,   4};   // c  g   -> e  ex a  c  
    }
    else if (psi.no_map[i_a] == 4){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   0,   5};   // g  b   -> e  ex a  b  
      osi.combination_pdf[1] = {-1,   0,   5};   // b  g   -> e  ex a  b  
    }
    else if (psi.no_map[i_a] == 5){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,  -1};   // g  dx  -> e  ex a  dx 
      osi.combination_pdf[1] = { 1,   0,  -3};   // g  sx  -> e  ex a  sx 
      osi.combination_pdf[2] = {-1,   0,  -1};   // dx g   -> e  ex a  dx 
      osi.combination_pdf[3] = {-1,   0,  -3};   // sx g   -> e  ex a  sx 
    }
    else if (psi.no_map[i_a] == 6){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,  -2};   // g  ux  -> e  ex a  ux 
      osi.combination_pdf[1] = { 1,   0,  -4};   // g  cx  -> e  ex a  cx 
      osi.combination_pdf[2] = {-1,   0,  -2};   // ux g   -> e  ex a  ux 
      osi.combination_pdf[3] = {-1,   0,  -4};   // cx g   -> e  ex a  cx 
    }
    else if (psi.no_map[i_a] == 7){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   0,  -5};   // g  bx  -> e  ex a  bx 
      osi.combination_pdf[1] = {-1,   0,  -5};   // bx g   -> e  ex a  bx 
    }
    else if (psi.no_map[i_a] == 9){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  g  
      osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  g  
      osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  g  
      osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  g  
    }
    else if (psi.no_map[i_a] == 11){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  g  
      osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  g  
      osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  g  
      osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  g  
    }
    else if (psi.no_map[i_a] == 13){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  g  
      osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  ex a  g  
    }
  }
  else if (psi.phasespace_order_alpha_s[i_a] == 0 && psi.phasespace_order_alpha_e[i_a] == 4 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 10){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   1,   7};   // d  a   -> e  ex a  d  
      osi.combination_pdf[1] = { 1,   3,   7};   // s  a   -> e  ex a  s  
      osi.combination_pdf[2] = {-1,   1,   7};   // a  d   -> e  ex a  d  
      osi.combination_pdf[3] = {-1,   3,   7};   // a  s   -> e  ex a  s  
    }
    else if (psi.no_map[i_a] == 12){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   2,   7};   // u  a   -> e  ex a  u  
      osi.combination_pdf[1] = { 1,   4,   7};   // c  a   -> e  ex a  c  
      osi.combination_pdf[2] = {-1,   2,   7};   // a  u   -> e  ex a  u  
      osi.combination_pdf[3] = {-1,   4,   7};   // a  c   -> e  ex a  c  
    }
    else if (psi.no_map[i_a] == 14){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   5,   7};   // b  a   -> e  ex a  b  
      osi.combination_pdf[1] = {-1,   5,   7};   // a  b   -> e  ex a  b  
    }
    else if (psi.no_map[i_a] == 15){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,  -1,   7};   // dx a   -> e  ex a  dx 
      osi.combination_pdf[1] = { 1,  -3,   7};   // sx a   -> e  ex a  sx 
      osi.combination_pdf[2] = {-1,  -1,   7};   // a  dx  -> e  ex a  dx 
      osi.combination_pdf[3] = {-1,  -3,   7};   // a  sx  -> e  ex a  sx 
    }
    else if (psi.no_map[i_a] == 16){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,  -2,   7};   // ux a   -> e  ex a  ux 
      osi.combination_pdf[1] = { 1,  -4,   7};   // cx a   -> e  ex a  cx 
      osi.combination_pdf[2] = {-1,  -2,   7};   // a  ux  -> e  ex a  ux 
      osi.combination_pdf[3] = {-1,  -4,   7};   // a  cx  -> e  ex a  cx 
    }
    else if (psi.no_map[i_a] == 17){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,  -5,   7};   // bx a   -> e  ex a  bx 
      osi.combination_pdf[1] = {-1,  -5,   7};   // a  bx  -> e  ex a  bx 
    }
    else if (psi.no_map[i_a] == 21){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  a  
      osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  a  
      osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  a  
      osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  a  
    }
    else if (psi.no_map[i_a] == 22){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  a  
      osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  a  
      osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  a  
      osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  a  
    }
    else if (psi.no_map[i_a] == 23){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  a  
      osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  ex a  a  
    }
    else if (psi.no_map[i_a] == 24){
      osi.combination_pdf.resize(1);
      osi.combination_pdf[0] = { 1,   7,   7};   // a  a   -> e  ex a  a  
    }
  }
  else if (psi.phasespace_order_alpha_s[i_a] == 3 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 1){
      osi.combination_pdf.resize(1);
      osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  ex a  g  
    }
    else if (psi.no_map[i_a] == 6){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,   1};   // g  d   -> e  ex a  d  
      osi.combination_pdf[1] = { 1,   0,   3};   // g  s   -> e  ex a  s  
      osi.combination_pdf[2] = {-1,   0,   1};   // d  g   -> e  ex a  d  
      osi.combination_pdf[3] = {-1,   0,   3};   // s  g   -> e  ex a  s  
    }
    else if (psi.no_map[i_a] == 7){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,   2};   // g  u   -> e  ex a  u  
      osi.combination_pdf[1] = { 1,   0,   4};   // g  c   -> e  ex a  c  
      osi.combination_pdf[2] = {-1,   0,   2};   // u  g   -> e  ex a  u  
      osi.combination_pdf[3] = {-1,   0,   4};   // c  g   -> e  ex a  c  
    }
    else if (psi.no_map[i_a] == 8){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   0,   5};   // g  b   -> e  ex a  b  
      osi.combination_pdf[1] = {-1,   0,   5};   // b  g   -> e  ex a  b  
    }
    else if (psi.no_map[i_a] == 9){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,  -1};   // g  dx  -> e  ex a  dx 
      osi.combination_pdf[1] = { 1,   0,  -3};   // g  sx  -> e  ex a  sx 
      osi.combination_pdf[2] = {-1,   0,  -1};   // dx g   -> e  ex a  dx 
      osi.combination_pdf[3] = {-1,   0,  -3};   // sx g   -> e  ex a  sx 
    }
    else if (psi.no_map[i_a] == 10){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   0,  -2};   // g  ux  -> e  ex a  ux 
      osi.combination_pdf[1] = { 1,   0,  -4};   // g  cx  -> e  ex a  cx 
      osi.combination_pdf[2] = {-1,   0,  -2};   // ux g   -> e  ex a  ux 
      osi.combination_pdf[3] = {-1,   0,  -4};   // cx g   -> e  ex a  cx 
    }
    else if (psi.no_map[i_a] == 11){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   0,  -5};   // g  bx  -> e  ex a  bx 
      osi.combination_pdf[1] = {-1,   0,  -5};   // bx g   -> e  ex a  bx 
    }
    else if (psi.no_map[i_a] == 12){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  g  
      osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  g  
      osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  g  
      osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  g  
    }
    else if (psi.no_map[i_a] == 14){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  g  
      osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  g  
      osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  g  
      osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  g  
    }
    else if (psi.no_map[i_a] == 16){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  g  
      osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  ex a  g  
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
