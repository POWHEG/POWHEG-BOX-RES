#include <classes.cxx>
#include <definitions.observable.set.cxx>
#include <header/ppllll24.amplitude.doublevirtual.h>


#include <chrono>
#include <fstream>

extern int H2_mode;
extern bool use_mcfm;

void Btwxt_fit(double ma2,double mb2,double s, double t, vector<vector<vector<double> > > &E_ip1);
void find_grid_point(double ma2,double mb2,double s,double t,double (&y)[4]);

extern "C"
{
  extern struct{
    double helamp_bornr[2][2][2],helamp_1loopr[2][2][2],helamp_borni[2][2][2],helamp_1loopi[2][2][2];
  } twoloop_contributions_;
}


typedef enum {
    V_PHOT = 0,
    V_ZLL,
    V_WMIN,
    V_WPLU,
    V_ZNN
} TypeV;

void ppllll24_calculate_H2(observable_set & oset){
  static Logger logger("ppllll24_calculate_H2");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  logger << LOG_DEBUG_VERBOSE << "NEW VT2 implementation called" << endl;
  
  ppllll24_calculate_amplitude_doublevirtual(oset);

  osi_QT_H1_delta = osi_QT_A1;
  osi_QT_H2_delta = osi_QT_A2;

  osi_QT_H1_delta /= 2;
  osi_QT_H2_delta /= 4;

  osi_QT_H1_delta /= (osi_alpha_S * inv2pi * osi_QT_A0);
  osi_QT_H2_delta /= (pow(osi_alpha_S * inv2pi, 2) * osi_QT_A0);   

  logger << LOG_DEBUG << "osi_QT_H1_delta = " << osi_QT_H1_delta << ", osi_QT_H2_delta = " << osi_QT_H2_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

double_complex ppllll24_computeM(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<double_complex> &E) {
  static Logger logger("ppllll24_computeM");
  i5-=2;
  i6-=2;
  i7-=2;
  i8-=2;

  double_complex result=0.0;
  result += E[0]*za[i1][i5]*za[i1][i7]*zb[i1][i6]*zb[i1][i8];
  result += E[1]*za[i1][i5]*za[i2][i7]*zb[i1][i6]*zb[i2][i8];
  result += E[2]*za[i2][i5]*za[i1][i7]*zb[i2][i6]*zb[i1][i8];
  result += E[3]*za[i2][i5]*za[i2][i7]*zb[i2][i6]*zb[i2][i8];
  result += E[4]*za[i5][i7]*zb[i6][i8];
  if (hel==-1) {
    result *= (zb[i1][i5]*za[i5][i2]+zb[i1][i6]*za[i6][i2]);
  } else if (hel==+1) {
    result *= (zb[i2][i5]*za[i5][i1]+zb[i2][i6]*za[i6][i1]);
  }

  if (hel==-1) {
    result += E[5]*za[i1][i5]*za[i2][i7]*zb[i1][i6]*zb[i1][i8];
    result += E[6]*za[i2][i5]*za[i2][i7]*zb[i2][i6]*zb[i1][i8];
    result += E[7]*za[i2][i5]*za[i1][i7]*zb[i1][i6]*zb[i1][i8];
    result += E[8]*za[i2][i5]*za[i2][i7]*zb[i1][i6]*zb[i2][i8];
  } else if (hel==+1) {
    result += E[5]*za[i1][i5]*za[i1][i7]*zb[i1][i6]*zb[i2][i8];
    result += E[6]*za[i2][i5]*za[i1][i7]*zb[i2][i6]*zb[i2][i8];
    result += E[7]*za[i1][i5]*za[i1][i7]*zb[i2][i6]*zb[i1][i8];
    result += E[8]*za[i1][i5]*za[i2][i7]*zb[i2][i6]*zb[i2][i8];
  }

  logger << LOG_DEBUG << "result=" << result << endl;
  return result;
}



double_complex ppllll24_computeM_FSR_7(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const double_complex F) {
  /* computes the "FSR" type diagram, i.e. qq' -> ll -> llll, where the intermediate gauge boson couples to i7. The diagram where it couples to i5 can be obtained by (5,6)<->(7,8),
     the diagram where it couples to i8 by 7<->8 plus helicity flip in 78
  // note that the crossing is different compared to the "resonant" contribution. swapping helicities in the initial state is always trivial (i.e. a permutation), in the final state it is non-trivial in 78 and trivial in 56 */
  i5-=2;
  i6-=2;
  i7-=2;
  i8-=2;

  double_complex result=0.0;

  if (hel==-1) { // LLL
    result -= za[i5][i7]*zb[i8][i1]*(zb[i5][i6]*za[i2][i5]+zb[i7][i6]*za[i2][i7]);
  } else if (hel==+1) { // LLR
    result -= za[i2][i8]*zb[i7][i6]*(za[i5][i6]*zb[i6][i1]+za[i5][i7]*zb[i7][i1]);
  }

  result *= (4.0*F/(za[i5][i6]*zb[i6][i5]+za[i5][i7]*zb[i7][i5]+za[i6][i7]*zb[i7][i6]));

  return result;
}

void ppllll24_computeE(double s, double t, double ma2, double mb2, int type, vector<double_complex> &E) {
  // tree level form factors (for debugging purposes)
  double u = ma2+mb2-s-t;

  if (type==0) {
    E[0] = 0;
    E[1] = -2.0/s/t;
    E[2] = 2.0/s/t;
    E[3] = 0;
    E[4] = 2.0/t;
    E[5] = -2.0*(s-t+mb2)/s/t;
    E[6] = 2.0*(t-ma2)/s/t;
    E[7] = -2.0*(t-mb2)/s/t;
    E[8] = 2.0*(s-t+ma2)/s/t;
  } else if (type==1) {
    E[0] = 0;
    E[1] = -2.0/s/u;
    E[2] = 2.0/s/u;
    E[3] = 0;
    E[4] = -2.0/u;
    E[5] = -2.0*(u-ma2)/s/u;
    E[6] = 2.0*(s-u+mb2)/s/u;
    E[7] = -2.0*(s-u+ma2)/s/u;
    E[8] = 2.0*(u-mb2)/s/u;
  } else if (type==2) {
    E[0] = 0;
    E[1] = 0;
    E[2] = 0;
    E[3] = 0;
    E[4] = 0;
    E[5] = 0;
    E[6] = 0;
    E[7] = 0;
    E[8] = 0;
  } else if (type==3) {
    E[0] = 0;
    E[1] = 0;
    E[2] = 0;
    E[3] = 0;
    E[4] = -4;
    E[5] = 4;
    E[6] = 4;
    E[7] = -4;
    E[8] = -4;
  }
}

void ppllll24_computeVVprimeHelAmplitudes(observable_set & oset, TypeV V1, TypeV V2, int q1, vector<fourvector> &p, const vector<vector<vector<double_complex> > > &E, vector<vector<vector<vector<double_complex> > > > &M_dressed) {
  // 0: photon
  // 1: Z
  // 2: W^-
  // 3: W^+
  
  //  int order=2;
  

  double I3_q;
  double I3_qp;
  double e_q;
  double e_qp;
  double total_q;

  if (q1==2 || q1==4) { // u, c
    I3_q=0.5;
    e_q=2.0/3;
    /*
    I3_q = osi_msi.Iw_u;
    I3_qp = -osi_msi.Iw_u;
    e_q = osi_msi.Q_u;
    e_qp = -osi_msi.Q_u;
    */
  } else if (q1==1 || q1==3 || q1==5) { // d, s, b
    I3_q=-0.5;
    e_q=-1.0/3;
    /*
    I3_q = osi_msi.Iw_d;
    I3_qp = -osi_msi.Iw_d;
    e_q = osi_msi.Q_d;
    e_qp = -osi_msi.Q_d;
    */
  }
  if ( ((V1==0 || V1==1) && (V2==0 || V2==1)) || (V1==2 && V2==3) || (V1==3 && V2==2) || (V1==1 && V2==4) || (V1==4 && V2==1) || (V1==0 && V2==4) || (V1==4 && V2==0) ) {
    //electrically neutral final state
    e_qp = -e_q; // FIXIT: this should not be the anti-quark, but the quark charge. However, it does not contribute below
    I3_qp = -I3_q;
    total_q = 0;
  } else if ( ((V1==0 || V1==1 || V1 ==4) && V2==2) || (V1==2 && (V2==0 || V2==1 || V2 ==4)) ) {
    // negatively charged final state
    e_qp = -(-1-e_q);
    I3_qp = -I3_q;
    total_q = -1;
  } else if ( ((V1==0 || V1==1 || V1 ==4) && V2==3) || (V1==3 && (V2==0 || V2==1 || V2 ==4)) ) {
    // positively charged final state
    e_qp = -(1-e_q);
    I3_qp = -I3_q;
    total_q = 1;
  } else {
    assert(false);
  }

  double_complex Q_lep[4];
  double_complex I_lep[4];
  if (V1==0 || V1==1) {
    Q_lep[0]=-1;
    I_lep[0]=-0.5;
    Q_lep[1]=-1;
    I_lep[1]=-0.5;
  } else if (V1==2) {
    Q_lep[0]=0;
    I_lep[0]=0.5;
    Q_lep[1]=-1;
    I_lep[1]=-0.5;
  } else if (V1==3) {
    Q_lep[0]=-1;
    I_lep[0]=-0.5;
    Q_lep[1]=0;
    I_lep[1]=0.5;
  } else if (V1==4) {
    Q_lep[0]=0;
    I_lep[0]=0.5;
    Q_lep[1]=0;
    I_lep[1]=0.5;
  }
  if (V2==0 || V2==1) {
    Q_lep[2]=-1;
    I_lep[2]=-0.5;
    Q_lep[3]=-1;
    I_lep[3]=-0.5;
  } else if (V2==2) {
    Q_lep[2]=0;
    I_lep[2]=0.5;
    Q_lep[3]=-1;
    I_lep[3]=-0.5;
  } else if (V2==3) {
    Q_lep[2]=-1;
    I_lep[2]=-0.5;
    Q_lep[3]=0;
    I_lep[3]=0.5;
  } else if (V2==4) {
    Q_lep[2]=0;
    I_lep[2]=0.5;
    Q_lep[3]=0;
    I_lep[3]=0.5;
  }


  double s = (p[1]+p[2]).m2();
  //  double t = (p[1]-(p[3]+p[4])).m2();
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();

  vector<vector<double_complex > > za, zb;
  vector<vector <double> > s_inv;

  calcSpinorProducts(p,za,zb,s_inv);

  double M_Z = osi_msi.M_Z;
  double Gamma_Z = osi_msi.Gamma_Z;
  double M_W = osi_msi.M_W;
  double Gamma_W = osi_msi.Gamma_W;

  static int count=1;

  double Nf=osi_N_f;
  // propagators for the decay
  double_complex DV1,DV2;

  if (V1==0) {
    DV1 = ma2;
  } else if (V1==1 || V1 == 4) {
    DV1 = ma2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z;
  } else if (V1 == 2 || V1 == 3) {
    DV1 = ma2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W;
  }
  if (V2==0) {
    DV2 = mb2;
  } else if (V2==1 || V2 == 4) {
    DV2 = mb2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z;
  } else if (V2 == 2 || V2 == 3) {
    DV2 = mb2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W;
  }

  // propagator denominators for the F_V contributions
  double_complex DZ=s-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z;
  double_complex DP=s;
  double_complex DW=s-M_W*M_W+double_complex(0,1)*Gamma_W*M_W;

  double_complex cos_W = osi_msi.ccos_w;
  double_complex sin_W = osi_msi.csin_w;

  double_complex LR56[2],LR78[2];

  if (V1==0) { //photon
    LR56[0] = -1.0;
    LR56[1] = -1.0;

    LR56[0] = Q_lep[0];
    LR56[1] = Q_lep[0];
    /*
    LR56[0] = osi_msi.Q_l;
    LR56[1] = osi_msi.Q_l;
    */
  } else if (V1==1 || V1==4) { // Z // TODO: depends on the decay!
    // LR56[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR56[1] = sin_W/cos_W;
    LR56[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W;
    LR56[1] = -Q_lep[0]*sin_W/cos_W;
    /*
    LR56[0] = osi_msi.cCminus_Zee;
    LR56[1] = osi_msi.cCplus_Zee;
    // Z -> nn~ decay !!!
    LR56[0] = osi_msi.cCminus_Znn;
    LR56[1] = osi_msi.cCplus_Znn;
    */
  } else if (V1 == 2 || V1 == 3) { // W
    LR56[0] = 1.0/sqrt(2)/sin_W;
    LR56[1] = 0.0;
    /*
    LR56[0] = osi_msi.cCminus_W;
    LR56[1] = 0.;
    */
  }
  if (V2==0) { //photon
    LR78[0] = -1.0;
    LR78[1] = -1.0;
    /*
    LR78[0] = osi_msi.Q_l;
    LR78[1] = osi_msi.Q_l;
    */
  } else if (V2==1 || V2==4) { // Z
    LR78[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W;
    LR78[1] = -Q_lep[2]*sin_W/cos_W;
    // LR78[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR78[1] = sin_W/cos_W;
    /*
    LR78[0] = osi_msi.cCminus_Zee;
    LR78[1] = osi_msi.cCplus_Zee;
    // Z -> nn~ decay !!!
    LR78[0] = osi_msi.cCminus_Znn;
    LR78[1] = osi_msi.cCplus_Znn;
    */
  } else if (V2 == 2 || V2 == 3) { // W
    LR78[0] = 1.0/sqrt(2)/sin_W;
    LR78[1] = 0.0;
    /*
    LR78[0] = osi_msi.cCminus_W;
    LR78[1] = 0.;
    */
  }

  double_complex c_P12=0;
  double_complex c_Z12=0;
  double_complex c_W12=0;
  double_complex L_P=0.0;
  double_complex L_Z=0.0;
  double_complex L_W=0.0;
  double_complex R_P=0.0;
  double_complex R_Z=0.0;
  double_complex R_W=0.0;
  
  // quark boson couplings
  if (total_q != 0) { // udb or dub
    L_W=1.0/sqrt(2)/sin_W;
    R_W=0.0;
    /*
    L_W = osi_msi.cCminus_W;
    R_W = 0.;
    */
  } else { // uub or ddb
    L_P=e_q;
    R_P=e_q;
    L_Z=(I3_q-e_q*sin_W*sin_W)/sin_W/cos_W;
    R_Z=-e_q*sin_W/cos_W;
  }
  
  if ((V1==2 || V1==3) && (V2==2 || V2==3)) { // WW
    c_P12=1;
    c_Z12=cos_W/sin_W;
    /*
    c_P12 = osi_msi.cC_AWminusWplus; // opposite sign compared to SD notation (all incoming) !!!
    c_Z12 = osi_msi.cC_ZWminusWplus; // same sign compared to SD notation (all incoming) !!!
    */
  } else if ((V1==0 && V2==2) || (V1==2 && V2==0)) { // gamma W
    c_W12=1;
  } else if ((V1==0 && V2==3) || (V1==3 && V2==0)) { // gamma W
    c_W12=-1;
  } else if ((V1==2 && (V2==1 || V2==4)) || ((V1==1 || V1==4) && V2==2)) { // WZ
    c_W12=cos_W/sin_W;
  } else if ((V1==3 && (V2==1 || V2==4)) || ((V1==1 || V1==4) && V2==3)) { // WZ
    c_W12=-cos_W/sin_W;
  }

  double_complex LAq1,LAq2,RAq1,RAq2;
  double_complex LBq1,LBq2,RBq1,RBq2;
  if (V1==0) {
      LAq1 = e_q;
      RAq1 = e_q;
    if (V2==0 || V2==1 || V2==4) {
      LBq1 = e_q;
      RBq1 = e_q;
    } else if (V2==2 || V2==3) {
      LBq1 = e_qp;
      RBq1 = e_qp;
    }
  } else if (V1==1 || V1==4) {
    LAq1 = (I3_q-e_q*sin_W*sin_W)/sin_W/cos_W;
    RAq1 = -e_q*sin_W/cos_W;
    if (V2==0 || V2==1 || V2==4) {
      LBq1 = (I3_q-e_q*sin_W*sin_W)/sin_W/cos_W;
      RBq1 = -e_q*sin_W/cos_W;
    } else if (V2==2 || V2==3) {
      LBq1 = (I3_qp-e_qp*sin_W*sin_W)/sin_W/cos_W;
      RBq1 = -e_qp*sin_W/cos_W;
    }
  } else if ((V1==2 && e_q>0) || (V1==3 && e_q<0)) {
    LAq1 = 0;
    RAq1 = 0;
    LBq1 = 1.0/sqrt(2)/sin_W;
    RBq1 = 0;
  } else if ((V1==3 && e_q>0) || (V1==2 && e_q<0)) {
    LAq1 = 1.0/sqrt(2)/sin_W;
    RAq1 = 0;
    LBq1 = 0;
    RBq1 = 0;
  }
  if (V2==0) {
    LAq2 = e_q;
    RAq2 = e_q;
    LBq2 = e_q;
    RBq2 = e_q;
  } else if (V2==1 || V2==4) {
    LAq2 = (I3_q-e_q*sin_W*sin_W)/sin_W/cos_W;
    RAq2 = -e_q*sin_W/cos_W;
    LBq2 = (I3_q-e_q*sin_W*sin_W)/sin_W/cos_W;
    RBq2 = -e_q*sin_W/cos_W;
  } else if (V2==2 && (V1==0 || V1==1 || V1==4)) {
    LAq2 = 1.0/sqrt(2)/sin_W;
    RAq2 = 0;
    LBq2 = 1.0/sqrt(2)/sin_W;
    RBq2 = 0;
  } else if (V2==3 && (V1==0 || V1==1 || V1==4)) {
    LAq2 = 1.0/sqrt(2)/sin_W;
    RAq2 = 0;
    LBq2 = 1.0/sqrt(2)/sin_W;
    RBq2 = 0;
  } else if ((V2==2 && e_q>0) || (V2==3 && e_q<0)) {
    LAq2 = 1.0/sqrt(2)/sin_W;
    RAq2 = 0;
    LBq2 = 0;
    RBq2 = 0;
  } else if ((V2==3 && e_q>0) || (V2==2 && e_q<0)) {
    LAq2 = 0;
    RAq2 = 0;
    LBq2 = 1.0/sqrt(2)/sin_W;
    RBq2 = 0;
  } else {
    assert(false);
  }

  double_complex N_VV=0.0;
  double_complex LZu=(0.5-2.0/3*sin_W*sin_W)/sin_W/cos_W;
  //  static double_complex LZu = osi_msi.cCminus_Zuu;
  double_complex LZd=(-0.5+1.0/3*sin_W*sin_W)/sin_W/cos_W;
  //  static double_complex LZd = osi_msi.cCminus_Zdd;
  double_complex RZu=-2.0/3*sin_W/cos_W;
  //  static double_complex RZu = osi_msi.cCminus_Zuu;
  double_complex RZd=1.0/3*sin_W/cos_W;
  //  static double_complex RZd = osi_msi.cCminus_Zdd;
  double_complex LW=1.0/sqrt(2)/sin_W;
  //  static double_complex LW = osi_msi.cCminus_W
  if (V1==0 && V2==0) {
    N_VV = 2*(2.0/3)*(2.0/3)+(Nf-2)*(1.0/3)*(1.0/3);
  } else if (((V1==1 || V1==4) && V2==0) || (V1==0 && (V2==1 || V2==4))) {
    // note: sign mistake in 1503.04812, consistent with 1112.1531
    N_VV = 0.5*(2.0*(LZu+RZu)*2.0/3.0 + (Nf-2.0)*(LZd+RZd)*(-1.0/3.0));
  } else if ((V1==1 || V1==4) && (V2==1 || V2==4)) {
    N_VV = 0.5*(2.0*(LZu*LZu+RZu*RZu)+(Nf-2.0)*(LZd*LZd+RZd*RZd));
  } else if ((V1==2 || V1==3) && (V2==2 || V2==3)) {
    int Ng = Nf/2; // number of massless quark generations
    N_VV = 0.5*Ng*LW*LW;
  }
  
  /*
  int N_fu = Nf / 2;
  int N_fd = (Nf + 1) / 2;
  if (V1==0 && V2==0) {
    N_VV = N_fu * pow(osi_msi.Q_u, 2) + N_fd * pow(osi_msi.Q_d, 2);
  } 
  else if ((V1==1 && V2==0) || (V1==0 && V2==1)) {
    // note: sign mistake in 1503.04812, consistent with 1112.1531
    N_VV = 0.5 * (N_fu * (LZu + RZu) * osi_msi.Q_u + N_fd * ( LZd + RZd) * osi_msi.Q_d);
  }
  else if (V1==1 && V1==1) {
    N_VV = 0.5 * (N_fu * (pow(LZu, 2) + pow(RZu, 2)) + N_fd * (pow(LZd, 2) + pow(RZd, 2)));
  }
  else if (V1>=2 && V2>=2) {
    int Ng = Nf/2; // number of massless quark generations
    N_VV = 0.5*Ng*LW*LW;
  }
  */
  

  // couplings
  double_complex Q_A[2],Q_B[2],Q_C[2],Q_FP[2],Q_FZ[2],Q_FW[2];
  Q_A[0] = LAq1*LAq2;
  Q_A[1] = RAq1*RAq2;

  Q_B[0] = LBq1*LBq2;
  Q_B[1] = RBq1*RBq2;

  Q_C[0] = N_VV;
  Q_C[1] = N_VV;

  Q_FP[0] = L_P*c_P12/DP;
  Q_FP[1] = R_P*c_P12/DP;

  Q_FZ[0] = L_Z*c_Z12/DZ;
  Q_FZ[1] = R_Z*c_Z12/DZ;

  Q_FW[0] = L_W*c_W12/DW;
  Q_FW[1] = R_W*c_W12/DW;

  double_complex M_undressed[2][2][2][3][4];

  int tch;
  static int count_tch=0;
  ++count_tch;
  if(count_tch<=6){tch=0;}
  else{tch=1;}
 
  
  for (int loop=0; loop<3; loop++) {
    for (int type=0; type<4; type++) {
      M_undressed[0][0][0][loop][type] = ppllll24_computeM(1,2,5,6,7,8,-1,za,zb,E[loop][type]);
      M_undressed[0][0][1][loop][type] = ppllll24_computeM(1,2,5,6,8,7,-1,za,zb,E[loop][type]);
      M_undressed[0][1][0][loop][type] = ppllll24_computeM(1,2,6,5,7,8,-1,za,zb,E[loop][type]);
      M_undressed[0][1][1][loop][type] = ppllll24_computeM(1,2,6,5,8,7,-1,za,zb,E[loop][type]);

      M_undressed[1][0][0][loop][type] = ppllll24_computeM(1,2,5,6,7,8,+1,za,zb,E[loop][type]);
      M_undressed[1][0][1][loop][type] = ppllll24_computeM(1,2,5,6,8,7,+1,za,zb,E[loop][type]);
      M_undressed[1][1][0][loop][type] = ppllll24_computeM(1,2,6,5,7,8,+1,za,zb,E[loop][type]);
      M_undressed[1][1][1][loop][type] = ppllll24_computeM(1,2,6,5,8,7,+1,za,zb,E[loop][type]);
    }

    if(count_tch==12){count_tch=0;}
    
  }

  for (int loop=0; loop<3; loop++) {
    for (int h1=0; h1<2; h1++) {
      for (int h2=0; h2<2; h2++) {
        for (int h3=0; h3<2; h3++) {
          M_dressed[h1][h2][h3][loop] = Q_A[h1]*M_undressed[h1][h2][h3][loop][0];
          M_dressed[h1][h2][h3][loop] += Q_B[h1]*M_undressed[h1][h2][h3][loop][1];
          M_dressed[h1][h2][h3][loop] += Q_C[h1]*M_undressed[h1][h2][h3][loop][2];
          M_dressed[h1][h2][h3][loop] += (Q_FP[h1]+Q_FZ[h1]+Q_FW[h1])*M_undressed[h1][h2][h3][loop][3];
          M_dressed[h1][h2][h3][loop] *= (LR56[h2]*LR78[h3]/DV1/DV2);
          
          M_dressed[h1][h2][h3][loop] *= double_complex(0,1);
        }
      }
    }
  }
}

// computes the helicity amplitudes for the single resonant process qq' -> V1 V2; resonant_V specifies which boson can become resonant
void ppllll24_computeVVprimeHelAmplitudesSR(observable_set & oset, int resonant_V, TypeV V1, TypeV V2, int q1, vector<fourvector> &p, vector<double_complex> &F_q, vector<vector<vector<vector<double_complex> > > > &M_dressed) {
  //  int order=2;
  
  double I3_q;
  double I3_qp;
  double e_q;
  double e_qp;
  
   if (q1==2 || q1==4) { // u, c
    I3_q=0.5;
    I3_qp=-0.5;
    e_q=2.0/3;
    e_qp=-2.0/3;
    /*
    I3_q = osi_msi.Iw_u;
    I3_qp = -osi_msi.Iw_u;
    e_q = osi_msi.Q_u;
    e_qp = -osi_msi.Q_u;
    */
  } else if (q1==1 || q1==3 || q1==5) { // d, s, b
    I3_q=-0.5;
    I3_qp=0.5;
    e_q=-1.0/3;
    e_qp=1.0/3;
    /*
    I3_q = osi_msi.Iw_d;
    I3_qp = -osi_msi.Iw_d;
    e_q = osi_msi.Q_d;
    e_qp = -osi_msi.Q_d;
    */
  }
  if ( ((V1==0 || V1==1) && (V2==0 || V2==1)) || (V1==2 && V2==3) || (V1==3 && V2==2) || (V1==1 && V2==4) || (V1==4 && V2==1) || (V1==0 && V2==4) || (V1==4 && V2==0) ) {
    //electrically neutral final state
    e_qp = -e_q;
    I3_qp = -I3_q;
  } else if ( ((V1==0 || V1==1 || V1 ==4) && V2==2) || (V1==2 && (V2==0 || V2==1 || V2 ==4)) ) {
    // negatively charged final state
    e_qp = -1-e_q;
    I3_qp = I3_q;
  } else if ( ((V1==0 || V1==1 || V1 ==4) && V2==3) || (V1==3 && (V2==0 || V2==1 || V2 ==4)) ) {
    // positively charged final state
    e_qp = 1-e_q;
    I3_qp = I3_q;
  } else {
    assert(false);
  }





  double s = (p[1]+p[2]).m2();
  //  double t = (p[1]-(p[3]+p[4])).m2();
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();

  vector<vector<double_complex > > za, zb;
  vector<vector <double> > s_inv;

  calcSpinorProducts(p,za,zb,s_inv);

  double M_Z = osi_msi.M_Z;
  double Gamma_Z = osi_msi.Gamma_Z;
  double M_W = osi_msi.M_W;
  double Gamma_W = osi_msi.Gamma_W;

  //  double Nf=osi_N_f;
  static int count=1;


  double_complex Q_lep[4];
  double_complex I_lep[4];
  if (V1==0 || V1==1) {
    Q_lep[0]=-1;
    I_lep[0]=-0.5;
    Q_lep[1]=-1;
    I_lep[1]=-0.5;
  } else if (V1==2) { // FIXIT: something is switched with neutrinos and leptons in W's
    Q_lep[1]=0;
    I_lep[1]=0.5;
    Q_lep[0]=-1;
    I_lep[0]=-0.5;
  } else if (V1==3) {
    Q_lep[1]=-1;
    I_lep[1]=-0.5;
    Q_lep[0]=0;
    I_lep[0]=0.5;
  } else if (V1==4) {
    Q_lep[0]=0;
    I_lep[0]=0.5;
    Q_lep[1]=0;
    I_lep[1]=0.5;
  }
  if (V2==0 || V2==1) {
    Q_lep[2]=-1;
    I_lep[2]=-0.5;
    Q_lep[3]=-1;
    I_lep[3]=-0.5;
  } else if (V2==2) { // FIXIT: something is switched with neutrinos and leptons in W's
    Q_lep[3]=0;
    I_lep[3]=0.5;
    Q_lep[2]=-1;
    I_lep[2]=-0.5;
  } else if (V2==3) {
    Q_lep[3]=-1;
    I_lep[3]=-0.5;
    Q_lep[2]=0;
    I_lep[2]=0.5;
  } else if (V2==4) {
    Q_lep[2]=0;
    I_lep[2]=0.5;
    Q_lep[3]=0;
    I_lep[3]=0.5;
  }


  // propagators for the decay
  double_complex DV1,DV2;

  if (V1==0) {
    DV1 = ma2;
  } else if (V1==1 || V1 == 4) {
    DV1 = ma2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z;
  } else if (V1 == 2 || V1 == 3) {
    DV1 = ma2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W;
  }
  if (V2==0) {
    DV2 = mb2;
  } else if (V2==1 || V2 == 4) {
    DV2 = mb2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z;
  } else if (V2 == 2 || V2 == 3) {
    DV2 = mb2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W;
  }


  // propagator denominators for the F_V contributions
  double_complex DZ=s-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z;
  double_complex DP=s;
  double_complex DW=s-M_W*M_W+double_complex(0,1)*Gamma_W*M_W;

  double_complex cos_W = osi_msi.ccos_w;
  double_complex sin_W = osi_msi.csin_w;

  double_complex LR56[2],LR78[2];
  
  // couplings of the on-shell VB to the lepton pairs
  double_complex LR5_2[2],LR7_1[2];
  double_complex LR6_2[2],LR8_1[2];

  if (V1==0) { //photon
    LR56[0] = Q_lep[0];
    LR56[1] = Q_lep[0];

    LR7_1[0] = Q_lep[2]; // FIXIT: something is switched with neutrinos and leptons in W's
    LR7_1[1] = Q_lep[2];
    LR8_1[0] = Q_lep[3];
    LR8_1[1] = Q_lep[3];

    // LR56[0] = -1.0;
    // LR56[1] = -1.0;
    
    // if (V2==0 || V2==1) {
    //   LR7_1[0] = -1.0;
    //   LR7_1[1] = -1.0;
    //   LR8_1[0] = -1.0;
    //   LR8_1[1] = -1.0;
    // } else if (V2==4) { // nunugamma->nunull
    //   LR7_1[0] = 0.0;
    //   LR7_1[1] = 0.0;
    //   LR8_1[0] = 0.0;
    //   LR8_1[1] = 0.0;
    // } else if (V2==2) {
    //   LR7_1[0] = -1.0;
    //   LR7_1[1] = -1.0;
    //   LR8_1[0] = 0.0;
    //   LR8_1[1] = 0.0;
    // } else if (V2==3) {
    //   LR7_1[0] = 0.0;
    //   LR7_1[1] = 0.0;
    //   LR8_1[0] = -1.0;
    //   LR8_1[1] = -1.0;
    // }
    /*
    LR56[0] = osi_msi.Q_l;
    LR56[1] = osi_msi.Q_l;
    LR78_1[0] = osi_msi.Q_l;
    LR78_1[1] = osi_msi.Q_l;
    */
  } else if (V1==1 || V1==4) { // Z // TODO: depends on the decay!
    // LR56[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR56[1] = sin_W/cos_W;
    LR56[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W;
    LR56[1] = -Q_lep[0]*sin_W/cos_W;
    LR7_1[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W; // FIXIT: something is switched with neutrinos and leptons in W's
    LR7_1[1] = -Q_lep[2]*sin_W/cos_W;
    LR8_1[0] = (I_lep[3]-Q_lep[3]*sin_W*sin_W)/sin_W/cos_W;
    LR8_1[1] = -Q_lep[3]*sin_W/cos_W;
    // if (V2==0 || V2==1 || V2==4) {
    //   LR7_1[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W;
    //   LR7_1[1] = -Q_lep[2]*sin_W/cos_W;
    //   LR8_1[0] = (I_lep[3]-Q_lep[3]*sin_W*sin_W)/sin_W/cos_W;
    //   LR8_1[1] = -Q_lep[3]*sin_W/cos_W;
    // } else if (V2==2) {
    //   LR7_1[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    //   LR7_1[1] = sin_W/cos_W;
    //   LR8_1[0] = (+0.5)/sin_W/cos_W;
    //   LR8_1[1] = 0.0;
    // } else if (V2==3) {
    //   LR7_1[0] = (+0.5)/sin_W/cos_W;
    //   LR7_1[1] = 0.0;
    //   LR8_1[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    //   LR8_1[1] = sin_W/cos_W;
    // }
    /*
    LR56[0] = osi_msi.cCminus_Zee;
    LR56[1] = osi_msi.cCplus_Zee;
    LR78_1[0] = osi_msi.cCminus_Zee;
    LR78_1[1] = osi_msi.cCplus_Zee;
    // Z -> nn~ decay !!!
    LR56[0] = osi_msi.cCminus_Znn;
    LR56[1] = osi_msi.cCplus_Znn;
    LR78_1[0] = osi_msi.cCminus_Znn;
    LR78_1[1] = osi_msi.cCplus_Znn;
    */
  } else if (V1==2 || V1==3) { // W
    LR56[0] = 1.0/sqrt(2)/sin_W;
    LR56[1] = 0.0;
    LR7_1[0] = 1.0/sqrt(2)/sin_W;
    LR7_1[1] = 0.0;
    LR8_1[0] = 1.0/sqrt(2)/sin_W;
    LR8_1[1] = 0.0;
    /*
    LR56[0] = osi_msi.cCminus_W;
    LR56[1] = 0.;
    LR78_1[0] = osi_msi.cCminus_W;
    LR78_1[1] = 0.;
    */
  }
  if (V2==0) { //photon
    LR78[0] = -1.0;
    LR78[1] = -1.0;
    if (V1==0 || V1==1) {
      LR5_2[0] = -1.0;
      LR5_2[1] = -1.0;
      LR6_2[0] = -1.0;
      LR6_2[1] = -1.0;
    } else if (V1==4) { // nunugamma->nunull
      LR5_2[0] = 0.0;
      LR5_2[1] = 0.0;
      LR6_2[0] = 0.0;
      LR6_2[1] = 0.0;
    } else if (V1==2) {
      assert(false);
    } else if (V1==3) {
      assert(false);
    }
    /*
    LR78[0] = osi_msi.Q_l;
    LR78[1] = osi_msi.Q_l;
    LR56_2[0] = osi_msi.Q_l;
    LR56_2[1] = osi_msi.Q_l;
    */
  } else if (V2==1 || V2==4) { // Z
    LR78[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W;
    LR78[1] = -Q_lep[2]*sin_W/cos_W;
    LR5_2[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W;
    LR5_2[1] = -Q_lep[0]*sin_W/cos_W;
    LR6_2[0] = (I_lep[1]-Q_lep[1]*sin_W*sin_W)/sin_W/cos_W;
    LR6_2[1] = -Q_lep[1]*sin_W/cos_W;
    /*
    LR78[0] = osi_msi.cCminus_Zee;
    LR78[1] = osi_msi.cCplus_Zee;
    LR56_2[0] = osi_msi.cCminus_Zee;
    LR56_2[1] = osi_msi.cCplus_Zee;
    // Z -> nn~ decay !!!
    LR78[0] = osi_msi.cCminus_Znn;
    LR78[1] = osi_msi.cCplus_Znn;
    LR56_2[0] = osi_msi.cCminus_Znn;
    LR56_2[1] = osi_msi.cCplus_Znn;
    */
  } else if (V2==2 || V2==3) { // W
    LR78[0] = 1.0/sqrt(2)/sin_W;
    LR78[1] = 0.0;
    if (V1==2 || V1==3) {
      // WW
      LR5_2[0] = 1.0/sqrt(2)/sin_W;
      LR5_2[1] = 0.0;
      LR6_2[0] = 1.0/sqrt(2)/sin_W;
      LR6_2[1] = 0.0;
    }
    else if (V1==0 || V1==1){
      if (V2==2) {
      // Z/gamma W^- --> ll W^-
      // DY-like boson is a W^-
      // the resonant W^- can only couple to the iatnineutrino, not the lepton
	LR6_2[0] = 1.0/sqrt(2)/sin_W;
	LR6_2[1] = 0.0;
	LR5_2[0] = 0.0;
	LR5_2[1] = 0.0;
      }
      else if (V2==3) {
      // Z/gamma W^+ --> ll W^+
      // DY-like boson is a W^+
      // the resonant W^+ can only couple to the neutrino, not the antilepton
	LR5_2[0] = 1.0/sqrt(2)/sin_W;
	LR5_2[1] = 0.0;
	LR6_2[0] = 0.0;
	LR6_2[1] = 0.0;
      }
    }
    else if (V1==4){
      if (V2==2) {
      // Z W^- ->nunu W^-
      // DY-like boson is a W^-
      // the resonant W^- can only couple to the iatnineutrino, not the lepton
	LR6_2[0] = 0.0;
	LR6_2[1] = 0.0;
	LR5_2[0] = 1.0/sqrt(2)/sin_W;
	LR5_2[1] = 0.0;
      }
      else if (V2==3) {
      // Z W^+ ->nunu W^+
      // DY-like boson is a W^+
      // the resonant W^+ can only couple to the neutrino, not the antilepton
	LR5_2[0] = 0.0;
	LR5_2[1] = 0.0;
	LR6_2[0] = 1.0/sqrt(2)/sin_W;
	LR6_2[1] = 0.0;
      }
    }
    else {
      assert(false);
    }
  }



  double_complex L_P=0.0;
  double_complex L_Z=0.0;
  double_complex L_W=0.0;
  double_complex R_P=0.0;
  double_complex R_Z=0.0;
  double_complex R_W=0.0;
  
  // quark boson couplings
  if (e_q*e_qp>0) { // udb or dub
    L_W=1.0/sqrt(2)/sin_W;
    R_W=0.0;
    /*
    L_W = osi_msi.cCminus_W;
    R_W = 0.;
    */
  } else { // uub or ddb
    L_P=e_q;
    R_P=e_q;
    L_Z=(I3_q-e_q*sin_W*sin_W)/sin_W/cos_W;
    R_Z=-e_q*sin_W/cos_W;
  }
  
  double_complex Q_FP_FSR[2],Q_FZ_FSR[2],Q_FW_FSR[2];
  Q_FP_FSR[0] = L_P/DP;
  Q_FP_FSR[1] = R_P/DP;

  Q_FZ_FSR[0] = L_Z/DZ;
  Q_FZ_FSR[1] = R_Z/DZ;

  Q_FW_FSR[0] = L_W/DW;
  Q_FW_FSR[1] = R_W/DW;

  // couplings of the DY like vector bosons to the non-resonant lepton pair
  double_complex LR_P5[2],LR_Z5[2],LR_W5[2];
  double_complex LR_P6[2],LR_Z6[2],LR_W6[2];
  double_complex LR_P7[2],LR_Z7[2],LR_W7[2];
  double_complex LR_P8[2],LR_Z8[2],LR_W8[2];
  // for the second VB becoming resonant
  if (V1==0 || V1==1 || V1==4) { // LR_W6 is probably LR_W5 and so on...   SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES
    LR_P5[0] = Q_lep[1]; // !!!!! 5<->6 is switched !!!!!! so we switched 1<->0 !!!! :-) !!!!    SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES
    LR_P5[1] = Q_lep[1];
    LR_P6[0] = Q_lep[0];
    LR_P6[1] = Q_lep[0];
    LR_Z5[0] = (I_lep[1]-Q_lep[1]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z5[1] = -Q_lep[1]*sin_W/cos_W;
    LR_Z6[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z6[1] = -Q_lep[0]*sin_W/cos_W;
    if (V2==2) {
      LR_W5[0] = 1.0/sqrt(2)/sin_W; // should be zero! But no problem, as contribution already forced zero above
      LR_W5[1] = 0.0;
      LR_W6[0] = 1.0/sqrt(2)/sin_W;
      LR_W6[1] = 0.0;
    } else if (V2==3) {
      LR_W5[0] = 1.0/sqrt(2)/sin_W;
      LR_W5[1] = 0.0;
      LR_W6[0] = 1.0/sqrt(2)/sin_W; // should be zero! But no problem, as contribution already forced zero above
      LR_W6[1] = 0.0;
    } else {
      LR_W5[0] = 0.0;
      LR_W5[1] = 0.0;
      LR_W6[0] = 0.0;
      LR_W6[1] = 0.0;
    }
    /*
    LR_P5[0] = osi_msi.Q_l;
    LR_P5[1] = osi_msi.Q_l;
    LR_P6[0] = osi_msi.Q_l;
    LR_P6[1] = osi_msi.Q_l;
    LR_Z5[0] = osi_msi.cCminus_Zee;
    LR_Z5[1] = osi_msi.cCplus_Zee;
    LR_Z6[0] = osi_msi.cCminus_Zee;
    LR_Z6[1] = osi_msi.cCplus_Zee;
    LR_W5[0] = 0.0;
    LR_W5[1] = 0.0;
    LR_W6[0] = 0.0;
    LR_W6[1] = 0.0;
    // Z -> nn~ decay !!!
    LR_P5[0] = osi_msi.Q_n;
    LR_P5[1] = osi_msi.Q_n;
    LR_P6[0] = osi_msi.Q_n;
    LR_P6[1] = osi_msi.Q_n;
    LR_Z5[0] = osi_msi.cCminus_Znn;
    LR_Z5[1] = osi_msi.cCplus_Znn;
    LR_Z6[0] = osi_msi.cCminus_Znn;
    LR_Z6[1] = osi_msi.cCplus_Znn;
    LR_W5[0] = 0.0;
    LR_W5[1] = 0.0;
    LR_W6[0] = 0.0;
    LR_W6[1] = 0.0;
    */
  } else if (V1==2) { // V1=W^- ==> 5=l^-, 6=nubar;  gamma/Z -> l^-nu W^+ -> l^-nu l^+nu  or  W^- -> l^-nu gamma/Z -> l^-nu l^-l^+
    // LR_P6[0] = -1.0; // NOTE 6<->5 switched; SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES
    // LR_P6[1] = -1.0;
    // LR_P5[0] = 0.0;  // Photon does not couple to nu ==> LR_P=0 for 6=nubar ==> LR_P5 = 0
    // LR_P5[1] = 0.0;
    // LR_Z6[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR_Z6[1] = sin_W/cos_W;
    // LR_Z5[0] = (+0.5)/sin_W/cos_W;
    // LR_Z5[1] = 0.0;
    // LR_W5[0] = 1.0/sqrt(2)/sin_W;
    // LR_W5[1] = 0.0;
    // LR_W6[0] = 1.0/sqrt(2)/sin_W;
    // LR_W6[1] = 0.0;

    LR_P5[0] = Q_lep[1]; // NOTE 6<->5 switched; SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES; thus we switche 1<->0 as well
    LR_P5[1] = Q_lep[1];
    LR_P6[0] = Q_lep[0];
    LR_P6[1] = Q_lep[0];
    LR_Z5[0] = (I_lep[1]-Q_lep[1]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z5[1] = -Q_lep[1]*sin_W/cos_W;
    LR_Z6[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z6[1] = -Q_lep[0]*sin_W/cos_W;
    LR_W5[0] = 1.0/sqrt(2)/sin_W;
    LR_W5[1] = 0.0;
    LR_W6[0] = 1.0/sqrt(2)/sin_W;
    LR_W6[1] = 0.0;
    

    /*
    LR_P5[0] = osi_msi.Q_l;
    LR_P5[1] = osi_msi.Q_l;
    LR_P6[0] = osi_msi.Q_n;
    LR_P6[1] = osi_msi.Q_n;
    LR_Z5[0] = osi_msi.cCminus_Zee;
    LR_Z5[1] = osi_msi.cCplus_Zee;
    LR_Z6[0] = osi_msi.cCminus_Znn;
    LR_Z6[1] = osi_msi.cCplus_Znn;
    LR_W5[0] = osi_msi.cCminus_W;
    LR_W5[1] = 0.0; // osi_msi.cCplus_W;
    LR_W6[0] = osi_msi.cCminus_W;
    LR_W6[1] = 0.0; // osi_msi.cCplus_W;
    */
  } else {
    assert(false);
  }
  if (V2==0 || V2==1 || V2==4) {
    // LR_P8[0] = -1.0; // !!!!! 7<->8 is switched !!!!!! so we switched 2<->3 !!!! :-) !!!!    SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES
    // LR_P8[1] = -1.0;
    // LR_P7[0] = -1.0;
    // LR_P7[1] = -1.0;
    // LR_Z8[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR_Z8[1] = sin_W/cos_W;
    // LR_Z7[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR_Z7[1] = sin_W/cos_W;

    // if (V1==2) {
    //   // FIXME: Z->nunu!
    //   LR_W8[0] = 1.0/sqrt(2)/sin_W;
    //   LR_W8[1] = 0.0;
    //   LR_W7[0] = 1.0/sqrt(2)/sin_W;
    //   LR_W7[1] = 0.0;
    // } else if (V2==3) {
    //   LR_W8[0] = 1.0/sqrt(2)/sin_W;
    //   LR_W8[1] = 0.0;
    //   LR_W7[0] = 0.0;
    //   LR_W7[1] = 0.0;
    // } else {
    //   LR_W8[0] = 0.0;
    //   LR_W8[1] = 0.0;
    //   LR_W7[0] = 0.0;
    //   LR_W7[1] = 0.0;
    // }


    LR_P7[0] = Q_lep[3]; // !!!!! 7<->8 is switched !!!!!! so we switched 2<->3 !!!! :-) !!!!    SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES
    LR_P7[1] = Q_lep[3];
    LR_P8[0] = Q_lep[2];
    LR_P8[1] = Q_lep[2];
    LR_Z7[0] = (I_lep[3]-Q_lep[3]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z7[1] = -Q_lep[3]*sin_W/cos_W;
    LR_Z8[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z8[1] = -Q_lep[2]*sin_W/cos_W;
    if (V2==3) {
      LR_W7[0] = 1.0/sqrt(2)/sin_W;
      LR_W7[1] = 0.0;
      LR_W8[0] = 1.0/sqrt(2)/sin_W; // should be zero! But no problem, as contribution already forced zero above
      LR_W8[1] = 0.0;
    } else if (V2==2) {
      LR_W7[0] = 0.0;
      LR_W7[1] = 0.0;
      LR_W8[0] = 1.0/sqrt(2)/sin_W;
      LR_W8[1] = 0.0;
    } else {
      LR_W7[0] = 0.0;
      LR_W7[1] = 0.0;
      LR_W8[0] = 0.0;
      LR_W8[1] = 0.0;
    }

    /*
    LR_P8[0] = osi_msi.Q_l;
    LR_P8[1] = osi_msi.Q_l;
    LR_P7[0] = osi_msi.Q_l;
    LR_P7[1] = osi_msi.Q_l;
    LR_Z8[0] = osi_msi.cCminus_Zee;
    LR_Z8[1] = osi_msi.cCplus_Zee;
    LR_Z7[0] = osi_msi.cCminus_Zee;
    LR_Z7[1] = osi_msi.cCplus_Zee;
    LR_W8[0] = 0.0;
    LR_W8[1] = 0.0;
    LR_W7[0] = 0.0;
    LR_W7[1] = 0.0;
    // Z -> nn~ decay !!!
    LR_P8[0] = osi_msi.Q_n;
    LR_P8[1] = osi_msi.Q_n;
    LR_P7[0] = osi_msi.Q_n;
    LR_P7[1] = osi_msi.Q_n;
    LR_Z8[0] = osi_msi.cCminus_Znn;
    LR_Z8[1] = osi_msi.cCplus_Znn;
    LR_Z7[0] = osi_msi.cCminus_Znn;
    LR_Z7[1] = osi_msi.cCplus_Znn;
    LR_W8[0] = 0.0;
    LR_W8[1] = 0.0;
    LR_W7[0] = 0.0;
    LR_W7[1] = 0.0;
    */
  } else if (V2==2 || V2==3) {
    // relevant for W^+W^-
    // LR_P8[0] = -1.0;
    // LR_P8[1] = -1.0;
    // LR_P7[0] = 0.0;
    // LR_P7[1] = 0.0;
    // LR_Z8[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR_Z8[1] = sin_W/cos_W;
    // LR_Z7[0] = (+0.5)/sin_W/cos_W;
    // LR_Z7[1] = 0.0;
    // LR_W8[0] = 1.0/sqrt(2)/sin_W;
    // LR_W8[1] = 0.0;
    // LR_W7[0] = 1.0/sqrt(2)/sin_W;
    // LR_W7[1] = 0.0;
    //  } else if (V2==3) {
    // relevant for W^-W^+
    // LR_P7[0] = -1.0;
    // LR_P7[1] = -1.0;
    // LR_P8[0] = 0.0;
    // LR_P8[1] = 0.0;
    // LR_Z7[0] = (-0.5+sin_W*sin_W)/sin_W/cos_W;
    // LR_Z7[1] = sin_W/cos_W;
    // LR_Z8[0] = (+0.5)/sin_W/cos_W;
    // LR_Z8[1] = 0.0;
    // // relevant for ZW
    // LR_W8[0] = 1.0/sqrt(2)/sin_W;
    // LR_W8[1] = 0.0;
    // LR_W7[0] = 1.0/sqrt(2)/sin_W;
    // LR_W7[1] = 0.0;
    // }

    LR_P7[0] = Q_lep[3]; // NOTE 7<->8 switched; SOLUTION: THE IDEA IS THAT THE NUMBER CORRESPONDS TO WHERE THE NON-DY BOSON COUPLES; thus we switche 3<->2 as well
    LR_P7[1] = Q_lep[3];
    LR_P8[0] = Q_lep[2];
    LR_P8[1] = Q_lep[2];
    LR_Z7[0] = (I_lep[3]-Q_lep[3]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z7[1] = -Q_lep[3]*sin_W/cos_W;
    LR_Z8[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W;
    LR_Z8[1] = -Q_lep[2]*sin_W/cos_W;
    // relevant for ZW
    LR_W7[0] = 1.0/sqrt(2)/sin_W;
    LR_W7[1] = 0.0;
    LR_W8[0] = 1.0/sqrt(2)/sin_W;
    LR_W8[1] = 0.0;
    
    /*
    LR_P8[0] = osi_msi.Q_l;
    LR_P8[1] = osi_msi.Q_l;
    LR_P7[0] = osi_msi.Q_n;
    LR_P7[1] = osi_msi.Q_n;
    LR_Z8[0] = osi_msi.cCminus_Zee;
    LR_Z8[1] = osi_msi.cCplus_Zee;
    LR_Z7[0] = osi_msi.cCminus_Znn;
    LR_Z7[1] = osi_msi.cCplus_Znn;
    LR_W8[0] = osi_msi.cCminus_W;
    LR_W8[1] = 0.0; // osi_msi.cCplus_W;
    LR_W7[0] = osi_msi.cCminus_W;
    LR_W7[1] = 0.0; // osi_msi.cCplus_W;
    */
  } else {
    assert(false);
  }

  double_complex M_FSR_undressed[2][2][2][3][4];

  for (int loop=0; loop<3; loop++) {
    double_complex F=F_q[loop];

    // intermediate gauge boson couples to 8
    M_FSR_undressed[0][0][0][loop][1] = ppllll24_computeM_FSR_7(1,2,5,6,7,8,-1,za,zb,F);
    M_FSR_undressed[0][1][0][loop][1] = ppllll24_computeM_FSR_7(1,2,6,5,7,8,-1,za,zb,F);
    M_FSR_undressed[1][0][0][loop][1] = ppllll24_computeM_FSR_7(2,1,5,6,7,8,-1,za,zb,F);
    M_FSR_undressed[1][1][0][loop][1] = ppllll24_computeM_FSR_7(2,1,6,5,7,8,-1,za,zb,F);

    M_FSR_undressed[0][0][1][loop][1] = ppllll24_computeM_FSR_7(1,2,5,6,7,8,+1,za,zb,F);
    M_FSR_undressed[0][1][1][loop][1] = ppllll24_computeM_FSR_7(1,2,6,5,7,8,+1,za,zb,F);
    M_FSR_undressed[1][0][1][loop][1] = ppllll24_computeM_FSR_7(2,1,5,6,7,8,+1,za,zb,F);
    M_FSR_undressed[1][1][1][loop][1] = ppllll24_computeM_FSR_7(2,1,6,5,7,8,+1,za,zb,F);

    // intermediate gauge boson couples to 7
    // sign from crossing of fermion lines
    M_FSR_undressed[0][0][0][loop][0] = -ppllll24_computeM_FSR_7(1,2,5,6,8,7,+1,za,zb,F);
    M_FSR_undressed[0][1][0][loop][0] = -ppllll24_computeM_FSR_7(1,2,6,5,8,7,+1,za,zb,F);
    M_FSR_undressed[1][0][0][loop][0] = -ppllll24_computeM_FSR_7(2,1,5,6,8,7,+1,za,zb,F);
    M_FSR_undressed[1][1][0][loop][0] = -ppllll24_computeM_FSR_7(2,1,6,5,8,7,+1,za,zb,F);

    M_FSR_undressed[0][0][1][loop][0] = -ppllll24_computeM_FSR_7(1,2,5,6,8,7,-1,za,zb,F);
    M_FSR_undressed[0][1][1][loop][0] = -ppllll24_computeM_FSR_7(1,2,6,5,8,7,-1,za,zb,F);
    M_FSR_undressed[1][0][1][loop][0] = -ppllll24_computeM_FSR_7(2,1,5,6,8,7,-1,za,zb,F);
    M_FSR_undressed[1][1][1][loop][0] = -ppllll24_computeM_FSR_7(2,1,6,5,8,7,-1,za,zb,F);

    // intermediate gauge boson couples to 6
    M_FSR_undressed[0][0][0][loop][3] = ppllll24_computeM_FSR_7(1,2,7,8,5,6,-1,za,zb,F);
    M_FSR_undressed[0][0][1][loop][3] = ppllll24_computeM_FSR_7(1,2,8,7,5,6,-1,za,zb,F);
    M_FSR_undressed[1][0][0][loop][3] = ppllll24_computeM_FSR_7(2,1,7,8,5,6,-1,za,zb,F);
    M_FSR_undressed[1][0][1][loop][3] = ppllll24_computeM_FSR_7(2,1,8,7,5,6,-1,za,zb,F);

    M_FSR_undressed[0][1][0][loop][3] = ppllll24_computeM_FSR_7(1,2,7,8,5,6,+1,za,zb,F);
    M_FSR_undressed[0][1][1][loop][3] = ppllll24_computeM_FSR_7(1,2,8,7,5,6,+1,za,zb,F);
    M_FSR_undressed[1][1][0][loop][3] = ppllll24_computeM_FSR_7(2,1,7,8,5,6,+1,za,zb,F);
    M_FSR_undressed[1][1][1][loop][3] = ppllll24_computeM_FSR_7(2,1,8,7,5,6,+1,za,zb,F);

    // intermediate gauge boson couples to 5
    // sign from crossing of fermion lines
    M_FSR_undressed[0][0][0][loop][2] = -ppllll24_computeM_FSR_7(1,2,7,8,6,5,+1,za,zb,F);
    M_FSR_undressed[0][0][1][loop][2] = -ppllll24_computeM_FSR_7(1,2,8,7,6,5,+1,za,zb,F);
    M_FSR_undressed[1][0][0][loop][2] = -ppllll24_computeM_FSR_7(2,1,7,8,6,5,+1,za,zb,F);
    M_FSR_undressed[1][0][1][loop][2] = -ppllll24_computeM_FSR_7(2,1,8,7,6,5,+1,za,zb,F);

    M_FSR_undressed[0][1][0][loop][2] = -ppllll24_computeM_FSR_7(1,2,7,8,6,5,-1,za,zb,F);
    M_FSR_undressed[0][1][1][loop][2] = -ppllll24_computeM_FSR_7(1,2,8,7,6,5,-1,za,zb,F);
    M_FSR_undressed[1][1][0][loop][2] = -ppllll24_computeM_FSR_7(2,1,7,8,6,5,-1,za,zb,F);
    M_FSR_undressed[1][1][1][loop][2] = -ppllll24_computeM_FSR_7(2,1,8,7,6,5,-1,za,zb,F);
  }


//   double_complex M_dressed[2][2][2][3];
  for (int loop=0; loop<3; loop++) {
    for (int h1=0; h1<2; h1++) {
      for (int h2=0; h2<2; h2++) {
        for (int h3=0; h3<2; h3++) {
          if (resonant_V==1) {
            M_dressed[h1][h2][h3][loop]  = LR56[h2]*LR8_1[h3]/DV1*(Q_FP_FSR[h1]*LR_P8[h3]+Q_FZ_FSR[h1]*LR_Z8[h3]+Q_FW_FSR[h1]*LR_W8[h3])*M_FSR_undressed[h1][h2][h3][loop][0];
            M_dressed[h1][h2][h3][loop] += LR56[h2]*LR7_1[h3]/DV1*(Q_FP_FSR[h1]*LR_P7[h3]+Q_FZ_FSR[h1]*LR_Z7[h3]+Q_FW_FSR[h1]*LR_W7[h3])*M_FSR_undressed[h1][h2][h3][loop][1];
          } else {
            M_dressed[h1][h2][h3][loop] = LR78[h3]*LR6_2[h2]/DV2*(Q_FP_FSR[h1]*LR_P6[h2]+Q_FZ_FSR[h1]*LR_Z6[h2]+Q_FW_FSR[h1]*LR_W6[h2])*M_FSR_undressed[h1][h2][h3][loop][2];
            M_dressed[h1][h2][h3][loop] += LR78[h3]*LR5_2[h2]/DV2*(Q_FP_FSR[h1]*LR_P5[h2]+Q_FZ_FSR[h1]*LR_Z5[h2]+Q_FW_FSR[h1]*LR_W5[h2])*M_FSR_undressed[h1][h2][h3][loop][3];
          }
          
//           cout << "+FSR: " << M_dressed[h1][h2][h3][loop] << endl;

          M_dressed[h1][h2][h3][loop] *= double_complex(0,1);
        }
      }
    }
  }
}

void add_hel_amps(vector<vector<vector<vector<double_complex> > > > &M1, vector<vector<vector<vector<double_complex> > > > &M2) {
  for (int h1=0; h1<2; h1++) {
    for (int h2=0; h2<2; h2++) {
      for (int h3=0; h3<2; h3++) {
        for (int loop=0; loop<3; loop++) {
          M1[h1][h2][h3][loop] += M2[h1][h2][h3][loop];
        }
      }
    }
  }
}

void ppllll24_calculate_amplitude_doublevirtual(observable_set & oset) {
  static Logger logger("ppllll24_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
//   for (int i=0; i<7; i++) {
//     cout << "fourvector p" << i << "(" << osi_p_parton[0][i].x0()  << "\t" << osi_p_parton[0][i].x1()  << "\t" << osi_p_parton[0][i].x2()  << "\t" << osi_p_parton[0][i].x3()  << ");" << endl;
// }
// 
  // cout << "      p(1,1)=" << -osi_p_parton[0][1].x1() << endl;
  // cout << "      p(1,2)=" << -osi_p_parton[0][1].x2() << endl;
  // cout << "      p(1,3)=" << -osi_p_parton[0][1].x3() << endl;
  // cout << "      p(1,4)=" << -osi_p_parton[0][1].x0() << endl;

  // cout << "      p(2,1)=" << -osi_p_parton[0][2].x1() << endl;
  // cout << "      p(2,2)=" << -osi_p_parton[0][2].x2() << endl;
  // cout << "      p(2,3)=" << -osi_p_parton[0][2].x3() << endl;
  // cout << "      p(2,4)=" << -osi_p_parton[0][2].x0() << endl;

  // cout << "      p(3,1)=" << osi_p_parton[0][3].x1() << endl;
  // cout << "      p(3,2)=" << osi_p_parton[0][3].x2() << endl;
  // cout << "      p(3,3)=" << osi_p_parton[0][3].x3() << endl;
  // cout << "      p(3,4)=" << osi_p_parton[0][3].x0() << endl;

  // cout << "      p(4,1)=" << osi_p_parton[0][5].x1() << endl;
  // cout << "      p(4,2)=" << osi_p_parton[0][5].x2() << endl;
  // cout << "      p(4,3)=" << osi_p_parton[0][5].x3() << endl;
  // cout << "      p(4,4)=" << osi_p_parton[0][5].x0() << endl;

  // cout << "      p(5,1)=" << osi_p_parton[0][4].x1() << endl;
  // cout << "      p(5,2)=" << osi_p_parton[0][4].x2() << endl;
  // cout << "      p(5,3)=" << osi_p_parton[0][4].x3() << endl;
  // cout << "      p(5,4)=" << osi_p_parton[0][4].x0() << endl;

  // cout << "      p(6,1)=" << osi_p_parton[0][6].x1() << endl;
  // cout << "      p(6,2)=" << osi_p_parton[0][6].x2() << endl;
  // cout << "      p(6,3)=" << osi_p_parton[0][6].x3() << endl;
  // cout << "      p(6,4)=" << osi_p_parton[0][6].x0() << endl;
  
  int V1,V2;
  
  int q1;
  if (osi_name_process == "uu~_emmumepmup" ||
      osi_name_process == "uu~_ememepep" ||
      osi_name_process == "uu~_emmupvmve~" ||
      osi_name_process == "uu~_emepvmvm~" ||
      osi_name_process == "uu~_emepveve~" ||
      osi_name_process == "ud~_emepmupvm" ||
      osi_name_process == "ud~_emepepve") {
    q1 = 2;
  }
  else if (osi_name_process == "dd~_emmumepmup" ||
	   osi_name_process == "dd~_ememepep" ||
	   osi_name_process == "dd~_emmupvmve~" ||
	   osi_name_process == "dd~_emepvmvm~" ||
	   osi_name_process == "dd~_emepveve~" ||
	   osi_name_process == "bb~_emmumepmup" ||
	   osi_name_process == "bb~_ememepep" ||
	   osi_name_process == "bb~_emepvmvm~" ||
	   osi_name_process == "bb~_emmupvmve~" ||
	   osi_name_process == "bb~_emepveve~" ||
	   osi_name_process == "du~_emmumepvm~" ||
	   osi_name_process == "du~_ememepve~") {
    q1 = 1;
  }
  else {
    cout << osi_name_process << endl;
    assert(false);
  }
  
  vector<fourvector> p(osi_p_parton[0].size());
 
  vector<vector<vector<vector<double_complex> > > > M_dressed,M_dressed2;
  vector<vector<vector<vector<double_complex> > > > M_dressed_tmp;
  M_dressed.resize(2);
  M_dressed2.resize(2);
  M_dressed_tmp.resize(2);
  for (int h1=0; h1<2; h1++) {
    M_dressed[h1].resize(2);
    M_dressed2[h1].resize(2);
    M_dressed_tmp[h1].resize(2);
    for (int h2=0; h2<2; h2++) {
      M_dressed[h1][h2].resize(2);
      M_dressed2[h1][h2].resize(2);
      M_dressed_tmp[h1][h2].resize(2);
      for (int h3=0; h3<2; h3++) {
        M_dressed[h1][h2][h3].resize(3,0.0);
        M_dressed2[h1][h2][h3].resize(3,0.0);
        M_dressed_tmp[h1][h2][h3].resize(3);
      }
    }
  }

  double sym_factor=1;

  if (osi_name_process == "uu~_emmumepmup" ||
      osi_name_process == "dd~_emmumepmup" ||
      osi_name_process == "bb~_emmumepmup") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][6];
  }
  else if (osi_name_process == "uu~_ememepep" ||
	   osi_name_process == "dd~_ememepep" ||
	   osi_name_process == "bb~_ememepep") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][6];

    sym_factor=1.0/4;
  }
  else if (osi_name_process == "uu~_emmupvmve~" ||
	   osi_name_process == "dd~_emmupvmve~" ||
	   osi_name_process == "bb~_emmupvmve~") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][6];
    p[5]=osi_p_parton[0][5];
    p[6]=osi_p_parton[0][4];
  }
  else if (osi_name_process == "uu~_emepvmvm~" ||
	   osi_name_process == "dd~_emepvmvm~" ||
	   osi_name_process == "bb~_emepvmvm~" ||
	   osi_name_process == "uu~_emepveve~" ||
	   osi_name_process == "dd~_emepveve~" ||
	   osi_name_process == "bb~_emepveve~") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][4];
    p[5]=osi_p_parton[0][5];
    p[6]=osi_p_parton[0][6];
  }
  else if (osi_name_process == "ud~_emepmupvm") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][4];
    p[5]=osi_p_parton[0][6];
    p[6]=osi_p_parton[0][5];
  }
  else if (osi_name_process == "ud~_emepepve") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][4];
    p[5]=osi_p_parton[0][6];
    p[6]=osi_p_parton[0][5];

    sym_factor = 1.0/2;
  }
  else if (osi_name_process == "du~_emmumepvm~") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][6];
  }
  else if (osi_name_process == "du~_ememepve~") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][6];

    sym_factor = 1.0/2;
  }
  
  vector<vector<vector<double_complex> > > E(3,vector<vector<double_complex> >(4,vector<double_complex>(9)));
  static vector<vector<vector<double_complex> > > E_saved(3,vector<vector<double_complex> >(4,vector<double_complex>(9)));

  vector<double_complex> E_tree(9);

  double s = (p[1]+p[2]).m2();
  double t = (p[1]-(p[3]+p[4])).m2();
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();

  double static t_saved=0;

  double static t_saved2=0,t_saved3=0;
  static vector<vector<vector<double_complex> > > E_saved2(3,vector<vector<double_complex> >(4,vector<double_complex>(9)));

  static int count=1;

  int static init=1;
  if (init==1) {
    // initialize the form factor library
    VVprime::getInstance().initAmplitude(osi_N_f);
    init=0;
  }

  bool debug = Log::getLogThreshold()<=LOG_DEBUG; 
    
  // compute the form factors
  int tch;
  static int count_tch=0;
  ++count_tch;
  
  if((H2_mode==0 && t!=t_saved) || (H2_mode==-3 && t!=t_saved2) ){
    vector<vector<vector<double> > > E_ip1(2,vector<vector<double> >(4,vector<double>(9)));

    // static std::chrono::duration<double> elapsed;
    // static std::chrono::duration<double> elapsed2;
    // static std::chrono::duration<double> elapsed3;

    if(H2_mode==0||H2_mode==-2){

      // auto start = std::chrono::high_resolution_clock::now();
      // VVprime::getInstance().computeAmplitude(s,t,ma2,mb2,debug);
      // auto finish = std::chrono::high_resolution_clock::now();
      // elapsed = finish - start;

      // auto start2 = std::chrono::high_resolution_clock::now();
      Btwxt_fit(ma2,mb2,s,t,E_ip1);
      // auto finish2 = std::chrono::high_resolution_clock::now();
      // elapsed2 = finish2 - start2;

      
    }
    else{
      // 	auto start3 = std::chrono::high_resolution_clock::now();
	VVprime::getInstance().computeAmplitude(s,t,ma2,mb2,debug);
	// auto finish3 = std::chrono::high_resolution_clock::now();
	// elapsed3=finish3-start3;
	//}
    }

    // if(H2_mode==-3){
    //   ofstream time_file;
    //   //static bool init=true;
    //   //if(init){
    //   time_file.open("timing.dat",ios::out | ios::app);
    //   //init=false;
    //   //      }
    //   time_file.setf(ios::fixed, ios::floatfield);
    //   time_file.setf(ios::showpoint);
    //   time_file<<showpos<<setprecision(9)<<"VVAMP "<<elapsed.count()<<" INTERP "<<elapsed2.count()<<" INTERP+RESCUE "<<elapsed3.count()<<endl;
      //cout<<showpos<<setprecision(9)<<"VVAMP "<<elapsed.count()<<" INTERP "<<elapsed2.count()<<" INTERP+RESCUE "<<elapsed3.count()<<endl;
//" time reduction "<<setw(24)<<(elapsed.count()-elapsed2.count())/elapsed.count()<<" time performance "<<setw(24)<<(elapsed.count()-elapsed2.count())/elapsed2.count()<<" faster by a factor of  "<<setw(24)<<elapsed.count()/elapsed2.count()<<endl;
    //}

    for (int loop=0; loop<3; loop++) {
      for (int type=0; type<4; type++) {
	for (int i=1; i<=9; i++) {
	  int ii=i-1;
	  	  
	    if(H2_mode==0||H2_mode==-2){
	      // here, you have always a provider for born and virtual
	      // so skip loop=0,1
	      if(loop<2)
		E[loop][type][i-1] = 0;	      		
	      else
		E[loop][type][i-1] = double_complex(E_ip1[0][type][i-1],E_ip1[1][type][i-1]);	    
	    }
	    else if(H2_mode==-3){
	      double Er,Ei;
	      VVprime::getInstance().getAmplitude(loop,i,type,Er,Ei);
	      E[loop][type][i-1] = double_complex(Er,Ei);
	    }
	  
	  if(H2_mode==0){
	    E_saved[loop][type][i-1] = E[loop][type][i-1];}
	  else if(H2_mode==-3){
	    E_saved2[loop][type][i-1] = E[loop][type][i-1];	  
	  }
	}
      }
    }
  }
  else{
    for (int loop=0; loop<3; loop++) {
      for (int type=0; type<4; type++) {
	for (int i=1; i<=9; i++) {
	  if(H2_mode==0){
	    E[loop][type][i-1] = E_saved[loop][type][i-1];}
	  else if(H2_mode==-3){
	    E[loop][type][i-1] = E_saved2[loop][type][i-1];}
	}
      }
    }
  }

if(H2_mode==0){
  t_saved=t;}
 else if(H2_mode==-3){
   t_saved2=t;
 }

 
  // Obtain Quark form factor
  vector<double_complex> F(3);
  F[0]=-E[0][3][8]/4.0;   
  F[1]=-E[1][3][8]/4.0;   
  F[2]=-E[2][3][8]/4.0;   

  if (osi_name_process == "uu~_emmumepmup" ||
      osi_name_process == "dd~_emmumepmup" ||
      osi_name_process == "bb~_emmumepmup" ||
      osi_name_process == "uu~_ememepep" ||
      osi_name_process == "dd~_ememepep" ||
      osi_name_process == "bb~_ememepep"){
    // ZZ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_ZLL, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_ZLL, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_ZLL, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamgam contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_PHOT, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_PHOT, V_PHOT, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_PHOT, V_PHOT, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);    

    // gamZ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_ZLL, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);    

    // Zgam contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_PHOT, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);    

  }
  else if (osi_name_process == "uu~_emmupvmve~" ||
	   osi_name_process == "dd~_emmupvmve~" ||
	   osi_name_process == "bb~_emmupvmve~"){
    // WW contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_WMIN, V_WPLU, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_WMIN, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_WMIN, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
  }
  else if (osi_name_process == "uu~_emepvmvm~" ||
	   osi_name_process == "dd~_emepvmvm~" ||
	   osi_name_process == "bb~_emepvmvm~" ||
	   osi_name_process == "uu~_emepveve~" ||
	   osi_name_process == "dd~_emepveve~" ||
	   osi_name_process == "bb~_emepveve~"){
    // ZZ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_ZNN, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_ZNN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_ZNN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    // gamZ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_ZNN, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);    
  }
  else if (osi_name_process == "ud~_emepmupvm" ||
	   osi_name_process == "ud~_emepepve"){
    // ZW^+ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_WPLU, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    // this contribution only occurs once, as it only contains intermediate W's
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamW^+ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_WPLU, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_PHOT, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
  }
  else if (osi_name_process == "du~_emmumepvm~" ||
	   osi_name_process == "du~_ememepve~"){
    // ZW^- contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_WMIN, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_WMIN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    // this contribution only occurs once, as it only contains intermediate W's
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_WMIN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamW^- contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_WMIN, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_PHOT, V_WMIN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
  }
  else {
    assert(false);
  }
  
  // same flavour case: take into account additional crossings
  if (osi_name_process == "ud~_emepepve"){
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][6];
    p[6]=osi_p_parton[0][4];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    VVprime::getInstance().computeAmplitude(s,t,ma2,mb2,debug);
    for (int loop=0; loop<3; loop++) {
      for (int type=0; type<4; type++) {
        for (int i=1; i<=9; i++) {
          double Er,Ei;
          VVprime::getInstance().getAmplitude(loop,i,type,Er,Ei);
          E[loop][type][i-1] = double_complex(Er,Ei);
        }
      }
    }


    // ZW^+ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_WPLU, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    // this contribution only occurs once, as it only contains intermediate W's
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    // gamW^+ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_WPLU, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_PHOT, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
  }
  else if (osi_name_process == "du~_ememepve~") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][4];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][3];
    p[6]=osi_p_parton[0][6];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    VVprime::getInstance().computeAmplitude(s,t,ma2,mb2,debug);
    for (int loop=0; loop<3; loop++) {
      for (int type=0; type<4; type++) {
        for (int i=1; i<=9; i++) {
          double Er,Ei;
          VVprime::getInstance().getAmplitude(loop,i,type,Er,Ei);
          E[loop][type][i-1] = double_complex(Er,Ei);
        }
      }
    }
    // ZW^- contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_WMIN, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_WMIN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    // this contribution only occurs once, as it only contains intermediate W's
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_WMIN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    // gamW^- contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_WMIN, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_PHOT, V_WMIN, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
  }
  else if (osi_name_process == "uu~_ememepep" ||
	   osi_name_process == "dd~_ememepep" ||
	   osi_name_process == "bb~_ememepep") {
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][6];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][5];
		
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    VVprime::getInstance().computeAmplitude(s,t,ma2,mb2,debug);
    for (int loop=0; loop<3; loop++) {
      for (int type=0; type<4; type++) {
        for (int i=1; i<=9; i++) {
          double Er,Ei;
          VVprime::getInstance().getAmplitude(loop,i,type,Er,Ei);
          E[loop][type][i-1] = double_complex(Er,Ei);
        }
      }
    }

    // ZZ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_ZLL, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_ZLL, V_ZLL, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_ZLL, V_ZLL, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    // gamgam contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_PHOT, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_PHOT, V_PHOT, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_PHOT, V_PHOT, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);    

    // gamZ contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_PHOT, V_ZLL, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);    
    // Zgam contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_ZLL, V_PHOT, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
  }
  else if (osi_name_process == "uu~_emepveve~" ||
	   osi_name_process == "dd~_emepveve~" ||
	   osi_name_process == "bb~_emepveve~") {
    // WW contribution
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][6];
    p[5]=osi_p_parton[0][5];
    p[6]=osi_p_parton[0][4];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    VVprime::getInstance().computeAmplitude(s,t,ma2,mb2,debug);
    for (int loop=0; loop<3; loop++) {
      for (int type=0; type<4; type++) {
        for (int i=1; i<=9; i++) {
          double Er,Ei;
          VVprime::getInstance().getAmplitude(loop,i,type,Er,Ei);
          E[loop][type][i-1] = double_complex(Er,Ei);
        }
      }
    }

    // WW contribution
    ppllll24_computeVVprimeHelAmplitudes(oset, V_WMIN, V_WPLU, q1, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 1, V_WMIN, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    ppllll24_computeVVprimeHelAmplitudesSR(oset, 2, V_WMIN, V_WPLU, q1, p, F, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
  }

  
  // square and add up helicity amplitudes
  osi_QT_A0=0;
  osi_QT_A1=0;
  osi_QT_A2=0;
  for (int h1=0; h1<2; h1++) {
    for (int h2=0; h2<2; h2++) {
      for (int h3=0; h3<2; h3++) {
	if(use_mcfm){
	  M_dressed[h1][h2][h3][0]=double_complex(twoloop_contributions_.helamp_bornr[h1][h2][h3],twoloop_contributions_.helamp_borni[h1][h2][h3]);
	}	
        osi_QT_A0 += norm(M_dressed[h1][h2][h3][0])+norm(M_dressed2[h1][h2][h3][0]);
	if(use_mcfm){
	  M_dressed[h1][h2][h3][1]=double_complex(twoloop_contributions_.helamp_1loopr[h1][h2][h3],twoloop_contributions_.helamp_1loopi[h1][h2][h3]);
	}
        osi_QT_A1 += 2*real(M_dressed[h1][h2][h3][0]*conj(M_dressed[h1][h2][h3][1]))+2*real(M_dressed2[h1][h2][h3][0]*conj(M_dressed2[h1][h2][h3][1]));

        osi_QT_A2 += norm(M_dressed[h1][h2][h3][1])+norm(M_dressed2[h1][h2][h3][1]);
        osi_QT_A2 += 2*real(M_dressed[h1][h2][h3][0]*conj(M_dressed[h1][h2][h3][2]))+2*real(M_dressed2[h1][h2][h3][0]*conj(M_dressed2[h1][h2][h3][2]));
        if (h2==h3) {
          osi_QT_A0 -= 2.0*real(conj(M_dressed[h1][h2][h3][0])*M_dressed2[h1][h2][h3][0]);
          
          osi_QT_A1 -= 2.0*real(conj(M_dressed[h1][h2][h3][0])*M_dressed2[h1][h2][h3][1]);
          osi_QT_A1 -= 2.0*real(conj(M_dressed2[h1][h2][h3][0])*M_dressed[h1][h2][h3][1]);
          
          osi_QT_A2 -= 2.0*real(conj(M_dressed[h1][h2][h3][1])*M_dressed2[h1][h2][h3][1]);
          osi_QT_A2 -= 2.0*real(conj(M_dressed[h1][h2][h3][0])*M_dressed2[h1][h2][h3][2]);
          osi_QT_A2 -= 2.0*real(conj(M_dressed2[h1][h2][h3][0])*M_dressed[h1][h2][h3][2]);
        }
      }
    }
  }

  double alpha_e = osi_msi.alpha_e;

  osi_QT_A0 *= pow(4*M_PI*alpha_e,4);
  osi_QT_A0 /= (4*9); // averaging factor
  osi_QT_A0 *= 3; // color factor
  osi_QT_A0 *= sym_factor;

  osi_QT_A1 *= pow(4*M_PI*alpha_e,4);
  osi_QT_A1 /= (4*9); // averaging factor
  osi_QT_A1 *= 3; // color factor
  osi_QT_A1 *= sym_factor;

  osi_QT_A2 *= pow(4*M_PI*alpha_e,4);
  osi_QT_A2 /= (4*9); // averaging factor
  osi_QT_A2 *= 3; // color factor
  osi_QT_A2 *= sym_factor;

  osi_QT_A1 *= osi_alpha_S/2/M_PI;
  osi_QT_A2 *= pow(osi_alpha_S/2/M_PI,2);

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
