void ppllll24_calculate_H2(observable_set & oset);
void ppllll24_calculate_amplitude_doublevirtual(observable_set & oset);

class VVprimePrivate;

class VVprime {
  public:
    static VVprime &getInstance() {
      static VVprime instance;
      return instance;
    }
    ~VVprime();
    void initAmplitude(int Nf);
    void computeAmplitude(double s, double t, double ma2, double mb2, bool debug=false);
    void getAmplitude(int l, int i, int j, double &Er, double &Ei);
  private:
    VVprime();
    VVprime(VVprime const&);
    void operator=(VVprime const&);
    
    VVprimePrivate* mData;
};
