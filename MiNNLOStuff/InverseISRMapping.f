c     inverse integral of spreading, denominator of F_corr of paper
      subroutine evaluubjakob(res, iuborn)
      use MiNLOdebug
      implicit none
      include 'brinclude.h'
      include 'pwhg_kn.h'
!      include 'pwhg_flst.h'
      include 'pwhg_math.h'
      include 'pwhg_pdf.h'
      integer, parameter :: nlegs=nlegbornexternal
      real * 8 res
      real * 8 amp2uub,ebeam 
      real * 8 pb(0:3,nlegs-1),sh, phint, fourPtsqOsh,x1b,x2b,pt,
     1     ptmax,mh2,yh,e,e2,pbsyst(0:3)
      real * 8 pdf1(-pdf_nparton:pdf_nparton),
     1     pdf2(-pdf_nparton:pdf_nparton)
      integer i
      real * 8 powheginput
      external powheginput
      logical,save:: ini=.true.
      logical nores
      save nores
      real*8 pb_fs_tmp(0:3,nlegborn),pb_fs_lab(0:3,nlegs)
      integer bnlegs_fs,bflav_fs(nlegborn)
      logical makecuts
      integer iuborn
      
      if(ini)then
         nores=powheginput('#nores').eq.1
         ini=.false.
      endif

      if(.not.nores)then
         call get_fs_particles_momenta(flst_bornlength(iuborn),
     $        flst_born(:,iuborn),flst_bornres(:,iuborn),kn_pborn,
     $        bnlegs_fs,bflav_fs,pb_fs_tmp)
         pb_fs_lab(:,1:nlegs)=pb_fs_tmp(:,1:nlegs)
      else
         bflav_fs(:)=flst_born(:,1)
         pb_fs_lab(:,1:nlegs)=kn_pborn(:,1:nlegs)
      endif

      call invISRmap(pb_fs_lab,nlegs,pb)
      if(dbg_phsp.and.dbg_phsp_with_cuts.and.dbg_phspproj)then
         makecuts=.false.
         call uub_phasespace_check(pb,makecuts)
         if(makecuts)then
            res=0d0
            return
         endif
      endif

      e2 =  kn_sbeams
      e = sqrt(e2)
      ebeam = e/2d0
      x1b = pb(0,1)/ebeam
      x2b = pb(0,2)/ebeam
      sh = x1b*x2b*kn_sbeams 

      brkn_xb1 = x1b
      brkn_xb2 = x2b
      brkn_sborn = sh
      brkn_pborn(:,:) = pb(:,:)

C     compute Phase space integral and divide it out 

c     pbsyst(0:3) is the colourless "boson" momentum
      pbsyst(:)=0d0
      do i=3,nlegs
c     the sequence of colourless particles is the same in all flst_born(i,X) (X does not matter), so we pick X=1
         if (abs(bflav_fs(i)).gt.6) then
            pbsyst(:)=pbsyst(:) + pb_fs_lab(:,i)
         endif
      enddo
      pt = dsqrt(pbsyst(1)**2+pbsyst(2)**2)

c this should be the same as the pt of the radiated parton
      if(.not.uubornonlyD1.and..not.uubornonlyD3)then
         if (abs(pt-dsqrt(pb_fs_lab(1,nlegs)**2+pb_fs_lab(2,nlegs)**2)) .gt. 1d-6) then !DL increasing from 1d-7 to 1d-6
            write(*,*) 'ERROR: in computing pt of colourless system!!!'
            !stop
         endif
      endif

      fourPtsqOsh = 4d0*pt**2/sh
      call borndenomint(x1b,x2b,fourPtsqOsh,phint)
      phint = phint * pt / (4d0*pi**2) 

C     include here flux factor 
      if (phint .ne. 0d0) then
         res = 1d0/phint/(2d0*sh )
         if(dbg_phsp) res=1d0/phint ! Do not include flux factor for phsp calculation
      else
         res = -1d10
      endif
      end


      subroutine evaluubsigma(res, iuborn)
      implicit none
      include 'brinclude.h'
      include 'pwhg_kn.h'
      include 'pwhg_math.h'
      include 'pwhg_pdf.h'
      integer, parameter :: nlegs=nlegbornexternal
      real * 8 res
      real * 8 amp2uub,ebeam 
      real * 8 pb(0:3,nlegs-1),sh, phint, fourPtsqOsh,x1b,x2b,pt,
     1     ptmax,mh2,yh,e,e2
      real * 8 pdf1(-pdf_nparton:pdf_nparton),
     1     pdf2(-pdf_nparton:pdf_nparton),pbsyst(0:3)
      integer i

      real * 8 powheginput
      external powheginput
      logical,save:: ini=.true.
      logical nores
      save nores

      real*8 pb_fs_tmp(0:3,nlegborn),pb_fs_lab(0:3,nlegs)
      integer bnlegs_fs,bflav_fs(nlegborn)
      integer iuborn

      if(ini)then
         nores=powheginput('#nores').eq.1
         ini=.false.
      endif

      if(.not.nores)then
         call get_fs_particles_momenta(flst_bornlength(iuborn),
     $        flst_born(:,iuborn),flst_bornres(:,iuborn),kn_pborn,
     $        bnlegs_fs,bflav_fs,pb_fs_tmp)
         pb_fs_lab(:,1:nlegs)=pb_fs_tmp(:,1:nlegs)
      else
         bflav_fs(:)=flst_born(:,1)
         pb_fs_lab(:,1:nlegs)=kn_pborn(:,1:nlegs)
      endif

      
      call invISRmap(pb_fs_lab,nlegs,pb)
      call uub_for_minnlo(pb,2,amp2uub)
      e2 =  kn_sbeams
      e = sqrt(e2)
      ebeam = e/2
      x1b = pb(0,1)/ebeam
      x2b = pb(0,2)/ebeam
      sh = x1b*x2b*kn_sbeams 
      call pdfcall(1,x1b,pdf1)
      call pdfcall(2,x2b,pdf2)
      res = amp2uub * pdf1(0)*pdf2(0) 

C     compute Phase space integral and divide it out 

c     pbsyst(0:3) is the colourless "boson" momentum
      pbsyst(:)=0d0
      do i=3,nlegs
c     the sequence of colourless particles is the same in all flst_born(i,X) (X does not matter), so we pick X=1
         if (abs(bflav_fs(i)).gt.6) then
            pbsyst(:)=pbsyst(:) + pb_fs_lab(:,i)
         endif
      enddo
      pt = sqrt(pbsyst(1)**2+pbsyst(2)**2)
      
      if (abs(pt-sqrt(pb_fs_lab(1,nlegs)**2+pb_fs_lab(2,nlegs)**2)) .gt. 1d-7) then
          write(*,*) 'ERROR: in computing pt of colourless system!!!'
          !stop
      endif

      fourPtsqOsh = 4d0*pt**2/sh
      call borndenomint(x1b,x2b,fourPtsqOsh,phint)
      phint = phint * pt / (4d0*pi**2)

      yh = 0.5d0*log(((pb(0,1)+pb(0,2)) + (pb(3,1)+pb(3,2)))/
     1     ((pb(0,1)+pb(0,2)) - (pb(3,1)+pb(3,2))))
      mh2 = kn_sbeams*x1b*x2b

      ptmax= sqrt((((e2-mh2)/2/e)**2+mh2)/cosh(yh)**2 - mh2)
C     include here flux factor 
      res = res/phint / (2d0*sh ) / ptmax
      
      end


c     Inverse mapping from p(0:3,n) -> pb(0:3,n-1), following      
c     Sec 5.1.1 of arXiv:0709.2092      
c     The n-th momentum is interpreted as the radiated particle in ISR
      subroutine invISRmap(p,n,pb)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      integer n
      real * 8 p(0:3,n)
      real * 8 pb(0:3,n-1)
      real * 8 ktot(0:3),ktotl(0:3),prad(0:3)
      real * 8 betalvec(3), betal
      real * 8 betatvec(3), betat,mod
      real * 8 pCM(0:3,n)
      integer i,mu
      real * 8 dotp
      external dotp
      real * 8 ecm, xplus, xminus, y, csi, xplusbar, xminusbar,v,ebeam
      real * 8 zero
      
      ktot = p(:,1) + p(:,2) - p(:,n)
c     longitudinal boost
      betalvec(1) = 0d0
      betalvec(2) = 0d0
      betalvec(3) = 1d0
      betal = - ktot(3)/ktot(0)
      v = ktot(3)/ktot(0)
      call mboost(1,betalvec,betal,ktot,ktot)

c     transverse boost
      mod = sqrt(ktot(1)**2+ktot(2)**2)
      betatvec(1) = ktot(1)/mod
      betatvec(2) = ktot(2)/mod
      betatvec(3) = 0d0
      betat = -mod/ktot(0)
      call mboost(1,betatvec,betat,ktot,ktot)

c     apply  BL^(-1) BT BL
      call mboost(n-3,betalvec,betal,p(:,3),pb(:,3))
      call mboost(n-3,betatvec,betat,pb(:,3),pb(:,3))
      call mboost(n-3,betalvec,-betal,pb(:,3),pb(:,3))
      ktot=0d0
      do i=3,n-1
         ktot=ktot+pb(:,i)
      enddo

      ebeam=sqrt(kn_sbeams)

c     use the fact that
c     xplusbar * Kplus + xminusbar * Kminus = ktot (after boosts)!
      xplusbar = (ktot(0)+ktot(3))/ebeam
      xminusbar = (ktot(0)-ktot(3))/ebeam

      pb(0,1) = ebeam/2d0*xplusbar
      pb(1,1) = 0
      pb(2,1) = 0
      pb(3,1) = ebeam/2d0*xplusbar

      pb(0,2) = ebeam/2d0*xminusbar
      pb(1,2) = 0
      pb(2,2) = 0
      pb(3,2) = -ebeam/2d0*xminusbar

      return
      
c     check momentum conservation
      ktot=ktot-pb(:,1)-pb(:,2)
      zero = 0d0
      do mu=0, 3
         zero = zero + ktot(mu)**2
      enddo
      if (zero.gt.1d-12) then
         write(*,*) 'VIOLATION MOM CONSER ', zero
      endif
      
      end


c     Inverse mapping from p(0:3,n) -> pb(0:3,n-1), following      
c     Sec 5.1.1 of arXiv:0709.2092      
c     The n-th momentum is interpreted as the radiated particle in ISR
      subroutine invISRmap2(p,n,pb)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      integer n
      real * 8 p(0:3,n)
      real * 8 pb(0:3,n-1)
      real * 8 dotp,mee,mepg,memg,pTg,Ebeam
      external dotp

      pb(:,:) = 0
      print*, p

      mee  = dsqrt(2*dotp(p(:,3),p(:,4)))
      memg = dsqrt(2*dotp(p(:,3),p(:,5)))
      mepg = dsqrt(2*dotp(p(:,4),p(:,5)))
      pTg  = dsqrt(p(1,5)**2+p(2,5)**2)

      Ebeam   = dsqrt(1/4d0 * (mee**2+mepg**2+memg**2))
      pb(0,5) = (mepg**2 + memg**2)/Ebeam/2d0
      pb(0,4) = (2d0 * pb(0,5) * Ebeam + mee**2 - memg**2)/Ebeam/2d0
      pb(0,3) = 2d0 * Ebeam - pb(0,4) - pb(0,5)
c$$$      print*, "E3 orig  = ", pb(0,3)
c$$$      pb(0,3) = (2d0 * pb(0,5) * Ebeam + mee**2 - mepg**2)/Ebeam/2d0
c$$$      print*, "E3 check = ", pb(0,3)
      print*, "E4, E5, Ebeam", pb(0,4),pb(0,5),Ebeam
      print*, "mee, memg, mepg", mee, memg, mepg
      pb(2,5) = pTg
      pb(3,5) = dsqrt(pb(0,5)**2-pb(2,5)**2)
      print*,pb(3,5),pb(0,5)**2-pb(2,5)**2,pb(0,5),pb(2,5)
      
      pb(3,4) = 
     - pb(0,4)*pb(0,5)*pb(3,5) - memg**2*pb(3,5) !- 
!     -       dsqrt(-(pb(0,4)**2*pb(0,5)**2*pTg**2) + 2*pb(0,4)*pb(0,5)*memg**2*pTg**2 - memg**4*pTg**2 + 
!     -         pb(0,4)**2*pb(3,5)**2*pTg**2 + pb(0,4)**2*pTg**4))/(pb(3,5)**2 + pTg**2)
      print*, "pz4", pb(3,4)

      pb(3,4) = 
     - (pb(0,4)*pb(0,5)*pb(3,5) - memg**2*pb(3,5) + 
     -       Sqrt(-(pb(0,4)**2*pb(0,5)**2*pTg**2) + 2*pb(0,4)*pb(0,5)*memg**2*pTg**2 - memg**4*pTg**2 + 
     -         pb(0,4)**2*pb(3,5)**2*pTg**2 + pb(0,4)**2*pTg**4))/(pb(3,5)**2 + pTg**2)
      print*, "pz4", pb(3,4)

      pb(2,4) = dsqrt(pb(0,4)**2-pb(1,4)**2-pb(3,4)**2)

      pb(2:3,3) = -pb(2:3,4)-pb(2:3,5)
      print*, "pty2 orig  = ", pb(2,3)
      pb(2,3) = dsqrt(pb(0,3)**2-pb(1,3)**2-pb(3,3)**2)
      print*, "pty2 check = ", pb(2,3)


      pb(0,1) = Ebeam/2d0!*xplusbar
      pb(1,1) = 0
      pb(2,1) = 0
      pb(3,1) = Ebeam/2d0!*xplusbar

      pb(0,2) = Ebeam/2d0!*xminusbar
      pb(1,2) = 0
      pb(2,2) = 0
      pb(3,2) = -Ebeam/2d0!*xminusbar



      print*, "mee:  ", mee ,  dsqrt(2*dotp(pb(:,3),pb(:,4)))
      print*, "memg: ", memg,  dsqrt(2*dotp(pb(:,3),pb(:,5)))
      print*, "mepg: ", mepg,  dsqrt(2*dotp(pb(:,4),pb(:,5)))
      print*, "pTg:  ", pTg,   dsqrt(p(1,5)**2+p(2,5)**2)
      stop
      end
