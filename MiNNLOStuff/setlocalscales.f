      subroutine setlocalscales(iuborn,imode,rescfac)
c     returns the rescaling factor including sudakov form factors and
c     coupling rescaling, for Born (imode=1) and NLO corrections (imode=2)
c     (imode=3) is as imode=1, but with the computation of the d3 term. 
c      use libnnlops
      use MiNLO_cache
      use MiNLOdebug
      use rad_tools
      use pdfs_tools
      use sudakov_radiators
      use internal_parameters, pi_hoppet => pi, cf_hoppet => cf, ca_hoppet => ca, tf_hoppet => tf
      implicit none
      interface
         subroutine get_B_V1_V2(pborn_UUB,msqB,msqV1,msqV2,nflav)
         implicit none
         include 'nlegborn.h'
         integer, parameter :: nlegs=nlegbornexternal-1
         integer, intent(in) :: nflav
         real *8 pborn_UUB(0:3,nlegs)
         double precision,dimension(-nflav:nflav,-nflav:nflav):: msqb,msqV1
         double precision,dimension(-nflav:nflav,-nflav:nflav),optional::msqV2
         logical,save :: ini=.true.
         end subroutine get_B_V1_V2
      end interface
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_br.h'
      include 'pwhg_st.h'
      include 'pwhg_pdf.h'
      include 'pwhg_flg.h'
      include 'pwhg_math.h'
      include 'minnlo_flg.h'
      include 'pwhg_mintw.h'
      include 'PhysPars.h'
      integer,intent(in) :: iuborn,imode
      real*8, intent(out) :: rescfac
      real*8 d3terms
      common/d3terms/d3terms
      integer, parameter :: nlegs=nlegbornexternal
      real * 8 expsud,sudakov,pwhg_alphas,resc_coupl
      real * 8 ptb2,mb2,mu2,alphas,alphas_ptb2,b0,optb2,omuf2
     $     ,alphasdterms,mu2dterms,mu2fact
      integer,save:: nflav
      integer pdf_set
      real * 8 pb(0:3), ptmax, yh
      real*8,save,dimension(:,:),allocatable::sudakov_form_factor
      real*8 od3terms
      integer oimode,i,j
      integer flav
      common/flav_initial_state/ flav
      save optb2,oimode,omuf2,od3terms
      data optb2/-1d0/
      logical ini,bmass_in_minlo_flg
      common/c_bmass_in_minlo_flg/bmass_in_minlo_flg
      data ini/.true./
      save ini
      real * 8 powheginput,factsc2min,frensc2min,as,y,b1,tmp,bfact
      save factsc2min,frensc2min,b0,b1
      integer imax
      double precision D1, D2, D3, xx1, xx2
      double precision D1_times_Delta2
      double precision,save,dimension (:,:),allocatable::msqB,msqV1,msqV2,msqB_sav,msqV1_sav,msqV2_sav
      double precision pborn(1:nlegs-1,1:4), L, pt, jacob
      character*100 pdf_name
      double precision pborn_UUB(0:3,nlegs-1),e2,e,ebeam
      double precision Delta1
      double precision,save, dimension (:,:), allocatable::Delta2
      double precision s3, kappar, kappaf, Q0_ini
      double precision mb2_sav, kappar_sav, kappaf_sav, kappaq_sav, st_alpha_sav
      double precision xx1_sav, xx2_sav
      save mb2_sav, kappar_sav, kappaf_sav, kappaq_sav, Q0_ini, xx1_sav, xx2_sav, st_alpha_sav
      double precision, parameter :: accuracy=1d-2
      double precision alphas_cutoff_fact, lambda,as_old
      double precision virt_over_born, virt2_over_born, pTg2, gamma_UUB_damping
      integer uub_flav(1:nlegbornexternal-1)
      save alphas_cutoff_fact
      logical use_analytic_alphas
      parameter (use_analytic_alphas = .false.)
      double precision, save :: eps
      real*8 pb_tmp(0:3,nlegborn)
      real*8 pb_fs_cm(0:3,nlegs),pb_fs_lab(0:3,nlegs), ! these should actually go to nlegs+1, but this entry is not needed...
     %          pr_fs(0:3,nlegreal) ! this needs the maximal size due to the routine it is passed to, also entries will be filled only up to nlegs+2
      integer bnlegs_fs,rnlegs_fs,bflav_fs(nlegborn),rflav_fs(nlegreal)
      logical nores
      save nores
      logical coupleW
      ! DEBUG
      logical makecuts, cuts_passed, impose_born_cut
      real*8 dotp
      external dotp
      
      integer born_cuts
      real*8 pTg_UUB_cut
      common/uub_photon_cuts/born_cuts, pTg_UUB_cut

      integer r,s
      logical flg_profiledscales,flg_largeptscales,flg_rescaleQ0
      save flg_profiledscales,flg_largeptscales,flg_rescaleQ0
      real *8 kappaq
      common/common_kappaQ/kappaq
      double precision, save :: modlog_p_sav = -1

      integer pstage
      logical, save ::  kill_D3_sav=.false.,kill_H2_sav=.false.
      logical kill_D3,kill_H2
      common/common_kill/kill_D3,kill_H2
      integer, save :: run_mode
      logical, save :: verbose = .false.

      ! If flg_rwl_add is true, the variables to reweight for
      ! need to be set according to the reweighting infos, not 
      ! to the input (even at the very first call)
      logical rwl_modlog_p, rwl_kappaq, rwl_run_mode, recompute_sudakov, recompute_virtuals
      common/minnlo_rwl_add/rwl_modlog_p, rwl_kappaq, rwl_run_mode, recompute_sudakov, recompute_virtuals

      cuts_passed = .true.

      if(kn_jacborn.eq.0d0
     1     .or. br_born(iuborn).eq.0d0) then
         rescfac=0d0
         return
      endif

c     the only purpose of this is to invalidate the currently stored results
c     of setlocalscales (see the line  if(imode.eq.oimode ...))
      if(imode .eq. -1) then
         oimode = -1
         return
      endif

      if(ini) then

         flg_smartMiNLO=.true.
         if(powheginput("#smartMiNLO").eq.0) flg_smartMiNLO=.false.
         if(flg_smartMiNLO)then
            write(*,*)
            write(*,*) " !!!!!!!!!!!!!!!!!!!!!!! "
            write(*,*) " !!   smartMiNLO on   !! "
            write(*,*) " !!!!!!!!!!!!!!!!!!!!!!! "
            write(*,*)
         endif

         ! WARNING: assumption is that number of resonances at born level is
         ! the same for all flst_born strings
         nores=powheginput('#nores').eq.1

         ! DL set nflav to st_nlight
         nflav=st_nlight
         
         if(allocated(msqb)) deallocate(msqb)
         if(allocated(msqV1)) deallocate(msqV1)
         if(allocated(msqV2)) deallocate(msqV2)
         if(allocated(msqB_sav)) deallocate(msqB_sav)
         if(allocated(msqV1_sav)) deallocate(msqV1_sav)
         if(allocated(msqV2_sav)) deallocate(msqV2_sav)
         if(allocated(Delta2)) deallocate(Delta2)
         if(allocated(sudakov_form_factor))deallocate(sudakov_form_factor)
         allocate(msqb(-nflav:nflav,-nflav:nflav))
         allocate(msqV1(-nflav:nflav,-nflav:nflav))
         allocate(msqV2(-nflav:nflav,-nflav:nflav))
         allocate(msqB_sav(-nflav:nflav,-nflav:nflav))
         allocate(msqV1_sav(-nflav:nflav,-nflav:nflav))
         allocate(msqV2_sav(-nflav:nflav,-nflav:nflav))
         allocate(Delta2(-nflav:nflav,-nflav:nflav))
         allocate(sudakov_form_factor(-nflav:nflav,-nflav:nflav))

c     set the number of flavour used in the Dterms
         call set_nflav(nflav)
c     ... and do the same for the cache system
         call cache_init(nflav)
         
c     this to make sure that at the line   if (.. abs(kappar-kappar_sav) > 1d-7 )
c     the block is entered on the first call
         mb2_sav=-1d0
         kappar_sav=-1d0
         kappaf_sav=-1d0
         kappaq_sav=-1d0
         st_alpha_sav = -1d99

         bmass_in_minlo_flg = powheginput("#bmass_in_minlo").eq.1

         b0=(33d0-2d0*st_nlight)/(12*pi)
         b1=(153d0-19d0*st_nlight)/(24*pi**2)
         ini = .false.

c     read the modified logarithms parameter p, if it's not found, set it to its default (p=6)
         if(.not.rwl_modlog_p)then
            modlog_p = powheginput("#modlog_p")
            if(modlog_p < -100d0) modlog_p = -1d0 !>> use new modified logs by default
            write(*,*) '============================================='
            write(*,*) "using modlog_p = ", modlog_p
            write(*,*) '============================================='
         endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
c     The conditions below should ultimately be replaced by a condition on pt/M
         call set_alphas_pdf_cutoff_fact(iuborn,alphas_cutoff_fact,flav)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         flg_use_numerical_sudakov=.true.
         if(powheginput('#numeric_sudakov').eq.0)
     $       flg_use_numerical_sudakov=.false.
         if(flg_use_numerical_sudakov)then
            write(*,*) ""
            write(*,*) " Using NUMERICAL SUDAKOV ... "
            write(*,*) ""
         else
            write(*,*) ""
            write(*,*) " Using ANALYTIC SUDAKOV ... "
            write(*,*) ""
         endif
c     (process dependent) precision on the colourless system mass difference
c     to decide if events are identical
         if(flav .eq. 0) then
            eps = 1d-3
         elseif(flav .eq. 1) then
            eps = 1d-7
         endif
         
         if (flg_minnlo) then
            write(*,*)
            write(*,*) '========================='
            write(*,*) '   *MiNNLO* ACTIVATED    '
            write(*,*) '========================='
            write(*,*)
!     set the MiNNLO flags
            flg_include_delta_terms = .false.
            if(powheginput('#inc_delta_terms').eq.1)
     $           flg_include_delta_terms=.true.
            write(*,*) "include_delta_terms is ",flg_include_delta_terms

            flg_distribute_by_ub = .true.
            if(powheginput('#distribute_by_ub').eq.0)
     $           flg_distribute_by_ub = .false.
            write(*,*) "distribute_by_ub is ",flg_distribute_by_ub

            flg_distribute_by_ub_AP = .true.
            if(powheginput('#distribute_by_ub_AP').eq.0)
     $           flg_distribute_by_ub_AP = .false.
            write(*,*) "distribute_by_ub_AP is ",flg_distribute_by_ub_AP
            flg_dtermsallorders = .true.                                            
            if(powheginput('#d3allorders').eq.0)                                    
     $           flg_dtermsallorders = .false.                                      
            write(*,*) '============================================='              
            write(*,*) "flg_dtermsallorders = ",flg_dtermsallorders                 
            write(*,*) '=============================================' 
         else
            write(*,*)
            write(*,*) '========================================='
            write(*,*) '  MINLO ACTIVATED (BUT *MINNLO* IS OFF)  '
            write(*,*) '========================================='
            write(*,*)
         endif

         if(.not.rwl_kappaq)then
            kappaq = powheginput('#kappaQ')
            if(kappaq.lt.0d0) kappaq = 1d0
            write(*,*) '============================================='
            write(*,*) "using kappaQ = ",kappaq
            write(*,*) '============================================='
         endif

         ! For processes not involving a final state photon, born_cuts
         ! should be 0 (in this case pTg_UUB_cut is not used). If neeed,
         ! the subroutine init_process_dependent, called by init_setlocalscales
         ! will initialize these values accordingly 
         born_cuts = 0
    
         call init_setlocalscales(flav,flg_minnlo,kappaq)  
   

c Setup profiled scales (main switch and parameters)    
         flg_profiledscales=.true.
         if(powheginput('#profiledscales').eq.0) flg_profiledscales=.false.
         Q0_ini = powheginput('#Q0')
         if(Q0_ini.lt.0d0) Q0_ini = 0d0
         if(Q0_ini.eq.0d0) Q0_ini = 1d-10
         npow = powheginput('#npow')
         if(npow.lt.0d0) npow = 1d0              
         call init_profiled_scales_parameters(flg_profiledscales,Q0_ini,npow)
         !>> flag that decides whether Q0 is rescaled by kappaf
         flg_rescaleQ0 = powheginput('#rescaleQ0').eq.1

         if(flg_profiledscales) then
            write(*,*) '============================================='
            write(*,*) 'Using profiled scales: (Q0,npow) = ',Q0_ini,npow
            write(*,*) '============================================='
         endif

c     Option to use mur and muf = pt in fixed order (Bbar) at large pt
         flg_largeptscales = .true.
         if(powheginput('#largeptscales').eq.0) flg_largeptscales = .false.
         if(flg_largeptscales) then
            write(*,*) '============================================='
            write(*,*) 'Using mu=pt in fixed order'
            write(*,*) '============================================='
         endif
         
         if(flg_minnlo.and..not.rwl_run_mode)then
            run_mode=powheginput("#run_mode")
            if(run_mode.gt.1)then
               write(*,*)""
               write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
               write(*,*)"        MiNNLO_PS optimization on:"
               if(run_mode.eq.2)then
                  ! Basically a minlo run
                  kill_D3 = .true.
                  kill_H2 = .true.
                  write(*,*)" D3terms are set to zero during whole run "
                  write(*,*)" in run_mode",run_mode
               else if(run_mode.eq.3)then
                  write(*,*)" the 2-loop contribution for the D3tems    "
                  write(*,*)" will be set to zero during the computation"
                  write(*,*)" of the integration grid and switched on"
                  write(*,*)" afterwards in run_mode",run_mode
               else if(run_mode.eq.4)then
                  kill_D3 = .false.
                  kill_H2 = .true.
                  write(*,*)" 2-loop will be included only through "
                  write(*,*)" the stage 4 on-the-fly reweighting "
                  write(*,*)" in run_mode",run_mode
               else
                  write(*,*) "Unknown running mode!!!"
                  write(*,*) "run_mode",run_mode
                  stop
               endif
               write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
               write(*,*)""
            else
               run_mode=1
               write(*,*)""
               write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
               write(*,*)"        MiNNLO_PS optimization off          "
               write(*,*)" the 2-loop contribution for the D3tems"
               write(*,*)" included in standard way in run_mode",run_mode
               write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
               write(*,*)""
               ! Standard running mode 
               kill_D3 = .false.
               kill_H2 = .false.
            endif
         else
            run_mode=1
         endif
      endif

      rescfac = 1
      if((uubornonlyD1.or.uubornonlyD3)
     &    .and.imode.ne.3)then 
         ! If we are performing uubornonly test, we are not interested in 
         ! getting the Sudakov form factor out of imode .eq. 3
         rescfac=1
         return
      endif

c Select momentum array without the resonance
      if(.not.nores)then
         call get_fs_particles_momenta(flst_bornlength(iuborn),
     $        flst_born(:,iuborn),flst_bornres(:,iuborn),kn_cmpborn,
     $        bnlegs_fs,bflav_fs,pb_tmp)
         pb_fs_cm(:,1:nlegs)=pb_tmp(:,1:nlegs)
         call get_fs_particles_momenta(flst_bornlength(iuborn),
     $        flst_born(:,iuborn),flst_bornres(:,iuborn),kn_pborn,
     $        bnlegs_fs,bflav_fs,pb_tmp)
         pb_fs_lab(:,1:nlegs)=pb_tmp(:,1:nlegs)
         if(flg_minlo_real) then
            ! flst_born2alr(0,iuborn) = number of singular reagions sharing underlying born iuborn              
            ! flst_born2alr(1:flst_born2alr(0,iuborn),iuborn) = list of these singular regions                
            ! flst_alr(1:nlegreal,flst_born2alr(i,iuborn)) list, by varying i from 1 to flst_born2alr(0,iuborn)        
            !    of the real flavour strings sharing that underlying born. We can pick up just the first 1,             
            !    but this ensure the resonance structure is the same of the underlying born -> this will              
            !    allow us to correctly discard the resonance momenta                                                 
            call get_fs_particles_momenta(flst_alrlength(flst_born2alr(1,iuborn)),                                
     $           flst_alr(:,flst_born2alr(1,iuborn)),flst_alrres(:,flst_born2alr(1,iuborn)),kn_cmpreal,                   
     $           rnlegs_fs,rflav_fs,pr_fs)
         endif
      else
         pb_fs_cm(:,1:nlegs)=kn_cmpborn(:,1:nlegs)
         pb_fs_lab(:,1:nlegs)=kn_pborn(:,1:nlegs)
         if(flg_minlo_real) then
            pr_fs(:,1:nlegs+1)=kn_cmpreal(:,1:nlegs+1)
         endif
         bflav_fs(:)=flst_born(:,iuborn)
      endif


c     pb(0:3) is the colourless "boson" momentum
      pb(:)=0d0
c     sum over colourless particles (they must all come from a single boson decay) 
      do i=3,nlegs
c     the sequence of colourless particles is unchanged in the Born and in the real flavour list
         if (abs(bflav_fs(i)).gt.6) then 
            if(flg_minlo_real) then
               pb(:)=pb(:) + pr_fs(:,i)
            else
               pb(:)=pb(:) + pb_fs_cm(:,i)
            endif
         endif
      enddo

      ptb2 = pb(1)**2 + pb(2)**2 ! transverse momentum squared
      dbg_ptb=sqrt(ptb2)
      mb2  = pb(0)**2 - pb(3)**2 - ptb2 ! invariant mass squared
      dbg_mb=sqrt(mb2)

      if(flav.eq.0)then
         uub_flav(:)=0
      else
         call get_uub_flav(flst_born(:,iuborn),uub_flav(:)) ! incoming quark flavours of UB
      endif

C     this call arranges for the A and B resummation coefficients to be set
C     as well as the virtual corrections
      kappar = st_renfact
      kappaf = st_facfact

      if(run_mode.eq.3)then
         ! Check POWHEG stage
         if(powheginput('#parallelstage').gt.0)then
            pstage = powheginput('#parallelstage')
         else
            if (flg_ingen) then
               pstage = 4
            else
               pstage = mintw_current_stage + 1
            endif
         endif
         ! Skip 2loop computation at stage 1
         ! when setting up integration grids
         kill_D3 = .false.
         kill_H2 = pstage .eq. 1
      endif

c     Cache system both for sudakov form factor and D3terms
      if(flg_smartMiNLO)then
         if(imode.eq.oimode.and.ptb2.eq.optb2.and.mb2.eq.mb2_sav
     &        .and.kappaf.eq.kappaf_sav.and.kappar.eq.kappar_sav
     &        .and.kappaq.eq.kappaq_sav
     &        .and.modlog_p.eq.modlog_p_sav
     &        .and.(kill_H2.eqv.kill_H2_sav).and.(kill_D3.eqv.kill_D3_sav)
     &        .and..not.recompute_sudakov.and..not.recompute_virtuals)then
            st_mufact2 = omuf2
            if(imode==3) then
               d3terms = od3terms
               return
            endif
            if( check_cache(uub_flav(1),uub_flav(2)) )then
               ! If the result has already been stored in the cache, use it!
               rescfac = get_cache(uub_flav(1),uub_flav(2))               
               return
            endif
         else 
            ! New kinematic configuration: reset cache to zero and recompute 
            ! flavour indipendent pieces in the sudakov_form_factor 
            ! subroutine (just once): when sudakov_cache=.false., the stored 
            ! pieces in this subroutine are combined with the new flavour channel
            ! dependent B(2) term 
            sudakov_cache = .true.
            call clean_cache
            optb2 = ptb2
            oimode = imode
            kill_D3_sav = kill_D3
            modlog_p_sav = modlog_p
            recompute_sudakov = .false.
         endif
      endif

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!!      If born_cuts is set, apply small generation cuts
      !!!!!      on uub phase space to remove QED singularities
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      pt = sqrt(ptb2)
      gamma_UUB_damping = 1d0
      if(imode.eq.3 .or. flg_diboson) then
         ! * MiNNLOPS D3terms * / * MiNLO DIBOSON *
         call invISRmap(pb_fs_lab,nlegs,pborn_UUB)
         makecuts=.false.
         if((dbg_ME.and.dbg_phspproj).or.
     &      (uubornonlyD3.and.dbg_phspproj))then
            call uub_phasespace_check(pborn_UUB,makecuts)
            if(makecuts)then
               d3terms=0d0
               if(flg_smartMiNLO) od3terms=d3terms
               return
            endif
         endif
         if (born_cuts.eq.1) then
            call cuts_on_uuborn(pborn_UUB,pb_fs_lab,pTg_UUB_cut,cuts_passed)
            if (.not. cuts_passed) then 
               gamma_UUB_damping = 0d0
               if(imode .eq. 3) then
                  d3terms=0d0
                  if(flg_smartMiNLO) od3terms=d3terms
                  return
               endif
            endif
         elseif (born_cuts.eq.2) then
            call cuts_on_uuborn(pborn_UUB,pb_fs_lab,pTg_UUB_cut,cuts_passed)
            if (.not.cuts_passed .and. impose_born_cut(pb_fs_lab)) then
               cuts_passed = .false.
               gamma_UUB_damping = 0d0
               if(imode.eq.3) then
                  d3terms=0d0
                  if(flg_smartMiNLO) od3terms=d3terms
                  return
               endif
            else
               cuts_passed = .true.                  
            endif
         elseif (born_cuts.eq.3) then
            call cuts_on_uuborn(pborn_UUB,pb_fs_lab,pTg_UUB_cut,cuts_passed)
            if (.not.cuts_passed .and. impose_born_cut(pb_fs_lab)) then
               cuts_passed = .false.
               pTg2 = pborn_UUB(1,5)**2+pborn_UUB(2,5)**2.
               gamma_UUB_damping = 2d0*pTg2/(pTg2+pTg_UUB_cut**2)
            else
               cuts_passed = .true.                  
            endif
         endif
      endif

      ! Reset all matrix elements to zero
      msqB=0d0
      msqV1=0d0
      msqV2=0d0

      if(imode .eq. 3) then
         e2 =  kn_sbeams
         e = sqrt(e2)
         ebeam = e/2d0
         xx1 = pborn_UUB(0,1)/ebeam
         xx2 = pborn_UUB(0,2)/ebeam   
         ! * MiNNLOPS D3terms * 
         if (xx1.ne.xx1_sav.or.xx2.ne.xx2_sav.or.(kill_H2.neqv.kill_H2_sav).or.recompute_virtuals.or.st_alpha.ne.st_alpha_sav) then
            if(.not. kill_H2) then
               call get_B_V1_V2(pborn_UUB,msqB,msqV1,msqV2=msqV2,nflav=nflav)
            else
               call get_B_V1_V2(pborn_UUB,msqB,msqV1,nflav=nflav)
            endif
            xx1_sav = xx1
            xx2_sav = xx2
            msqB_sav  = msqB
            msqV1_sav = msqV1
            msqV2_sav = msqV2
            kill_H2_sav = kill_H2
            recompute_virtuals = .false.
            st_alpha_sav = st_alpha
         else
            msqB  = msqB_sav
            msqV1 = msqV1_sav
            msqV2 = msqV2_sav
         endif         
      elseif(flg_diboson)then
         ! * MiNLO DIBOSON *
         ! If we do not need to compute d3terms (imode 3), but 
         ! we are in diboson case, B2 is flavour channel dependent
         ! and so we need the virtual/born ratio for the current flavour
         ! also for MiNLO
         call get_B_V1_V2(pborn_UUB,msqB,msqV1,nflav=nflav)
         ! If imode is not 3, kill_H2 irrelevant. but the kill_H2_sav needs
         ! to be set, because this value is used to probe the caching system
         ! above, regardless of the imode
         if(flg_rwl_add) kill_H2_sav = kill_H2
      endif
      
     
      !>> update Q0 scale according to KF (comment to avoid rescaling)
      if (flg_rescaleQ0) call reset_profiled_scales_parameters(Q0_ini/kappaf)
      
      if (abs(mb2-mb2_sav) > 0 .or. abs(kappar-kappar_sav) > 0
     1     .or. abs(kappaf-kappaf_sav) > 0
     2     .or. abs(kappaq-kappaq_sav) > 0) then
         call reset_dynamical_parameters(dsqrt(mb2), kappar, kappaf, kappaq)
         call init_anom_dim
         mb2_sav = mb2
         kappar_sav = kappar
         kappaf_sav = kappaf
         kappaq_sav = kappaq
      endif
      


c     Here we compute the modified log with KR=1 as the KR dependence is
c     explicitly accounted for in the computation of the D3 terms
      call log_and_jacob(pt,cs%Q,L,jacob)

      if(uubornonlyD1)goto 113

      if(imode .eq. 3) then
         ! * MiNNLOPS D3terms * 
         call virtual_scale_dep(msqB, msqV1, msqV2)
      elseif(flg_diboson)then
         ! * MiNLO DIBOSON *
         ! If imode is not 3 but we are in diboson case, the B(2)
         ! will be flavour channel dependent
         call virtual_scale_dep(msqB, msqV1)
      endif
      


      !!!!!!!!!!!!!!!!!!!!!!!
      ! COMPUTE THE SUDAKOV !
      !!!!!!!!!!!!!!!!!!!!!!!
      ! Reset all matrix elements to zero
      sudakov_form_factor=0d0
      Delta2=0d0
c     alpha_s reweighting
      !mu2=ptb2*st_renfact**2
      !st_mufact2 = ptb2*st_facfact**2
      !>> ER+PM: set all scales using the modified logarithms
      if(flg_profiledscales) then
        ! scale entering the alphas passed to the dterms
         mu2dterms  = st_renfact**2/kappaq**2 * (kappaq*sqrt(mb2) * exp(-L) + Q0 / (1d0 +
     $        (kappaq*sqrt(mb2)/Q0*exp(-L))**npow))**2
        ! scale entering pdfs of the fixed order
         mu2fact = st_facfact**2/kappaq**2 * (kappaq*sqrt(mb2) * exp(-L) + Q0 / (1d0 +
     $        (kappaq*sqrt(mb2)/Q0*exp(-L))**npow))**2
      else
         mu2dterms  = st_renfact**2*mb2*exp(-2d0*L)
         mu2fact    = st_facfact**2*mb2*exp(-2d0*L)
      endif

      if(flg_largeptscales) then
         ! those scales only enter the fixed order
         if(flg_profiledscales) then
            ! this now has a kappaq dependence at large pT --> not desirable
            mu2     = st_renfact**2/kappaq**2 * (sqrt(ptb2) + Q0 / (1d0 +
     $           (sqrt(ptb2)/Q0 )**npow))**2
            mu2fact = st_facfact**2/kappaq**2 * (sqrt(ptb2) + Q0 / (1d0 +
     $           (sqrt(ptb2)/Q0 )**npow))**2
         else
            ! this now has a kappaq dependence at large pT --> not desirable
            mu2     = st_renfact**2/kappaq**2 * ptb2
            mu2fact = st_facfact**2/kappaq**2 * ptb2
         endif        
      else
c     This is needed when running with modified logs in the fixed-order
c     (Bbar) part
         mu2 = mu2dterms
      endif

      st_mufact2 = mu2fact
      omuf2=st_mufact2
      if(pdf_alphas_from_pdf) then
         !alphas = pwhg_alphas(st_renfact**2*ptb2,st_lambda5MSB,st_nlight)
         !if(st_renfact**2*ptb2.lt.alphas_cutoff_fact**2 *pdf_q2min) then
         !   alphas = pwhg_alphas(alphas_cutoff_fact**2 *pdf_q2min,st_lambda5MSB,st_nlight)
         !endif
         !>> ER+PM: set the scale of the coupling using the modified logarithms
         if (use_analytic_alphas) then
            if(flg_largeptscales) then
               write(*,*)
     $  'flg_largeptscales is not supported with use_analytic_alphas'
               stop
            endif
            if(flg_profiledscales) then
               lambda=st_alpha*b0*log(sqrt(mb2) / (sqrt(mb2)*exp(-L) +
     $              Q0 / (1d0 + (sqrt(mb2)/Q0*exp(-L))**npow)))
            else
               lambda = st_alpha*b0*L
            endif
            alphas = st_alpha / (1-2d0*lambda) * (1d0 
     &           - st_alpha / (1-2d0*lambda) * b1/b0 * log(1d0-2d0*lambda))
         else
!            alphas = pwhg_alphas(st_renfact**2*mb2*exp(-2d0*L),st_lambda5MSB,st_nlight)
!     GZ 
            alphas = pwhg_alphas(mu2,st_lambda5MSB,st_nlight)
            alphasdterms = pwhg_alphas(mu2dterms,st_lambda5MSB,st_nlight)
            if(mu2.lt.alphas_cutoff_fact**2 *pdf_q2min.and. (.not.flg_profiledscales)) then
                 alphas = pwhg_alphas(alphas_cutoff_fact**2 *pdf_q2min,st_lambda5MSB,st_nlight)
c              if mu2 << Q^2, we assume that mu2dterms=mu2 (at small pt, they essentially coincide)
                 alphasdterms = alphas
            endif
         end if  
      else
         write(*,*) "ERROR: pdf_alphas_from_pdf=false ",
     c        "not supported for minnlops"
         call exit(-1)
      endif


      do i=-nflav,nflav
         do j=-nflav,nflav
            if(flav.ne.0.and.i.eq.0) cycle
            if(flav.eq.0.and.i.ne.0.and.j.ne.0) cycle
            if(flg_minnloproc.eq.'WZ'.and..not.coupleW(i,j))then
               cycle
            elseif(flg_minnloproc.ne.'WZ'.and.j.ne.-i)then
               cycle
            endif
            if(flg_smartMiNLO.and.check_cache(i,j)) cycle

            ! Update the B(2), which is generally kinematic and flavour dependent
            ! -> this is needed also for imode 3
            if(flg_diboson)then
               if(msqB(i,j).ne.0d0) then
                  virt_over_born=msqV1(i,j)/msqB(i,j) * gamma_UUB_damping
               else
                  if(msqV1(i,j).eq.0d0) then
                     virt_over_born=0d0
                  else
                     write(*,*) " WARNING: msqB=zero, but msqV1 not. This should not happen."
                  endif
               endif
               if(abs(virt_over_born).gt.100d0) then
                  rescfac=sqrt(-abs(virt_over_born)) ! set rescfac to NaN if B2 is too large
                  d3terms=rescfac
                  if(flg_smartMiNLO) then
                     od3terms=d3terms
                     call update_cache(rescfac)
                  endif
                  if(verbose)then
                     write(*,*) ""
                     write(*,*) " WARNING: abs(virt_over_born).gt.100d0:"
                     write(*,*) i,j,"ratio",abs(virt_over_born)
                     write(*,*) " setting phase-space point to zero"
                  endif
                  return
               endif

               ! Update B2 piece, which is flavour dependent
               if(imode .eq. 3) then
                  ! For MiNNLOPS, also update msqV2, which contains a B2 dependent piece
                  ! when the resummation scale is used
                  virt2_over_born=msqV2(i,j)/msqB(i,j)
                  call update_b2_proc_dep(i,j,virt_over_born,virt2_over_born)
                  msqV2(i,j)=virt2_over_born*msqB(i,j)
                  call set_equivalent_flavour(msqV2,nflav,msqV2(i,j),i,j)
               elseif(flg_diboson)then
                  ! MiNLO DIBOSON: just update the B2 term
                  call update_b2_proc_dep(i,j,virt_over_born)
               endif
            endif
            

            if(flg_use_numerical_sudakov)then
               sudakov_form_factor(i,j) = Sudakov_pt_exact(L, alphas_cutoff_fact*sqrt(pdf_q2min), use_analytic_alphas)
            else
               sudakov_form_factor(i,j) = Sudakov_pt(L, alphas_in = pwhg_alphas(st_renfact**2*mb2,st_lambda5MSB,st_nlight))
            endif
            
            if(flg_smartMiNLO) call set_equivalent_flavour(sudakov_form_factor,nflav,sudakov_form_factor(i,j),i,j)
            Delta1  = expsudakov_pt(L)  * alphasdterms
            Delta2(i,j)  = exp2sudakov_pt(L) * alphasdterms**2 ! B2 dependence here
            if(flg_smartMiNLO) call set_equivalent_flavour(Delta2,nflav,Delta2(i,j),i,j)
            rescfac = sudakov_form_factor(i,j)
            if(imode.eq.2) then
               rescfac = rescfac * (alphas/st_alpha)**2
               if (flg_include_delta_terms.and.flg_minnlo) rescfac =
     1              rescfac * (1d0 + Delta1)
            elseif(imode .eq. 1) then
               rescfac = rescfac * (alphas/st_alpha)
               if(.not.flg_bornonly) then
                  if (.not. flg_minnlo) then 
                     rescfac = rescfac * 
     1                    (1 + Delta1 + alphas*b0*log(mu2/st_muren2))
                  else
                     if (flg_include_delta_terms) then
                        rescfac = rescfac *
     1                       (1d0 + (Delta1+alphas*b0*log(mu2/st_muren2) * (1d0 + Delta1))
     2                       + (Delta1**2/2d0+Delta2(i,j)))
                     else
                        rescfac = rescfac * (1d0 + (Delta1+alphas*b0*log(mu2/st_muren2)))
                     end if
                  endif
               endif
            endif

            if(imode.eq.3 .and. kill_D3) then
               d3terms = 0d0
               if(flg_smartMiNLO) od3terms=d3terms
               return
            endif
            
            if(bmass_in_minlo_flg) then
               call bmass_in_minlo(bfact,alphas)
               rescfac = rescfac * bfact
            endif
            

            if(flg_diboson)then
               call update_cache(rescfac,i,j)
            else
               ! If we do not have flavour dependent B2, both for DY and H
               ! all of the contributions are the same
               call update_cache(rescfac)
               sudakov_form_factor(:,:)=sudakov_form_factor(i,j)
               Delta2(:,:)=Delta2(i,j)
               exit
            endif

            ! To speed up the computation of the sudakov in Sudakov_pt: after the
            ! the first loop iteration, only the B(2) dependent parts are recomputed
            if(flg_smartMiNLO) sudakov_cache=.false.
         enddo
      enddo
      rescfac = get_cache(uub_flav(1),uub_flav(2))               

 113  continue
      if(uubornonlyD1)then
      ! dummy initialization
         rescfac=1d0 
         jacob=1d0
         sudakov_form_factor=1d0
         Delta1=0d0
         Delta2=0d0
      endif
      
      !!!!!!!!!!!!!!!!!!!!!!!
      ! COMPUTE THE D3TERMS !
      !!!!!!!!!!!!!!!!!!!!!!!
      if (flg_minnlo .and. imode .eq. 3 ) then
         call reset_dynamical_C_matxs()
         msqB(:,:)=msqB(:,:)*sudakov_form_factor(:,:)
         msqV1(:,:)=msqV1(:,:)*sudakov_form_factor(:,:)
         msqV2(:,:)=msqV2(:,:)*sudakov_form_factor(:,:)
         if(flg_dtermsallorders) then
            call DtermsAllOrders(D1, D2, D3, exp(-L), xx1, xx2, msqB, msqV1, msqV2, alphas_in=alphasdterms)
         else
            call Dterms(D1, D2, D3, exp(-L), xx1, xx2, msqB, msqV1, msqV2, alphas_in=alphasdterms)
         endif
         if (flg_include_delta_terms) then
            call D1xDelta2(Delta2,D1_times_Delta2, exp(-L), xx1, xx2, msqB, alphas_in=alphasdterms)
            d3terms = D3 - D2 * Delta1 + D1 * Delta1**2/2d0 - D1_times_Delta2
         else
            d3terms = D3
         end if
         d3terms = d3terms * jacob * gamma_UUB_damping

         ! to run uubornonly for LO
         if(uubornonlyD1)then
            ! This test should reproduce the 0-jet LO cross section
            d3terms = D1
         endif
         
         if(flg_smartMiNLO) od3terms = d3terms
      endif

      end


      subroutine set_alphas_pdf_cutoff_fact(iuborn,alphas_cutoff_fact,flav)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_pdf.h'
      include 'pwhg_flst.h'
      include 'minnlo_flg.h'
      double precision alphas_cutoff_fact
      integer iuborn,fl,k,flav
      integer count_H,count_W,count_Z
      real*8 powheginput
      external powheginput
c     alphas_cutoff_fact**2 * pdf_q2min is the scale (in GeV^2) at which we freeze the running of alphas
c     pdf_q2min is the LHAPDF cutoff (e.g. for NNPDF30 it's 1GeV^2)
      if (.not. flg_hoppet_initialized)
     $     pdf_cutoff_fact = powheginput('#pdf_cutoff_fact')

      count_H=0
      count_W=0
      count_Z=0
      
      do k=3,nlegborn
         fl=abs(flst_born(k,iuborn))
         if(fl.eq.25)then
            count_H=count_H+1
         elseif(fl.eq.24)then
            count_W=count_W+1
         elseif(fl.eq.23)then
            count_Z=count_Z+1
         endif
      enddo

      flav=1 ! qqb initialized process
      if(count_H.ge.1.and.count_W.eq.0.and.count_Z.eq.0)then ! single (or double) Higgs production
         flav=0 ! gg initialized process
      endif
      ! MW: this way of determining flav is very incomplete and incorrect for many processes (e.g. bb->H)!

      alphas_cutoff_fact = powheginput('#alphas_cutoff_fact')
      if(alphas_cutoff_fact.lt.0d0) alphas_cutoff_fact=0d0

      write(*,*) 'freezing of alphas in setlocalscales at [GeV] '
     $     ,sqrt(alphas_cutoff_fact**2 *pdf_q2min)

      write(*,*)'freezing of PDF evolution in '
     $     ,'setlocalscales at [GeV]'
     $     ,sqrt(pdf_cutoff_fact**2 *pdf_q2min)
      
      end subroutine



      subroutine init_setlocalscales(flav,minnlo,kappaq)
      use rad_tools
      use pdfs_tools
      use internal_parameters, pi_hoppet => pi, cf_hoppet => cf, ca_hoppet => ca, tf_hoppet => tf
      implicit none
      include 'brinclude.h'
!      include 'nlegborn.h'
      include 'PhysPars.h'
      include 'pwhg_pdf.h'
      include 'pwhg_kn.h'
      include 'pwhg_st.h'
      include 'minnlo_flg.h'
      double precision D1, D2, D3, xx1, xx2, get_M_for_init_Dterms
      integer pdf_set,flav
      character*100 pdf_name
      character*100 string
      integer stringlength
      real *8 kappar,kappaf,kappaq
      logical ini,minnlo
      data ini/.true./
      save ini
      real *8 powheginput
      logical check_pdf
      parameter (check_pdf = .false.)

      if(minnlo.or.flg_diboson)then
         call init_process_dependent()
      endif

      if(minnlo)then
         call init_amplitude()  ! used for ampliude initialization of external libraries such as MATRIX
         ! initialise pdf (already done in hoppetif.f, see there)
         string = " "
         call lhapdfname(pdf_ndns1,string,pdf_set)
         pdf_name=trim(string(1:stringlength(string)-1))//trim(".LHgrid")
         ! set the global mass of the colour singlet (this has to happen before the pdf initialisation)
         cs%M = get_M_for_init_Dterms()
         cs%Q = kappaq*get_M_for_init_Dterms()
         if(.not. flg_hoppet_initialized) then
            if(flg_use_NNLOPS_pdfs) then
               call init_pdfs_NNLOPS(pdf_name, pdf_set, cutoff_high_fact=pdf_cutoff_fact)
            else
               call init_pdfs_from_LHAPDF(pdf_name, pdf_set, pdf_cutoff_fact)
            endif         
         endif
         
         if(check_pdf) call checkpdf

      else
         cs%M = get_M_for_init_Dterms()
         cs%Q = kappaq*get_M_for_init_Dterms()
         call qcd_setNf(st_nlight)
      endif
      
      kappar=st_renfact
      kappaf=st_facfact

      if (flav.eq.0) then ! gg-initiated
         call set_process_and_parameters('pp', 'gg', sqrt(kn_sbeams), get_M_for_init_Dterms(), kappar, kappaf, kappaq) ! M, KR, KF, KQ
      else ! qqbar-initiated
         call set_process_and_parameters('pp', 'qq', sqrt(kn_sbeams), get_M_for_init_Dterms(), kappar, kappaf, kappaq) ! M, KR, KF, KQ
      endif

c     initialise anomalous dimensions      
      call init_anom_dim
      if(minnlo)then
         ! initialise coefficient functions
         call init_C_matxs()
      endif

      end

      subroutine log_and_jacob(pt,M,L,jacob)
      include 'minnlo_flg.h'
      double precision pt, M, L, jacob
      double precision a, b, c, d, g

      if (modlog_p > 0d0) then
         !>> standard modified log
         L     = 1d0/modlog_p*log((M/pt)**modlog_p + 1d0)
         jacob = 1d0/pt*(M/pt)**modlog_p/(1d0 + (M/pt)**modlog_p) ! dL/dpt
      else if (modlog_p .eq. -1d0) then
         !>> piecewise modified log
         if ((M .ge. pt).and.(pt .ge. M/2d0)) then
            a = 5d0
            b = -8d0/M
            c = 4d0/M**2
            g  = a + b*pt + c*pt**2
            L     = log(g)
            jacob = abs((-8*(M - pt))/(5*M**2 - 8*M*pt + 4*pt**2))
         else if (pt .ge. M) then
            L     = 0d0
            jacob = 0d0
         else
            L     = log(M/pt)
            jacob = 1d0/pt
         end if
      else if (modlog_p .eq. -2d0) then
         !>> piecewise modified log
         if ((M .ge. pt).and.(pt .ge. M/2d0)) then
            a = 4d0*(1d0-log(2d0))
            b = 8d0*(-2d0+3d0*log(2d0))/M
            c = 4d0*(5d0-9d0*log(2d0))/M**2
            d = 8d0*(-1d0+2d0*log(2d0))/M**3
            g  = a + b*pt + c*pt**2 + d*pt**3
            L     = g
            jacob = b + 2d0*c*pt + 3d0*d*pt**2
         else if (pt .ge. M) then
            L     = 0d0
            jacob = 0d0
         else
            L     = log(M/pt)
            jacob = 1d0/pt
         end if
      else if (modlog_p .eq. 0d0) then
         !>> theta function with unmodified log
         L     = 0d0
         jacob = 0d0
         if (pt <= M) then
            L     = log(M/pt)
            jacob = 1d0/pt
         end if
      else
         write(*,*) "ERROR: log_and_jacob, unrecognized modlog_p"
         call exit(-1)
      end if
      end

      subroutine sudakov_exponent
      write(*,*) ' sudakov_exponent should not be called here'
      call exit(-1)
      end
      

      subroutine checkpdf
      include 'pwhg_st.h'
      include 'pwhg_pdf.h'
      real *8 xxx(1:4),muF,pdf1(-pdf_nparton:pdf_nparton)
      integer i

      write(*,*) '==============================================='
      write(*,*) 'LHAPDF'
      write(*,*) '==============================================='

      xxx(1)=0.001d0
      xxx(2)=0.01d0
      xxx(3)=0.05d0
      xxx(4)=0.1d0

      muF=0.75d0
      st_mufact2 = muF**2
      write(*,*) 'muF = ',muF
      do i=1,4
         call pdfcall(1,xxx(i),pdf1)
         write(*,*) 'x,g,d,u,c,b -> ',xxx(i),pdf1(0),pdf1(1),pdf1(2),pdf1(4),pdf1(5)
      enddo

      muF=1.d0
      st_mufact2 = muF**2
      write(*,*) 'muF = ',muF
      do i=1,4
         call pdfcall(1,xxx(i),pdf1)
         write(*,*) 'x,g,d,u,c,b -> ',xxx(i),pdf1(0),pdf1(1),pdf1(2),pdf1(4),pdf1(5)
      enddo

      muF=2.d0
      st_mufact2 = muF**2
      write(*,*) 'muF = ',muF
      do i=1,4
         call pdfcall(1,xxx(i),pdf1)
         write(*,*) 'x,g,d,u,c,b -> ',xxx(i),pdf1(0),pdf1(1),pdf1(2),pdf1(4),pdf1(5)
      enddo
      
      muF=5.d0
      st_mufact2 = muF**2
      write(*,*) 'muF = ',muF
      do i=1,4
         call pdfcall(1,xxx(i),pdf1)
         write(*,*) 'x,g,d,u,c,b -> ',xxx(i),pdf1(0),pdf1(1),pdf1(2),pdf1(4),pdf1(5)
      enddo
      
      muF=91.d0
      st_mufact2 = muF**2
      write(*,*) 'muF = ',muF
      do i=1,4
         call pdfcall(1,xxx(i),pdf1)
         write(*,*) 'x,g,d,u,c,b -> ',xxx(i),pdf1(0),pdf1(1),pdf1(2),pdf1(4),pdf1(5)
      enddo
      
      write(*,*) '============================='
      end


      logical function coupleW (j,k)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'      
      include 'pwhg_ckm.h'
      integer j,k
      integer,save::idvecbosW
      logical, save::ini=.true.
      
      if(ini)then
         do k=3,nlegborn
            if(flst_born(k,1).eq.24)then
               idvecbosW=+24
            elseif(flst_born(k,1).eq.-24)then
               idvecbosW=-24
            endif
         enddo
         ini=.false.
      endif
      coupleW=.false.
      ! Assuming CKM diagonal
      if(ckm_diag)then
         if(idvecbosW.eq.24) then !W+                                                                                                                                                 
            if(j.eq.-1.and.k.eq.2) coupleW=.true.
            if(j.eq.2.and.k.eq.-1) coupleW=.true.
            if(j.eq.-3.and.k.eq.4) coupleW=.true.
            if(j.eq.4.and.k.eq.-3) coupleW=.true.
            if(j.eq.-5.and.k.eq.6) coupleW=.true.
            if(j.eq.6.and.k.eq.-5) coupleW=.true.
         elseif(idvecbosW.eq.-24) then !W-                                                                                                                                          
            if(j.eq.1.and.k.eq.-2) coupleW=.true.
            if(j.eq.-2.and.k.eq.1) coupleW=.true.
            if(j.eq.3.and.k.eq.-4) coupleW=.true.
            if(j.eq.-4.and.k.eq.3) coupleW=.true.
            if(j.eq.5.and.k.eq.-6) coupleW=.true.
            if(j.eq.-6.and.k.eq.5) coupleW=.true.
         endif
      else
         write(*,*) "To be implemented yet"
         stop
      endif
      end
