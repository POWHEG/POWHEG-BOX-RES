      module MiNLO_cache
      implicit none
      include 'nlegborn.h'
      
!      integer,parameter,private:: flav=5
      integer,save,private:: flav
      real*8,parameter,private:: def=-1d10 
      real*8, dimension(:,:),allocatable,save,private :: M_rescfactor
      public :: check_cache,get_cache,clean_cache,update_cache,set_equivalent_flavour
      logical,save,public:: sudakov_cache=.true.
      
      contains

      subroutine cache_init(nlight)
      implicit none
      integer, intent(in)::nlight
      flav=nlight
      if(allocated(M_rescfactor))deallocate(M_rescfactor)
      allocate(M_rescfactor(-flav:flav,-flav:flav))
      M_rescfactor(:,:)=def
      end subroutine cache_init

      function check_cache(flav1,flav2) result(status)
      implicit none 
      integer flav1,flav2
      logical status
      status=.false.
      if(M_rescfactor(flav1,flav2).ne.def) then
         status=.true.
      endif
      end function check_cache

      function get_cache(flav1,flav2) result(result)
      implicit none
      integer flav1,flav2
      real*8 result
      result=M_rescfactor(flav1,flav2)
      end function get_cache

      subroutine clean_cache
      implicit none
      ! The Sudakov can never be negative (def<0)
      M_rescfactor=def
      return
      end subroutine clean_cache

      subroutine update_cache(cache,flav1,flav2)
      implicit none
      include 'pwhg_flg.h'
      integer, optional:: flav1,flav2
      real*8 cache

      if(present(flav1).and.present(flav2))then
         if(M_rescfactor(flav1,flav2).ne.def.and.flg_smartMiNLO) then
            write(*,*) "You are trying to overwrite a non-zero"  
            write(*,*) "stored value of rescfactor ",M_rescfactor(flav1,flav2)
            write(*,*) "with",cache," ... "
            stop
         endif

         call set_equivalent_flavour(M_rescfactor,flav,cache,flav1,flav2)
      else
         M_rescfactor(:,:)=cache
      endif
      
      end subroutine update_cache


      ! This subroutine is also used inpependently of the caching system,
      ! to fill equivalent flavour entries of M, passed as argument
      subroutine set_equivalent_flavour(M,n,cache,flav1,flav2)
      implicit none
      include "pwhg_ckm.h"
      integer,intent(in)::n
      real*8 :: M(-n:n,-n:n)
      real*8 cache
      integer sign,j,k,flav1,flav2
      
      sign=+1
      if(flav1.eq.0.and.flav2.eq.0)then
         M(0,0)=cache
      elseif(abs(flav1).eq.abs(flav2))then
         if(flav1.lt.0) sign=-1
         if(mod(abs(flav1),2).eq.1d0) then
            do j=1,n,2
               k=j*sign
               M(k,-k)=cache
            enddo
         else
            do j=2,n,2
               k=j*sign
               M(k,-k)=cache
            enddo
         endif
      elseif(flav1.eq.0.and.flav2.ne.0)then
         if(flav2.lt.0) sign=-1
         if(mod(abs(flav2),2).eq.1d0) then
            do j=1,n,2
               k=j*sign
               M(0,k)=cache
            enddo
         else
            do j=2,n,2
               k=j*sign
               M(0,k)=cache
            enddo
         endif
      elseif(flav2.eq.0.and.flav1.ne.0)then
         if(flav1.lt.0) sign=-1
         if(mod(abs(flav1),2).eq.1d0) then
            do j=1,n,2
               k=j*sign
               M(k,0)=cache
            enddo
         else
            do j=2,n,2
               k=j*sign
               M(k,0)=cache
            enddo
         endif         
      elseif(ckm_diag.and.abs(flav1).ne.abs(flav2).and.
     1       abs(abs(flav1)-abs(flav2)).eq.1) then !Used only for diagonal CKM
         if(flav1.lt.0) sign=-1
         if(mod(abs(flav1),2).eq.1d0) then 
               do j=-3,0,2
                  k=j*sign
                  M(-k, sign*(j-1))=cache
               enddo
         else
               do j=-4,-1,2
                  k=j*sign
                   M(-k, sign*(j+1))=cache
               enddo
         endif
      else
         write(*,*) "Unknown underlying born"
         write(*,*) "flavour structure ... "
         stop
      endif
      end subroutine set_equivalent_flavour
      
      end module MiNLO_cache
